package es.uniovi.uo237525.dataquality.engine.rules.remediation

import java.util.UUID

import es.uniovi.uo237525.dataquality.LogConstants.Level
import es.uniovi.uo237525.dataquality.engine.RemediationInfo
import es.uniovi.uo237525.dataquality.engine.rules.{RemediationRule, RuleOptions}
import es.uniovi.uo237525.dataquality.utils.SparkUtils
import scala.util.Try

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StringType

case class ReplaceOutOfRangeOptions(default: String, accepted: Array[String], caseSensitive: Boolean = false) extends RuleOptions

/**
  * This remediation rule replaces all the records that are not in the accepted list by a default provided value.
  *
  * @param id      Identifier for the rule. Automatically generated if not provided
  * @param field   Field in which the rule is going to be applied
  * @param options ReplaceOutOfRangeOptions needed for the rule application.
  */
case class ReplaceOutOfRange(id: String = UUID.randomUUID().toString,
                             field: String,
                             options: ReplaceOutOfRangeOptions) extends RemediationRule {

  val default = options.default
  val accepted = options.accepted
  val caseSensitive = options.caseSensitive

  /**
    * Checks the needed preconditions for the rule.
    *
    * The preconditions that need to be fulfilled are:
    *   - The field exists
    *   - The field is of type StringType
    *   - The list containing all the possible values is not empty
    *
    * @param df the dataframe to evaluate
    * @return The error string in case a condition is not fulfilled. None otherwise
    */
  override def checkPreconditions(df: DataFrame): Option[String] = {
    // checking that the fields exists
    val res = Try(df(field)).isSuccess
    if (!res) {
      return Some(s"Field $field does not exists.")
    }

    val columnType = df.select(field).schema.head.dataType
    columnType match {
      case StringType => None
      case _ => return Some(s"This rule can not be applied to fields of type $columnType")
    }

    if (accepted.isEmpty) Some("List of accepted values is empty")
    else None
  }

  /**
    * Calculates whether the number of records in a field that are not in the specified set of acceptable values.
    * And if that is the case, replaces them by a default value
    * Null records are ignored and not replaced.
    *
    * @param df the base dataframe where the rule is going to be applied
    * @return the results of the application of the rule
    */
  override def apply(df: DataFrame): Try[RemediationInfo] = Try {
    val spark = SparkUtils.getSparkSession()
    import spark.implicits._

    val calculateSuitableValueUdf = udf((value: String) => {
      if (value != null) {
        if (caseSensitive) {
          if (accepted.contains(value)) (value, false)
          else (default, true)
        }
        else {
          if (accepted.map(_.toLowerCase()).contains(value.toLowerCase())) (value, false)
          else (default, true)
        }
      }
      else (value, false)
    })

    val auxField = s"${UUID.randomUUID()}"
    val dfWithCalc = df.withColumn(field, calculateSuitableValueUdf(col(field)))
      .withColumn(auxField, $"$field._2").withColumn(field, $"$field._1")

//    dfWithCalc.show()

    // calculate num replaced values
    val numRemValues = dfWithCalc.filter(dfWithCalc(auxField)).count()
    val modifiedDf = dfWithCalc.drop(auxField)
    val totalRows = df.select(field).count()

    RemediationInfo(Level.INFO,
      s"""Substituted all values (except null) not present in ${accepted.mkString("[", ",", "]")} by '$default'""",
      numRemValues,
      totalRows,
      modifiedDf)
  }
}
