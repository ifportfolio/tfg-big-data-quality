package es.uniovi.uo237525.dataquality.reader

import java.io.File

import es.uniovi.uo237525.dataquality.{InputTypes, SparkSessionTestWrapper}
import es.uniovi.uo237525.dataquality.parser.models.InputParams
import es.uniovi.uo237525.dataquality.reader.sources.HiveReader
import es.uniovi.uo237525.dataquality.utils.SparkUtils
import org.apache.spark.sql.{AnalysisException, SaveMode}
import org.scalatest.tagobjects.Slow
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FunSuite}

import scala.util.Try

class HiveReaderTest extends FunSuite with BeforeAndAfterAll with BeforeAndAfterEach with SparkSessionTestWrapper {

  private val tableName = "defaultTableCSV"
  private val warehouse = new File("./src/test/resources/hive/test-spark-warehouse").getCanonicalPath
  private val defaultDb = "default"
  private val dbTest = "dbTest"
  private val dbTestTable = s"${dbTest}Table"
  private val dbTestAlt = "dbTestAlt"
  private val dbTestAltTable = s"${dbTestAlt}Table"

  override def beforeAll(): Unit = {

    val sparkMaster = "spark.master"
    val props = System.getProperties
    props.setProperty("spark.master", "local[2]")
    val master = System.getProperties.getProperty(sparkMaster)

    assume(master != null, "The property spark.master should be enabled in order that the tests work")

    // initialize context with the proper params
    val i1 = InputParams(InputTypes.HIVE,
      hive_warehouse_dir = Some(warehouse),
      hive_database = Some(defaultDb),
      hive_table = Some(tableName))
    Try(HiveReader(i1).createDF)


    // generate the test hive
    val sparkSession = SparkUtils.getSparkSession(enableHiveSupport = true, "test")

    val txtPath = new File(getClass.getResource("/txt/subset_unique_tracks.txt").getPath).getCanonicalPath
    val df = sparkSession.read.option("sep", "|")
      .csv(txtPath)

    sparkSession.sql(s"DROP DATABASE IF EXISTS $dbTest CASCADE")
    sparkSession.sql(s"DROP DATABASE IF EXISTS $dbTestAlt CASCADE")
    sparkSession.sql(s"DROP TABLE IF EXISTS $defaultDb.$tableName")
//    sparkSession.catalog.refreshTable(s"$defaultDb.$tableName")
    // saving table in default
    df.write.mode(SaveMode.Overwrite).saveAsTable(s"$defaultDb.$tableName")

    // creating db
    sparkSession.sql(s"create database $dbTest")
    sparkSession.sql(s"create database $dbTestAlt")
    sparkSession.sql(s"use $dbTest")
    df.write.mode(SaveMode.Overwrite).saveAsTable(dbTestTable)
    sparkSession.sql(s"use $dbTestAlt")
    df.write.mode(SaveMode.Overwrite).saveAsTable(dbTestAltTable)

  }

  override def afterAll(): Unit = {
    // generate the test hive
    val sparkSession = SparkUtils.getSparkSession(enableHiveSupport = true, "test")
    sparkSession.sql(s"DROP DATABASE IF EXISTS $dbTest CASCADE")
    sparkSession.sql(s"DROP DATABASE IF EXISTS $dbTestAlt CASCADE")
    sparkSession.sql(s"use $defaultDb")
    sparkSession.sql(s"DROP TABLE IF EXISTS $tableName")
  }

  private def changeCurrentDatabaseToDefault(): Unit = {
    val i1 = InputParams(InputTypes.HIVE,
      hive_warehouse_dir = Some(warehouse),
      hive_database = Some(defaultDb))
    Try(HiveReader(i1).createDF)
  }

  /** TEST DATABASE */
  test("test DATABASE PARAM: missing uses last used db", Slow) {
    changeCurrentDatabaseToDefault()
    val i1 = InputParams(InputTypes.HIVE,
      hive_warehouse_dir = Some(warehouse),
      hive_table = Some(dbTestTable))
    assertThrows[AnalysisException] {
      HiveReader(i1).createDF
    }
    // it should read correctly from the default db table
    changeCurrentDatabaseToDefault()
    val ipDef = InputParams(InputTypes.HIVE,
      hive_warehouse_dir = Some(warehouse),
      hive_table = Some(tableName))
    HiveReader(ipDef).createDF
    succeed
  }

  test("test DATABASE PARAM: database no existing", Slow) {
    val i1 = InputParams(InputTypes.HIVE,
      hive_warehouse_dir = Some(warehouse),
      hive_database = Some("no-existing-db"),
      hive_table = Some(dbTestTable))
    assertThrows[AnalysisException] {
      HiveReader(i1).createDF
    }
  }

  test("test DATABASE PARAM: existing database", Slow) {
    val i1 = InputParams(InputTypes.HIVE,
      hive_warehouse_dir = Some(warehouse),
      hive_database = Some(dbTest),
      hive_table = Some(dbTestAltTable))
    assertThrows[AnalysisException] {
      HiveReader(i1).createDF
    }

    val i2 = InputParams(InputTypes.HIVE,
      hive_warehouse_dir = Some(warehouse),
      hive_database = Some(dbTest),
      hive_table = Some(dbTestTable))
    HiveReader(i2).createDF
    succeed
  }

  /** Test WAREHOUSE */
  test("test WAREHOUSE PROPERTY missing implies using the spark default", Slow) {
    // should fail because was not created in the default warehouse
    val ip1 = InputParams(InputTypes.HIVE,
      hive_table = Some(tableName))
    assertThrows[AnalysisException] {
      HiveReader(ip1).createDF
    }
  }

  test("test WAREHOUSE PROPERTY: valid value", Slow) {
    val i = InputParams(InputTypes.HIVE,
      hive_warehouse_dir = Some(warehouse),
      hive_database = Some(defaultDb),
      hive_table = Some(tableName))
    HiveReader(i).createDF
    succeed
  }

  test("test WAREHOUSE PROPERTY: un-existing warehouse dir", Slow) {
    // alternativeTableName should not be created in the default warehouse dir
    val ipShouldFail = InputParams(InputTypes.HIVE,
      hive_warehouse_dir = Some("un-existing"),
      hive_table = Some(dbTestAltTable))
    val readerShouldFail = HiveReader(ipShouldFail)
    assertThrows[AnalysisException] {
      readerShouldFail.createDF
    }
  }

  test("test WAREHOUSE PROPERTY null value", Slow) {
    val inputParams = InputParams(InputTypes.HIVE,
      hive_warehouse_dir = Some(warehouse))
    val r = HiveReader(inputParams)
    assertThrows[IllegalArgumentException] {
      r.createDF
    }
  }

  /** TEST MANDATORY PROPERTY TABLE NAME DIR */
  test("test MANDATORY PROPERTY: tableName missing throws IllegalArgumentException", Slow) {
    changeCurrentDatabaseToDefault()
    val inputParams = InputParams(InputTypes.HIVE,
      hive_warehouse_dir = Some("not missing"))
    val r = HiveReader(inputParams)
    assertThrows[IllegalArgumentException] {
      r.createDF
    }
  }

  test("test MANDATORY PROPERTY invalid tableName throws AnalysisException", Slow) {
    val inputParams = InputParams(InputTypes.HIVE,
      hive_table = Some("wrong-tableName"))
    val r = HiveReader(inputParams)
    assertThrows[AnalysisException] {
      r.createDF
    }
  }

  test("test MANDATORY PROPERTY tableName null throws AnalysisException", Slow) {
    val inputParams = InputParams(InputTypes.HIVE,
      hive_table = Some(null))
    val r = HiveReader(inputParams)
    assertThrows[NullPointerException] {
      r.createDF
    }
  }

  test("test TABLE PARAM: only possible to read tables in the current database", Slow) {
    // alt table does not exists on db test
    val i = InputParams(InputTypes.HIVE,
      hive_warehouse_dir = Some(warehouse),
      hive_database = Some(dbTest),
      hive_table = Some(dbTestAltTable))
    assertThrows[AnalysisException] {
      HiveReader(i).createDF
    }

    // alt table exists on alt database
    val i1 = InputParams(InputTypes.HIVE,
      hive_warehouse_dir = Some(warehouse),
      hive_database = Some(dbTestAlt),
      hive_table = Some(dbTestAltTable))
    HiveReader(i1).createDF

    changeCurrentDatabaseToDefault()
    // from default database you cannot read other db tables
    val i2 = InputParams(InputTypes.HIVE,
      hive_warehouse_dir = Some(warehouse),
      hive_table = Some(dbTestTable))
    assertThrows[AnalysisException] {
      HiveReader(i2).createDF
    }

    // default db tables can only be read when using default db
    val i3 = InputParams(InputTypes.HIVE,
      hive_warehouse_dir = Some(warehouse),
      hive_database = Some(dbTest),
      hive_table = Some(tableName))
    assertThrows[AnalysisException] {
      HiveReader(i3).createDF
    }
  }

  /** TEST METASTORE URI NAME */
}
