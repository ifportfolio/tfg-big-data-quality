package es.uniovi.uo237525.dataquality.reader

import es.uniovi.uo237525.dataquality.reader.sources.{CSVReader, HiveReader}
import es.uniovi.uo237525.dataquality._
import es.uniovi.uo237525.dataquality.parser.models.InputParams
import es.uniovi.uo237525.dataquality.reader.sources.{CSVReader, HiveReader}
import org.scalatest.FunSuite
import es.uniovi.uo237525.dataquality.reader.sources.{CSVReader, HiveReader}


class ReaderEngineTest extends FunSuite {

  test("test CREATE READER: un-existing source type throws IllegalArgumentException") {
    val input = InputParams("unknownType")
    val da = new ReaderEngine(input)
    assertThrows[IllegalArgumentException] {
      da.createReader
    }
  }

  test("test CREATE READER: NULL sourceType throws IllegalArgumentException") {
    val input = InputParams(null)
    val da = new ReaderEngine(input)
    assertThrows[NullPointerException] {
      da.createReader
    }
  }

  test("test CREATE READER: HiveReader creation successfully works") {
    val input = InputParams(InputTypes.HIVE)
    assert(new ReaderEngine(input).createReader.isInstanceOf[HiveReader])
  }

  test("test CREATE READER:  CSVReader creation successfully works") {
    val da = new ReaderEngine(inputParams = InputParams(InputTypes.CSV))
    assert(da.createReader.isInstanceOf[CSVReader])

    val daUp = new ReaderEngine(inputParams = InputParams("cSv"))
    assert(daUp.createReader.isInstanceOf[CSVReader])
  }
}
