package es.uniovi.uo237525.dataquality.logmanager.logs

import java.sql.Timestamp
import java.time.Instant

import es.uniovi.uo237525.dataquality.LogConstants.Level.Level
import es.uniovi.uo237525.dataquality.LogConstants.LogType.LogType
import es.uniovi.uo237525.dataquality.logmanager.Log

/**
  * Generic log without any extra fields than the trait
  *
  * @param level
  * @param info
  * @param logType
  * @param timestamp
  */
case class GenericLog(level: Level,
                      info: String,
                      logType: LogType,
                      timestamp: Timestamp = Timestamp.from(Instant.now())) extends Log {


  override def toString: String = s"$timestamp [$level] - $logType: $info"

}
