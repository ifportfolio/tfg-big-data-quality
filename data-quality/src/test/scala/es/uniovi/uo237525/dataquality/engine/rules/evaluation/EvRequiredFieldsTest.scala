package es.uniovi.uo237525.dataquality.engine.rules.evaluation

import es.uniovi.uo237525.dataquality.SparkSessionTestWrapper
import org.scalatest.FunSuite

import org.apache.spark.sql.AnalysisException

class EvRequiredFieldsTest extends FunSuite with SparkSessionTestWrapper {

  import spark.implicits._

  /** QUALITY PRE-CONDITIONS TEST */
  test("test QUALITY PRE-CONDITIONS: application on un-existing field") {
    val rule = EvRequiredFields(field = "non-existing")
    val someDF = Seq(1, 2, 3).toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }
  test("test QUALITY PRE-CONDITIONS: application on existing field") {
    val rule = EvRequiredFields(field = "nums")
    val someDF = Seq(1, 2, 3).toDF("nums")
    assert(rule.checkPreconditions(someDF).isEmpty)
  }

  /** TEST APPLY QUALITY */
  test("test QUALITY APPLY: application on existing fields") {
    val df = Seq((1, null),
      (2, "value"),
      (3, null)
    ).toDF("nums", "values")
    val resNoNulls = EvRequiredFields(field = "nums").apply(df).get
    val resNulls = EvRequiredFields(field = "values").apply(df).get
    assert(resNoNulls.matched == 0)
    assert(resNulls.matched == 2)
  }
  test("test QUALITY APPLY: application on un-existing fields") {
    val df = Seq((1, null),
      (2, "value"),
      (3, null)
    ).toDF("nums", "values")
    val fieldName = "no-existing"
    val thrown = intercept[AnalysisException] {
      EvRequiredFields(field = fieldName).apply(df).get
    }
    assert(thrown.getSimpleMessage == s"cannot resolve '`$fieldName`' given input columns: [nums, values];")
  }
  test("test QUALITY APPLY: testing empty and None values") {
    val df = Seq((1, null),
      (2, ""),
      (3, null),
      (4, "")
    ).toDF("nums", "values")
    val resNulls = EvRequiredFields(field = "values").apply(df).get
    assert(resNulls.matched == 4)
  }

}
