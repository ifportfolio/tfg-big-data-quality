import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { LaunchBdqService } from './services/launch-bdq.service';
import { LaunchResult } from './model/LaunchResult';

/**
 * Main component that handles the events thrown by its child components.
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  /**
   * Title for the entire client.
   */
  title = 'Big Data Quality Client';

  /**
   * Represents whether the component is waiting for a backend result.
   */
  loading = false;

  /**
   * Name for the rules file.
   */
  rulesFile: string;
  /**
   * Content of the rules file.
   */
  rulesContent: string;
  /**
   * Name for the config file
   */
  configFile: string;
  /**
   * Content for the configuratio file
   */
  configContent: string;

  /**
   *Launch result of the last execution
   */
  launchResult: LaunchResult;
  /**
   * Message of the error.
   */
  errorMsg: string;

  /**
   * Contructor to inject the services and initialize the variables.
   * 
   * @param launchService The launch service that request the bdq execution to the backend.
   */
  constructor(private launchService: LaunchBdqService) {
    this.rulesFile = "";
    this.rulesContent = "";
    this.configFile = "";
    this.configContent = "";
    this.launchResult = null;
    this.errorMsg = "";
  }

  /**
   * Handler for the dashboard event of launching a new bdq execution.
   * It puts the loading variable to true while waiting for a response.
   * Assigns the results (error or not) to the variables that provides the info to the dashboard. 
   */
  handleLaunch() {
    this.loading = true;
    if (this.rulesContent.length == 0 || this.rulesFile.length == 0) {
      this.loading = false;
      this.launchResult = null;
      this.errorMsg = "The BDQ component can not be executed without a rules file o an empty one."
      console.error(this.errorMsg)
    }
    else {
      console.log("Rules fn: " + this.rulesFile)
      console.log("Rules content: " + this.rulesContent.length)
      console.log("Config fn: " + this.configFile)
      console.log("Rules fn: " + this.configContent.length)
      this.launchService.launchBDQComponent(this.rulesFile, this.rulesContent, this.configFile, this.configContent).subscribe(
        res => {
          this.errorMsg = ""
          let launchResult = new LaunchResult()
          launchResult.start = res.start
          launchResult.execTime = res.execTime
          launchResult.execState = res.execState//.replace('\n', "<br>");
          launchResult.execLog = res.execLog//.replace('\n', "<br>");
          launchResult.rulesPath = res.rulesPath
          launchResult.configPath = res.configPath
          launchResult.alerts = JSON.parse(res.alerts)
          this.launchResult = launchResult
          console.log(launchResult)
          this.loading = false
        },
        err => {
          this.loading = false;
          this.launchResult = null;
          this.errorMsg = err.message + "-> " + err.error.error + " " + err.error.status + ": " + err.error.message
          console.error(err)
        })
    }
  }
}
