package es.uniovi.uo237525.dataquality.engine.rules.evaluation

import java.util.UUID
import java.util.regex.Pattern

import es.uniovi.uo237525.dataquality.LogConstants.Level
import es.uniovi.uo237525.dataquality.engine.EvaluationInfo
import es.uniovi.uo237525.dataquality.engine.rules.{EvaluationRule, RuleOptions}

import scala.util.{Failure, Success, Try}
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StringType, _}


case class EvRegexOptions(regex: String, capGroup: Int = 0) extends RuleOptions

/**
  * The purpose of this rule is the detection of a regex in the provided field
  *
  * @param id      Identifier for the rule. Automatically generated if not provided
  * @param field   Field in which the rule is going to be applied
  * @param options EvDateFormatOptions needed for the rule application.
  */
case class EvRegex(id: String = UUID.randomUUID().toString,
                   field: String,
                   options: EvRegexOptions) extends EvaluationRule {

  private val regex = options.regex
  private val captGroup = options.capGroup

  /**
    * Checks the needed preconditions for the rule.
    *
    * The preconditions that need to be fulfilled are:
    *   - The field exists
    *   - The field must be of StringType
    *   - The specified capturing group must be >=0
    *   - The provided regex pattern must be valid (according to java.util.regex library)
    * @param df
    * @return The error string in case a condition is not fulfilled. None otherwise
    */
  override def checkPreconditions(df: DataFrame): Option[String] = {
    val exists = Try(df(field)).isSuccess
    if (!exists) {
      val errorMsg = s"Field $field does not exists."
      return Some(errorMsg)
    }

    val columnType = df.select(field).schema.head.dataType
    columnType match {
      case StringType => None
      case _ => return Some(s"EvRegex rule can not be applied to fields of type $columnType")
    }

    if (captGroup < 0) return Some("The specified capturing group should be greater or equal 0")

    Try(Pattern.compile(regex)) match {
      case Failure(exception) => Some(s"Provided regex is not valid: ${exception.getMessage}")
      case Success(_) => None
    }
  }

  /**
    * Counts the number of records that match the provided regex with the provided capturing group.
    * Null values are count as non-matched by the rule.
    *
    * @param df the base dataframe where the rule is going to be applied
    * @return A pair containing the resulting DataFrame and
    *         the results of the application of the rule
    */
  override def apply(df: DataFrame): Try[EvaluationInfo] = Try {
    val totalRows = df.select(field).count()
    val countMatching = df.select(field).filter(col(field).isNotNull).filter(length(regexp_extract(col(field), regex, captGroup)) =!= 0).count()

    EvaluationInfo(Level.INFO,
      s"Regex: '$regex' evaluated",
      countMatching,
      totalRows)
  }

}
