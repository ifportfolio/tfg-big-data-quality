package es.uniovi.uo237525.dataquality.engine.rules.evaluation

import java.sql.{Date, Timestamp}
import java.text.SimpleDateFormat
import java.time.Instant

import es.uniovi.uo237525.dataquality.SparkSessionTestWrapper
import org.scalatest.FunSuite
import scala.util.Try

import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.sql.types._


class EvDateFormatTest extends FunSuite with SparkSessionTestWrapper {

  import spark.implicits._

  val txtPath: String = getClass.getResource("/txt/date-format-dataset.txt").getPath
  private val someDf: DataFrame = spark.read.option("delimiter", "|").option("header", value = true).csv(txtPath).cache()

  /** QUALITY PRE-CONDITIONS TEST */
  test("test QUALITY PRE-CONDITIONS: only applicable on string columns") {
    val strColName = "str"
    val sch = StructType(
      StructField(strColName, StringType) ::
        StructField("date", DateType) ::
        StructField("int", LongType) ::
        StructField("timestamp", TimestampType)
        :: Nil)
    val ts = Timestamp.from(Instant.now())
    val date = new Date(System.currentTimeMillis())
    val seq = Seq(
      Row("2018-12-31", date, 0, ts),
      Row("31-12-2018", date, 1, ts),
      Row("31/12/2018", date, 2, ts))

    val someDf = spark.createDataFrame(
      spark.sparkContext.parallelize(seq),
      StructType(sch)
    ).cache()

    val opts = EvDateFormatOptions("dd-mm-yyyy")
    for (columnName <- sch.map(_.name)) {
      val rule = EvDateFormat(field = columnName, options = opts)
      val canBeApplied = rule.checkPreconditions(someDf).isEmpty
      if (columnName.equals(strColName)) {
        assert(canBeApplied)
      }
      else assert(!canBeApplied)
    }
  }

  test("test QUALITY PRE-CONDITIONS: application on un-existing field") {
    val rule = EvDateFormat(field = "non-existing", options = EvDateFormatOptions("yyyy-MM-dd"))
    val someDF = Seq("1", "2", "3").toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }
  test("test QUALITY PRE-CONDITIONS: application on existing string field") {
    val rule = EvDateFormat(field = "nums", options = EvDateFormatOptions("yyyy-MM-dd"))
    val someDF = Seq("1", "2", "3").toDF("nums")
    assert(rule.checkPreconditions(someDF).isEmpty)
  }
  test("test QUALITY PRE-CONDITIONS: application on existing non-string field") {
    val rule = EvDateFormat(field = "nums", options = EvDateFormatOptions("yyyy-MM-dd"))
    val someDF = Seq(1, 2, 3).toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }
  test("test QUALITY PRE-CONDITIONS: invalid format") {
    val rule = EvDateFormat(field = "nums", options = EvDateFormatOptions("invalid-format"))
    val someDF = Seq("1", "2", "3").toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }
  test("test QUALITY PRE-CONDITIONS: null format") {
    val rule = EvDateFormat(field = "nums", options = EvDateFormatOptions(null))
    val someDF = Seq("1", "2", "3").toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }
  test("test QUALITY PRE-CONDITIONS: valid format") {
    val rule = EvDateFormat(field = "nums", options = EvDateFormatOptions("yyyy-MM-dd"))
    val someDF = Seq("1", "2", "3").toDF("nums")
    assert(rule.checkPreconditions(someDF).isEmpty)
  }

  /** TEST APPLY QUALITY */
  test("test QUALITY APPLY: application on existing fields (generic)") {
    val rule = EvDateFormat(field = "dd-MM-yyyy", options = EvDateFormatOptions("yyyy.M-d"))
    assert(rule.apply(someDf).get.matched == 0)

    val rule1 = EvDateFormat(field = "dd-MM-yyyy", options = EvDateFormatOptions("dd-MM-yyyy"))
    // knowing in advance the result
    // this column contains null, contains non-grouped valid numbers
    val numMatchingRows = someDf.select(rule.field).collect().map(row => row.getString(0)).count(date => {
      val dateFormat = new SimpleDateFormat(rule1.options.format)
      dateFormat.setLenient(false)
      Try(dateFormat.parse(date)).isSuccess
    })
    assert(rule1.apply(someDf).get.matched == numMatchingRows)
  }
  test("test QUALITY APPLY: application on un-existing fields") {
    val rule = EvDateFormat(field = "non-existing", options = EvDateFormatOptions("yyyy.M.d"))
    val someDF = Seq("1", "2", "3").toDF("nums")
    assert(rule.apply(someDF).isFailure)
  }

  test("test QUALITY APPLY: null values are count as non-matched")(pending)
  test("test QUALITY APPLY: application on non-string field")(pending)

}
