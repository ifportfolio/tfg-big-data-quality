package es.uniovi.uo237525.dataquality.storage.writers

import es.uniovi.uo237525.dataquality._
import es.uniovi.uo237525.dataquality.LogConstants.{Level, LogType}
import es.uniovi.uo237525.dataquality.logmanager.LogManager
import es.uniovi.uo237525.dataquality.parser.models.OutputParams
import es.uniovi.uo237525.dataquality.storage.Writer
import scala.collection.mutable
import scala.util.{Success, Try}

import org.apache.spark.sql.{DataFrame, SaveMode}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{ArrayType, MapType}

/**
  * The class that is responsible for writing the modified data set in CSV format
  *
  * @param outParams the params that specify all the options that need to be taken into account in order to properly write the modified data.
  */
case class CSVWriter(outParams: OutputParams) extends Writer {

  /**
    * Transforms into a String an element of any type.
    * In case it is an Array or a WrappedArray, a custom string is generated in a recursive manner.
    *
    * @param x the object to convert to String
    * @tparam T the type of the element to convert
    * @return The element reconverted into a String
    */
  private def stringAll[T](x: T): String = x match {
    case arr: mutable.WrappedArray[_] => stringAll(arr.array)
    case arr: Array[_] => arr.map(stringAll).mkString("[", ",", "]")
    case _ => x.toString
  }

  /**
    * Prepares the data set in order that the writer is able to write it.
    *
    * @param df the data set to prepare
    * @return a tuple containing the data set ready, the provided params needed during the writing process and
    *         the path where the data is going to be located.
    */
  private def prepareDf(df: DataFrame): (DataFrame, Map[String, String], String) = {
    val definedParams = outParams.getOptionsByHeader(OutputTypes.CSV)

    definedParams.foreach(param => {
      LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"Added param: ${param._1} -> ${param._2}")
    })

    val path = definedParams.getOrElse("path", throw new IllegalArgumentException("Mandatory CSV config field 'path' is missing."))

    val stringifyUDF = udf(stringAll _)

    val nameArrayColumns = df.schema.filter(el => {
      el.dataType match {
        case ArrayType(_, _) | MapType(_, _, _) => true
        case _ => false
      }
    }).map(_.name)

    (
      nameArrayColumns.foldLeft(df)((actualDf, colName) => {
        actualDf.withColumn(colName, when(col(colName).isNotNull, stringifyUDF(col(colName))))
      }),
      definedParams,
      path
    )
  }


  /**
    * Opens the connections and returns the connection instance
    *
    * @return An instance representing the connection
    */
  override def open(): Try[Any] = Success()

  /**
    * Substitutes the data by the new one.
    *
    * @param df         the dataframe that is going to be written.
    * @param connection an object that provides the connection to the source.
    */
  override def overwrite(df: DataFrame, connection: Any = null): Try[Unit] = Try {
    val pair = prepareDf(df)

    val dfToWrite = pair._1.cache()
    val opts = pair._2
    val path = pair._3

    dfToWrite.write.options(opts).mode(SaveMode.Overwrite).csv(path)
  }

  /**
    * Appends the content of the  the resulting data-frame onto the specific format.
    *
    * @param df         the dataframe that is going to be written.
    * @param connection an object that provides the connection to the source.
    */
  override def append(df: DataFrame, connection: Any = null): Try[Unit] = Try {
    val pair = prepareDf(df)

    val dfToWrite = pair._1.cache()
    val opts = pair._2
    val path = pair._3

    dfToWrite.write.options(opts).mode(SaveMode.Append).csv(path)
  }

  /**
    * It tries to save the data into de provided path, throwing an exception in case there is already data in that path,
    *
    * @param df         the dataframe that is going to be written.
    * @param connection an object that provides the connection to the source.
    */
  override def errorIfExists(df: DataFrame, connection: Any = null): Try[Unit] = Try {
    val pair = prepareDf(df)

    val dfToWrite = pair._1.cache()
    val opts = pair._2
    val path = pair._3

    dfToWrite.write.options(opts).mode(SaveMode.ErrorIfExists).csv(path)
  }

  /**
    * It tries to save the data into the provided path and, in case there is data already in that path,
    * it simply ignores the writing operation and continues without throwing an exception.
    *
    * @param df         the dataframe that is going to be written.
    * @param connection an object that provides the connection to the source.
    */
  override def ignore(df: DataFrame, connection: Any = null): Try[Unit] = Try {
    val pair = prepareDf(df)

    val dfToWrite = pair._1.cache()
    val opts = pair._2
    val path = pair._3

    dfToWrite.write.options(opts).mode(SaveMode.Ignore).csv(path)
  }
}