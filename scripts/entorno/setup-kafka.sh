#!/bin/bash

source ./common.sh

function installLocalKafka {
	echo "install Kafka from local file"
	FILE=$RESOURCES_DIR$KAFKA_ARCHIVE
	tar -xzf $FILE -C /usr/local
}

function installRemoteKafka {
	echo "install Kafka from remote file"
	curl -sS -o $RESOURCES_DIR/$KAFKA_ARCHIVE -O -L $KAFKA_MIRROR_DOWNLOAD

	tar -xzf $RESOURCES_DIR/$KAFKA_ARCHIVE -C /usr/local
}

function installKafka {
	if resourceExists $KAFKA_ARCHIVE; then
		installLocalKafka
	else
		installRemoteKafka
	fi
	ln -s /usr/local/kafka_$KAFKA_SCALA-$KAFKA_VERSION /usr/local/kafka
	mkdir /usr/local/kafka/logs
}

function setupKafka {
	cp -f $KAFKA_RES_DIR/* $KAFKA_CONF
}

function setupEnvVars {
	echo "creating kafka environment variables"
	cp -f $KAFKA_RES_DIR/kafka.sh /etc/profile.d/kafka.sh
	. /etc/profile.d/kafka.sh
}

function runKafka {
	if resourceExists $KAFKA_ARCHIVE; then
		installLocalKafka
	else
		installRemoteKafka
	fi
	echo "starting Zookeeper service"
	/usr/local/kafka/bin/zookeeper-server-start.sh -daemon $KAFKA_RES_DIR/zookeeper.properties 
	echo "starting Kafka service"
	/usr/local/kafka/bin/kafka-server-start.sh -daemon $KAFKA_RES_DIR/server.properties
}

echo "setup kafka"
installKafka
setupKafka
setupEnvVars
runKafka
