package es.uniovi.uo237525.dataquality.engine

import es.uniovi.uo237525.dataquality.LogConstants.Level
import es.uniovi.uo237525.dataquality.SparkSessionTestWrapper
import es.uniovi.uo237525.dataquality.engine.rules.evaluation.{EvDateFormat, EvDateFormatOptions}
import es.uniovi.uo237525.dataquality.logmanager.LogManager
import es.uniovi.uo237525.dataquality.logmanager.logs.RuleLog
import org.scalatest.{BeforeAndAfterAll, FunSuite}

class RulesEngineTest extends FunSuite with BeforeAndAfterAll with SparkSessionTestWrapper {

  /** TEST EXECUTE */
  test("test Execute with null dataframe") {
    val engine = new RulesEngine(null, List(EvDateFormat("id", "field", EvDateFormatOptions("format"))))
    engine.execute()
    assert(LogManager.logs.head.level === Level.WARN)
  }

  test("test Execute with null rules") {
    val engine = new RulesEngine(null, null)
    assertThrows[NullPointerException] {
      engine.execute()
    }
  }

}
