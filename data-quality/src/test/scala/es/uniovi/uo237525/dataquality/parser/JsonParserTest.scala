package es.uniovi.uo237525.dataquality.parser

import java.io.{File, FileNotFoundException}
import java.net.URL

import es.uniovi.uo237525.dataquality.engine.rules.evaluation.{EvDateFormat, EvFieldLength, EvNumericFormat, EvRangeValues, EvRegex, EvRequiredFields}
import es.uniovi.uo237525.dataquality.engine.rules.remediation.{FillEmptyRecords, RemDateFormat, RemNumericFormat, RemNumericFormatTest, ReplaceOutOfRange, SliceField}
import es.uniovi.uo237525.dataquality.parser.models.{InputParams, OutputParams}
import org.json4s.MappingException
import org.scalatest.{BeforeAndAfterAll, FunSuite}

import scala.io.Source

class JsonParserTest extends FunSuite with BeforeAndAfterAll {

  private val exampleJsonFileName: String = "example.json"
  private val exampleJsonFile: URL = getResource(exampleJsonFileName)
  private val exampleWithWrongRuleJsonFileName: String = "example-with-wrong-rule.json"
  private val exampleWithWrongRuleJsonFile: URL = getResource(exampleWithWrongRuleJsonFileName)
  private val wrongJsonFileName: String = "wrong-format.json"
  private val noRulesJson: String = "no-rules.json"
  private val noInputJson: String = "no-input.json"
  private val noOutputJson: String = "no-output.json"


  private def getResource(filename: String) = getClass.getResource(s"/json/$filename")

  override def beforeAll() {
    //    val inParams = classOf[InputParams].getDeclaredFields.map(_.getName.replace("$u002E", "."))
    //    val outParams = classOf[OutputParams].getDeclaredFields.map(_.getName.replace("$u002E", "."))
    val inParams = classOf[InputParams].getDeclaredFields.map(_.getName)
    val outParams = classOf[OutputParams].getDeclaredFields.map(_.getName)
    val fileContents = Source.fromFile(exampleJsonFile.getFile).mkString
    val params = inParams ++ outParams
    params.foreach(fieldName => {
      withClue(s"The example.json file is missing property: $fieldName, declare it before running the tests.\n") {
        assume(fileContents.contains(fieldName))
      }
    })
  }

  /** GENERIC TESTS */
  test("test parse wrong path") {
    val wrongPath = "/json/non-existing.json"
    assertThrows[FileNotFoundException] {
      JsonParser.extractJsonSchema(wrongPath)
    }
  }

  test("test parse un-parsable json") {
    assertThrows[FileNotFoundException] {
      JsonParser.extractJsonSchema("/json/" + wrongJsonFileName)
    }
  }

  test("test PARSE valid json") {
    assert(JsonParser.extractJsonSchema(exampleJsonFile.getPath, isAbsolute = true).isInstanceOf[JsonWrapper])
  }

  /** INPUT PARAMS */
  test("test INPUT PARAMS: missing property in file") {
    assertThrows[MappingException] {
      JsonParser.extractJsonSchema(getResource(noInputJson).getPath).input
    }
  }

   test("test INPUT PARAMS: correctness of input params parsing") {
    val input = JsonParser.extractJsonSchema(exampleJsonFile.getPath, isAbsolute = true).input
    val fileContents = Source.fromFile(exampleJsonFile.getFile).getLines.toList
      .map(_.trim)
      .map(_.replaceAll("[{}]", ""))
      .filter(_.nonEmpty)
    val inputParams = input.getClass.getDeclaredFields
    for (param <- inputParams) {
      param.setAccessible(true)
      val fieldName = param.getName.replace("$u002E", ".")
      val value = param.get(input)

      value match {
        case None => fail(s"Input param: $fieldName not parsed")
        case _ => // do nothing
      }

      val extractedValue = param.get(input) match {
        case Some(x) => x
        case _ => param.get(input)
      }
      withClue(exampleJsonFileName + " is not correctly parsed in input property: " + fieldName + ": " + extractedValue) {
        assert(fileContents.exists(el => {
          el.contains(fieldName.toString) && el.contains(extractedValue.toString)
        }))
      }
    }
  }

  /** OUTPUT PARAM TESTS */
  test("test OUTPUT PARAMS: undefined output in file parsed as None") {
    val noOutputFilePath = new File("./src/test/resources/json/" + noOutputJson).getCanonicalPath
    assert(JsonParser.extractJsonSchema(noOutputFilePath).output.isEmpty)
  }

  test("test OUTPUT PARAMS: correctness of output params parsing") {
    val output = JsonParser.extractJsonSchema(exampleJsonFile.getPath, isAbsolute = true).output.get
    val fileContents = Source.fromFile(exampleJsonFile.getFile).getLines.toList
      .map(_.trim)
      .map(_.replaceAll("[{}]", ""))
      .filter(_.nonEmpty)
    val outputParams = classOf[OutputParams].getDeclaredFields
    for (param <- outputParams) {
      param.setAccessible(true)
      val fieldName = param.getName.replace("$u002E", ".")
      val value = param.get(output)

      value match {
        case None => fail(s"Output param: $fieldName not parsed")
        case _ => // do nothing
      }

      val extractedValue = param.get(output) match {
        case Some(x) => x
        case _ => param.get(output)
      }
      withClue(exampleJsonFileName + " is not correctly parsed in output property: " + fieldName + ": " + extractedValue) {
        assert(fileContents.exists(el => {
          el.contains(fieldName.toString) && el.contains(extractedValue.toString)
        }))
      }
    }
  }

  /** ALERTS PARAM TESTS */
  test("test ALERTS: alerts parsed") {
    assert(JsonParser.extractJsonSchema(exampleJsonFile.getPath, isAbsolute = true).alerts.isDefined)
  }

  // mail alerts
  test("test ALERTS: mail alerts parsed") {
    val mailAlerts = JsonParser.extractJsonSchema(exampleJsonFile.getPath, isAbsolute = true).alerts.get.mail
    assert(mailAlerts.nonEmpty)
  }

  // kafka alerts
  test("test ALERTS: kafka alerts parsed") {
    val kafkaAlerts = JsonParser.extractJsonSchema(exampleJsonFile.getPath, isAbsolute = true).alerts.get.kafka
    assert(kafkaAlerts.nonEmpty)
  }

  // redis alerts
  test("test ALERTS: redis alerts parsed") {
    val redisAlerts = JsonParser.extractJsonSchema(exampleJsonFile.getPath, isAbsolute = true).alerts.get.redis
    assert(redisAlerts.nonEmpty)
  }

  /** RULES PARAM TESTS */
  test("test RULES: not provided parsed as empty rules list") {
    assert(JsonParser.extractJsonSchema(getResource(noRulesJson).getPath).rules.isEmpty)
  }

  test("test RULE UN-EXISTING-RULE not parsed") {
    assertThrows[MappingException] {
      JsonParser.extractJsonSchema(exampleWithWrongRuleJsonFile.getPath, isAbsolute = true)
    }
  }

  /** EVALUATION RULES PARSING */
  val rules = JsonParser.extractJsonSchema(exampleJsonFile.getPath, isAbsolute = true).rules
  // RequiredFields
  test("test RULE RequiredFields correctly parsed") {
    assert(rules.count(_.isInstanceOf[EvRequiredFields]) > 0)
  }
  //  test("test RULE RequiredFields missing mandatory params ")(pending)

  // NumericFormat
  test("test RULE NumericFormat correctly parsed") {
    assert(rules.count(_.isInstanceOf[EvNumericFormat]) > 0)
  }

  // DateFormat
  test("test RULE DateFormat correctly parsed") {
    assert(rules.count(_.isInstanceOf[EvDateFormat]) > 0)
  }

  // FieldLength
  test("test RULE StringFormat correctly parsed") {
    assert(rules.count(_.isInstanceOf[EvFieldLength]) > 0)
  }

  // EvRegex
  test("test RULE EvRegex correctly parsed") {
    assert(rules.count(_.isInstanceOf[EvRegex]) > 0)
  }

  // EvRangeValues
  test("test RULE EvRangeValues correctly parsed") {
    assert(rules.count(_.isInstanceOf[EvRangeValues]) > 0)
  }

  /** REMEDIATION RULES PARSING */
  // FillEmptyRecords
  test("test RULE FillEmptyRecords correctly parsed") {
    assert(rules.count(_.isInstanceOf[FillEmptyRecords]) > 0)
  }

  test("test RULE RemDateFormat correctly parsed") {
    assert(rules.count(_.isInstanceOf[RemDateFormat]) > 0)
  }

  test("test RULE RemNumericFormat correctly parsed") {
    assert(rules.count(_.isInstanceOf[RemNumericFormat]) > 0)
  }

  test("test RULE SliceField correctly parsed") {
    assert(rules.count(_.isInstanceOf[SliceField]) > 0)
  }

  // ReplaceOutOfRange
  test("test RULE ReplaceOutOfRange correctly parsed") {
    assert(rules.count(_.isInstanceOf[ReplaceOutOfRange]) > 0)
  }
}

