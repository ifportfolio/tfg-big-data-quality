package es.uniovi.uo237525.dataquality.parser.models

import org.apache.spark.sql.SaveMode

/**
  * All the possible output configuration parameters:
  *
  * @see: https://spark.apache.org/docs/latest/api/java/org/apache/spark/sql/DataFrameWriter.html
  * @param target                          Determines the type of the source from which the module is going to read the data.
  * @param mode                            Specifies the behavior when data or table already exists. Options include:
  *                                        - overwrite: overwrite the existing data.
  *                                        - append: append the data.
  *                                        - ignore: ignore the operation (i.e. no-op).
  *                                        - error or errorifexists: default option, throw an exception at runtime.
  * @param repartition                     Number of repartitions in which the data will be distributed.
  * @param csv_path                        Path for the csv file that will be generated.
  * @param csv_sep                         (default ,) Character as a separator for each field and value.
  * @param csv_quote                       (default ") Character used for escaping quoted values where the separator can be part of the value.
  *                                        If an empty string is set, it uses u0000 (null character).
  * @param csv_escape                      (default \) Character used for escaping quotes inside an already quoted value.
  * @param csv_charToEscapeQuoteEscaping   (default escape or \0) Character used for escaping the escape for the quote character.
  *                                        The default value is escape character when escape and quote characters are different,\0 otherwise.
  * @param csv_escapeQuotes                (default true) Flag indicating values containing quotes should always be enclosed in quotes.
  *                                        Default is to escape all values containing a quote character.
  * @param csv_quoteAll                    (default false) Flag indicating whether all values should always be enclosed in quotes.
  *                                        Default is to only escape values containing a quote character.
  * @param csv_header                      (default false) Writes the names of columns as the first line.
  * @param csv_ignoreLeadingWhiteSpace     (default true) Flag indicating if leading whitespaces from values being written should be skipped.
  * @param csv_ignoreTrailingWhiteSpace    (default true) Flag indicating defines if trailing whitespaces from values being written should be skipped.
  * @param csv_nullValue                   (default empty string) String representation of a null value.
  * @param csv_compression                 (default null): compression codec to use when saving to file. This can be one of the known case-insensitive shorten names:
  *                                        - none
  *                                        - bzip2
  *                                        - gzip
  *                                        - lz4
  *                                        - snappy
  *                                        - deflate
  * @param csv_dateFormat                  (default yyyy-MM-dd) String that indicates a date format. This applies to date type.
  *                                        Custom date formats follow the formats at java.text.SimpleDateFormat.
  * @param csv_timestampFormat             (default yyyy-MM-dd'T'HH:mm:ss.SSSXXX): String that indicates the timestamp format. This applies to timestamp type.
  *                                        Custom date formats follow the formats at java.text.SimpleDateFormat.
  * @param hive_metastore_uris             Location of the metastore database to connect
  * @param hive_warehouse_dir              Location of the hive warehouse.
  * @param hive_database                   Database where the table is stored
  * @param hive_table                      Table where the hive needs to read from
  */
case class OutputParams(target: String,
                        mode: Option[String] = Some(SaveMode.ErrorIfExists.name),
                        repartition: Option[Int] = None,
                        csv_path: Option[String] = None,
                        csv_sep: Option[String] = None,
                        csv_quote: Option[String] = None,
                        csv_escape: Option[String] = None,
                        csv_charToEscapeQuoteEscaping: Option[String] = None,
                        csv_escapeQuotes: Option[Boolean] = None,
                        csv_quoteAll: Option[Boolean] = None,
                        csv_header: Option[Boolean] = None,
                        csv_ignoreLeadingWhiteSpace: Option[Boolean] = None,
                        csv_ignoreTrailingWhiteSpace: Option[Boolean] = None,
                        csv_nullValue: Option[String] = None,
                        csv_compression: Option[String] = None,
                        csv_dateFormat: Option[String] = None,
                        csv_timestampFormat: Option[String] = None,
                        hive_metastore_uris: Option[String] = None,
                        hive_warehouse_dir: Option[String] = None,
                        hive_database: Option[String] = None,
                        hive_table: Option[String] = None) {

  /**
    * Calculates a map containing all the options that start by an specific header.
    *
    * @param header prefix for the option
    * @return a map containing all the options names with their values
    */
  def getOptionsByHeader(header: String): Map[String, String] = {
    classOf[OutputParams].getDeclaredFields
      .filter(_.getName.contains(header))
      .map(field => {
        field.setAccessible(true)
        val value: String = field.get(this) match {
          case Some(n) => n.toString
          case None => ""
        }
        val name: String = field.getName.replace("_", ".").split('.').last
        (name, value)
      }).filter(_._2.length > 0).toMap
  }
}