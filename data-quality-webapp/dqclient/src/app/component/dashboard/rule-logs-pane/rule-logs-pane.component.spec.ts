import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RuleLogsPaneComponent } from './rule-logs-pane.component';

describe('RuleLogsPaneComponent', () => {
  let component: RuleLogsPaneComponent;
  let fixture: ComponentFixture<RuleLogsPaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RuleLogsPaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RuleLogsPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
