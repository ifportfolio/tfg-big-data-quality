package es.uniovi.uo237525.dataquality.configuration

import java.io.{File, FileNotFoundException}

import com.typesafe.config._
import es.uniovi.uo237525.dataquality._
import es.uniovi.uo237525.dataquality.CommandLineConstants._
import es.uniovi.uo237525.dataquality.LogConstants._
import es.uniovi.uo237525.dataquality.logmanager.LogManager
import org.rogach.scallop.{ScallopConf, ScallopOption}
import org.rogach.scallop.exceptions.{Help, Version}
import scala.collection.JavaConverters._
import scala.util.{Failure, Success, Try}

/**
  * This class represents a Wrapper for the different configurations supported.
  * The precedence order for the property values is:
  *  - Command line property values: (Example: "-Dkey=value", "-D key1=value1 key2=value2", "-Dkey=value key1=value1 key2=value2").
  *  - Properties declared in config file.
  *  - If config file not provided, the properties declared in the default config file.
  *
  * It is possible to pass the command "--version" or "--help" in order to print the commands
  *
  * @param arguments The arguments passed to create the desired configurations.
  * @author Ignacio Fernandez Fernandez
  */
case class Configuration(arguments: Seq[String]) extends ScallopConf(arguments) {

  val version = "bdq v0.1.0"

  version(version)
  banner("Options: ")

  footer(
    """|Usage:
    spark-submit <spark-options> <jar-path> \
    -[p1] parameter1 \
    -[p2] parameter2""".stripMargin)
  mainOptions = Seq(rules, config)
  val rules: ScallopOption[String] = opt[String](
    short = RULE_ARG_SHORT, required = true, descr = "Relative path to the json containing the rules.")
  val config: ScallopOption[String] = opt[String](short = CONFIG_ARG_SHORT, descr = "Relative path to the config file.")
  val commandLineProperties: Map[String, String] = props[String](
    descr = "Allows to override properties from the configuration file.")
  verify

  Try(LogManager.addGenericLog(Level.INFO, LogType.CONFIG, s"Rules file path: $getRulesPath"))

  val confFile: Config = resolveConfig()

  override def onError(e: Throwable): Unit = e match {
    case Help("") =>
      builder.printHelp
    case Help(sub) =>
      builder.findSubbuilder(sub).get.printHelp
    case Version =>
      // scalastyle:off
      builder.vers.foreach(println)
    // scalastyle:on
    //    case ScallopException(message) => errorMessageHandler(message)
    case other => throw other
  }

  /**
    * Resolves the configuration with property values.
    *
    * If no path is provided, it will use the default config file located under the resources directory.
    * Else it will parse the file extracting its properties.
    *
    * When the use provided CLI properties (-Dkey=value...) they will override the existing ones with the same key.
    *
    * @return the configuration object with the proper configuration values for the execution.
    */
  private def resolveConfig(): Config = {
    val loadedConfig = config.toOption.fold(
      // if path not provided use default
      ifEmpty = {
        LogManager.addGenericLog(Level.INFO, LogType.CONFIG, s"Config file not provided, using default one '$CONFIG_DEFAULT_NAME'")
        ConfigFactory.load(CONFIG_DEFAULT_NAME)
      })(
      path => {
        // if path provided parse new file
        val confFile = new File(path)
        LogManager.addGenericLog(Level.INFO, LogType.CONFIG, s"Config file path: $path")
        if (confFile.exists) {
          val customConf = ConfigFactory.parseFile(confFile).resolve
          customConf
        }
        else throw new FileNotFoundException(s"No such file: ${confFile.getCanonicalPath}")
      }
    )
    val loadedConfigWithCliProp = commandLineProperties.foldLeft(loadedConfig)((actualConfig, property) => {
      val header = property._1
      val value = property._2
      actualConfig.withValue(header, ConfigValueFactory.fromAnyRef(value))
    })
    loadedConfigWithCliProp.entrySet().asScala.foreach(entry => {
      val key = entry.getKey
      val value = entry.getValue.render(ConfigRenderOptions.concise())
      LogManager.addGenericLog(Level.DEBUG, LogType.CONFIG, s"Config property: $key -> $value")
    })
    loadedConfigWithCliProp
  }

  /**
    * @param name The name identifying the property.
    * @return true if the property is declared on the configuration file, false otherwise.
    * @throws ConfigException.BadPath - if the path expression is invalid
    */
  private def isDeclaredOnConfigFile(name: String): Boolean = {
    if (name != null) confFile.hasPath(name) else false
  }

  /**
    *
    * @return returns the provided rule s file path.
    */
  def getRulesPath: String = rules()

  /**
    *
    * @return returns the provided config file path.
    */
  def getConfigFilePath: String = config()

  /**
    * Retrieves the property value as an string option.
    * It the property is not declared or can not be retrieved as a string by any reason, it returns None
    * In case the property is missing or the value cannot be converted to string the function returns None.
    *
    * @param name The name identifying the property.
    * @return
    */
  def getOptionalStringProperty(name: String): Option[String] = {
    if (isDeclaredOnConfigFile(name)) {
      Try(confFile.getString(name)) match {
        case Success(x) => Some(x)
        case Failure(_) => None
      }
    }
    else None
  }


  /**
    * Retrieves a String property value from the configuration file.
    * As it is supposed to be a mandatory property, an exception is thrown if it is missing.
    * Throws an exception if the property value cannot be converted to string type.
    *
    * @param name The name identifying the property.
    * @return The boolean value associated to the property.
    * @throws ConfigException.Missing   - if value is absent or null
    * @throws ConfigException.WrongType - if value is not convertible to string
    */
  def getMandatoryStringProperty(name: String): String = confFile.getString(name)

  /**
    * Retrieves the property value as an string option.
    * In case the property is missing or the value cannot be retrieved by some reason it returns None.
    *
    * @return The boolean value associated to the property inside a Some, None otherwise.
    */
  def getOptionalBooleanProperty(name: String): Option[Boolean] = {
    if (isDeclaredOnConfigFile(name)) {
      Try(confFile.getBoolean(name)) match {
        case Success(x) => Some(x)
        case Failure(_) => None
      }
    }
    else None

  }

  /**
    * Retrieves a Boolean property value from the configuration file.
    * As it is supposed to be a mandatory property, an exception is thrown if it is missing.
    * Throws an exception if the property value cannot be converted to boolean type.
    *
    * @param name The name identifying the property.
    * @return The boolean value associated to the property.
    * @throws ConfigException.Missing   - if value is retrieved from config file and it is absent or null
    * @throws ConfigException.WrongType - if value is retrieved from config file and is not convertible to boolean
    */
  def getMandatoryBooleanProperty(name: String): Boolean = confFile.getBoolean(name)

  /**
    * Retrieves the property value as an integer option.
    * It does not throw any exception.
    * In case the property is missing or the value cannot be retrieved it returns None.
    *
    * @param name The name identifying the property.
    * @return if declared, the int value associated to the property inside and Option, None otherwise.
    */
  def getOptionalIntProperty(name: String): Option[Int] = {
    if (isDeclaredOnConfigFile(name)) {
      Try(confFile.getInt(name)) match {
        case Success(x) => Some(x)
        case Failure(_) => None
      }
    }
    else None
  }

  /**
    * Retrieves a Int property value from the configuration file.
    * As it is supposed to be a mandatory property, an exception is thrown if it is missing.
    * Throws an exception if the property value cannot be converted to Int type.
    *
    * @param name The name identifying the property.
    * @return The int value associated to the property.
    * @throws ConfigException.Missing   - if value is retrieved from config file and it is absent or null
    * @throws ConfigException.WrongType - if value is retrieved from config file and is not convertible to int
    *                                   (for example it is out of range, or it's a boolean value)
    */
  def getMandatoryIntProperty(name: String): Int = confFile.getInt(name)

  /**
    * Calculates all the child properties that are present under one key prefix. T
    *
    * @param prefix        the prefix under which all the properties are defined.
    * @param includePrefix (DEFAULT: TRUE) if active, the map keys will contain the specified prefix preceding the property key.
    * @return an option containing the the properties
    */
  def getChildPropertiesOptionFromPath(prefix: String, includePrefix: Boolean = true): Option[Map[String, _]] = {
    if (isDeclaredOnConfigFile(prefix)) {
      val entrySet = confFile.getConfig(prefix).entrySet.asScala.toList.map(el => {
        if (includePrefix) (prefix + "." + el.getKey, el.getValue.unwrapped())
        else (el.getKey, el.getValue.unwrapped())
      }).toMap
      Some(entrySet)
    }
    else None
  }
}
