package es.uniovi.uo237525.dataquality.logmanager.logs

import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.time.Instant

import es.uniovi.uo237525.dataquality.LogConstants.Level.Level
import es.uniovi.uo237525.dataquality.LogConstants.LogType
import es.uniovi.uo237525.dataquality.LogConstants.LogType.LogType
import es.uniovi.uo237525.dataquality.logmanager.Log

/**
  * A model case class that represents the result of the application of a quality rule.
  *
  * @param id        Identifier of the rule (provided or generated)
  * @param level     Granularity level of the rule(DEBUG, INFO, WARN...)
  * @param signature Name of the rule
  * @param field     Field in which it was applied
  * @param matched   Number of records of the field that match
  * @param total     Total number of records in that field
  * @param info      Descriptive message
  * @param timestamp The time of the RuleLog creation
  * @param logType   Type of the log, it is always of "RULE" type.
  */
case class RuleLog(id: String,
                   level: Level,
                   signature: String,
                   field: String,
                   matched: Long,
                   total: Long,
                   info: String,
                   timestamp: Timestamp = Timestamp.from(Instant.now()),
                   logType: LogType = LogType.RULE) extends Log {


  override def toString: String = {
    val sb: StringBuilder = new StringBuilder
    val pattern = "MM/dd/yyyy HH:mm:ss.SSS"
    val formattedTs = new SimpleDateFormat(pattern).format(timestamp)
    sb.append(
      s"""
         |-TIMESTAMP: $formattedTs
         |-TYPE: $logType
         |-ID: $id
         |-LEVEL: $level
         |-SIGNATURE: $signature
         |-FIELD: $field
         |-MATCHES: $matched
         |-TOTAL: $total
         |-INFO: $info
      """.stripMargin)

    sb.toString
  }
}
