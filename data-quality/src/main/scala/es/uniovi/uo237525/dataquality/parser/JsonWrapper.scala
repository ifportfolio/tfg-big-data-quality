package es.uniovi.uo237525.dataquality.parser

import es.uniovi.uo237525.dataquality.engine.rules.Rule
import es.uniovi.uo237525.dataquality.parser.models.{Alerts, InputParams, OutputParams}

/**
  * Model case class that represents all the params that can/must be present in the specification rules JSON file.
  *
  * @param input  Input related params
  * @param output Output related params (optional)
  * @param alerts Defined alerts (Optional)
  * @param rules  List of defined rules
  */
case class JsonWrapper(input: InputParams,
                       output: Option[OutputParams],
                       alerts: Option[Alerts],
                       rules: List[Rule])



