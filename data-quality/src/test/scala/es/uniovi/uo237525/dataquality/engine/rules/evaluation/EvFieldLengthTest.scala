package es.uniovi.uo237525.dataquality.engine.rules.evaluation

import java.sql.{Date, Timestamp}
import java.time.Instant

import es.uniovi.uo237525.dataquality.SparkSessionTestWrapper
import org.scalatest.{BeforeAndAfterAll, FunSuite}

import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

class EvFieldLengthTest extends FunSuite with SparkSessionTestWrapper with BeforeAndAfterAll {

  private val ts = Timestamp.from(Instant.now())
  private val date = new Date(System.currentTimeMillis())
  private val map = Map("example1" -> "ex1", "example2" -> "ex2")
  private val mulArray = Array(Array("array of array1"), Array("array of array2"))
  private val seq = Seq(Row(1, Long.MaxValue, null, false, 0.5, ts, Array("val1", "val2"), mulArray, date, map),
    Row(2, Long.MaxValue, "value", null, null, ts, Array("val1", "val2"), mulArray, null, map),
    Row(3, null, "", true, 0.004, ts, null, null, date, null),
    Row(4, 3434343.toLong, "", true, 0.00344, ts, Array(), null, date, null),
    Row(null, null, "", null, null, null, null, null, null, null))

  val someSchema = StructType(
    StructField("int", IntegerType) ::
      StructField("long", LongType) ::
      StructField("string", StringType) ::
      StructField("boolean", BooleanType) ::
      StructField("double", DoubleType) ::
      StructField("timestamp", TimestampType) ::
      StructField("array", ArrayType(StringType)) ::
      StructField("multiple", ArrayType(ArrayType(StringType))) ::
      StructField("date", DateType) ::
      StructField("map", MapType(StringType, StringType)) :: Nil)

  val someDf: DataFrame = spark.createDataFrame(
    spark.sparkContext.parallelize(seq),
    StructType(someSchema))


  override def afterAll(): Unit = someDf.unpersist()

  /** TEST CHECK PRECONDITIONS */
  test("test CHECK PRE-CONDITIONS: application on un-existing field") {
    val opts = EvFieldLengthOptions(5)
    val rule = EvFieldLength(field = "non-existing", options = opts)
    assert(rule.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: application on existing field") {
    val opts = EvFieldLengthOptions(5)
    val rule = EvFieldLength(field = someDf.schema.fields.head.name, options = opts)
    assert(rule.checkPreconditions(someDf).isEmpty)
  }

  test("test QUALITY PRE-CONDITIONS: NEGATIVE MAX LENGTH OPTION") {
    val opts = EvFieldLengthOptions(-5)
    val rule = EvFieldLength(field = someDf.schema.fields.head.name, options = opts)
    assert(rule.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: >= 0 MAX LENGTH OPTION") {
    val opts0 = EvFieldLengthOptions(0)
    val rule = EvFieldLength(field = someDf.schema.fields.head.name, options = opts0)
    assert(rule.checkPreconditions(someDf).isEmpty)

    val opts1 = EvFieldLengthOptions(1)
    val rule1 = EvFieldLength(field = someDf.schema.fields.head.name, options = opts1)
    assert(rule1.checkPreconditions(someDf).isEmpty)
  }

  /** TEST APPLICATION OF THE RULE */

  test("test QUALITY APPLY: NUMERIC fields") {
    // int
    val intF = "int"
    val intMax = 5
    val ruleInt = EvFieldLength(field = intF, options = EvFieldLengthOptions(intMax))
    val matchedInt = ruleInt.apply(someDf).get
    assert(matchedInt.matched == someDf.filter(length(col(intF)) <= intMax).filter(col(intF).isNotNull).count())

    // long
    val longF = "long"
    val longMax = 7
    val ruleLong = EvFieldLength(field = longF, options = EvFieldLengthOptions(longMax))
    val matchedLong = ruleLong.apply(someDf).get
    assert(matchedLong.matched == someDf.filter(length(col(longF)) <= longMax).filter(col(longF).isNotNull).count())

    // decimal / double
    val doubleF = "double"
    val doubleMax = 3
    val ruleDouble = EvFieldLength(field = doubleF, options = EvFieldLengthOptions(doubleMax))
    val matchedDouble = ruleDouble.apply(someDf).get
    assert(matchedDouble.matched == someDf.filter(length(col(doubleF)) <= doubleMax).filter(col(doubleF).isNotNull).count())
  }

  test("test QUALITY APPLY: STRING && BOOLEAN && TIMESTAMP fields") {
    // str
    val strF = "string"
    val strMax = 0
    val ruleStr = EvFieldLength(field = strF, options = EvFieldLengthOptions(strMax))
    val matchedStr = ruleStr.apply(someDf).get
    assert(matchedStr.matched == someDf.filter(length(col(strF)) <= strMax).filter(col(strF).isNotNull).count())

    // bool
    val boolF = "boolean"
    val boolMax = 0
    val ruleBool = EvFieldLength(field = boolF, options = EvFieldLengthOptions(boolMax))
    val matchedBool = ruleBool.apply(someDf).get
    assert(matchedBool.matched == someDf.filter(length(col(boolF)) <= boolMax).filter(col(boolF).isNotNull).count())

    // timestamp
    val tsF = "timestamp"
    val tsMax = 0
    val ruleTs = EvFieldLength(field = tsF, options = EvFieldLengthOptions(tsMax))
    val matchedTs = ruleTs.apply(someDf).get
    assert(matchedTs.matched == someDf.filter(length(col(tsF)) <= tsMax).filter(col(tsF).isNotNull).count())
  }

  test("test QUALITY APPLY: ARRAY && MAP fields") {
    // array
    val arrayF = "array"
    val arrayMax = 1
    val ruleArray = EvFieldLength(field = arrayF, options = EvFieldLengthOptions(arrayMax))
    val matchedArray = ruleArray.apply(someDf).get
    assert(matchedArray.matched == someDf.filter(size(col(arrayF)) <= arrayMax).filter(col(arrayF).isNotNull).count())

    // multiple array
    val mulArrayF = "multiple"
    val mulArrayMax = 5
    val ruleMulArray = EvFieldLength(field = mulArrayF, options = EvFieldLengthOptions(mulArrayMax))
    val matchedMulArray = ruleMulArray.apply(someDf).get
    assert(matchedMulArray.matched == someDf.filter(size(col(mulArrayF)) <= mulArrayMax).filter(col(mulArrayF).isNotNull).count())

    // map
    val mapF = "map"
    val mapMax = 0
    val ruleMap = EvFieldLength(field = mapF, options = EvFieldLengthOptions(mapMax))
    val matchedMap = ruleMap.apply(someDf).get
    assert(matchedMap.matched == someDf.filter(size(col(mapF)) <= mapMax).filter(col(mapF).isNotNull).count())
  }



}
