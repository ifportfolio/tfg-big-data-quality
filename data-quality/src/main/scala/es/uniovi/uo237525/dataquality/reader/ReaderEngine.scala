package es.uniovi.uo237525.dataquality.reader

import java.io.ByteArrayOutputStream

import org.apache.log4j.Logger

import scala.util.{Failure, Success, Try}
import es.uniovi.uo237525.dataquality.parser.models.InputParams
import org.apache.spark.sql.DataFrame
import es.uniovi.uo237525.dataquality.InputTypes
import es.uniovi.uo237525.dataquality.LogConstants.{Level, LogType}
import es.uniovi.uo237525.dataquality.logmanager.LogManager
import es.uniovi.uo237525.dataquality.reader.sources.{CSVReader, HiveReader, Reader}

/**
  * Manages all the reading process.
  * It is the responsible for creating the suitable reader for the data source and to read using it.
  */
class ReaderEngine(inputParams: InputParams) {

  val logger: Logger = Logger.getRootLogger

  /**
    * Creates the reader and uses it to read the data.
    *
    * @return a dataframe containing the data from the source
    */
  def read(): DataFrame = {
    Try {
      val ds = createReader
      ds.createDF
    } match {
      case Success(df) =>
        val outCaptureDfPrintSchema = new ByteArrayOutputStream
        Console.withOut(outCaptureDfPrintSchema) {
          df.printSchema()
        }
        val dfPrintSchema = new String(outCaptureDfPrintSchema.toByteArray)
        LogManager.addGenericLog(Level.DEBUG, LogType.INPUT, dfPrintSchema)
        LogManager.addGenericLog(Level.INFO, LogType.INPUT, "Successfully read input data operation.")
        df
      case Failure(exception) =>
        LogManager.addGenericLog(Level.FATAL, LogType.INPUT, exception.getMessage)
        throw exception
    }
  }

  /**
    * Creates the suitable reader based on the input parameters provided in the constructos
    *
    * @throws IllegalArgumentException in case any mandatory field is missing
    *                                  or in case no reader can be created from the input parameters
    * @return an implementation of a reader
    */
  def createReader: Reader = {
    val inputType = inputParams.source.toLowerCase

    inputType match {
      case InputTypes.CSV =>
        val r = CSVReader(inputParams)
        logger.warn("Creating CSV reader")
        LogManager.addGenericLog(Level.INFO, LogType.INPUT, "Successfully created CSV reader.")
        r
      case InputTypes.HIVE =>
        val r = HiveReader(inputParams)
        logger.warn("Creating Hive reader")
        LogManager.addGenericLog(Level.INFO, LogType.INPUT, "Successfully created HIVE reader.")
        r
      case _ => throw new IllegalArgumentException(s"Provided reader '$inputType' is not supported.")
    }
  }
}
