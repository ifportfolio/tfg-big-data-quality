package es.uniovi.uo237525.dataquality.alert

import es.uniovi.uo237525.dataquality.configuration.Configuration
import es.uniovi.uo237525.dataquality.logmanager.Log
import scala.util.Try

/**
  * Interface that represents the structure that must be followed by any sender
  *
  * @author Ignacio Fernandez Fernandez
  */
trait AlertSender {

  val config: Configuration

  def sendAlert(logs: Iterable[Log]): Try[Unit]

}
