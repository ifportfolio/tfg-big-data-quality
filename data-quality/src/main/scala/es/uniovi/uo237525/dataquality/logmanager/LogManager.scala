package es.uniovi.uo237525.dataquality.logmanager

import java.io.{BufferedWriter, File, FileWriter}
import java.text.SimpleDateFormat

import es.uniovi.uo237525.dataquality.logmanager.logs.GenericLog
import org.json4s.DefaultFormats
import org.json4s.native.Serialization.write
import org.json4s.native.Serialization.writePretty
import es.uniovi.uo237525.dataquality.LogConstants.Level
import es.uniovi.uo237525.dataquality.LogConstants.Level.Level
import es.uniovi.uo237525.dataquality.LogConstants.LogType.LogType

import scala.collection.mutable.ListBuffer
import scala.util.Try

/**
  * A singleton object that manages all the logsproducedin the different process of the application
  */
object LogManager {

  val logs: ListBuffer[Log] = new ListBuffer[Log]()

  /**
    * Adds a log to the logs list
    */
  def addLog(res: Log): Boolean = {
    Try(logs.append(res)).isSuccess
  }

  /**
    * Adds a generic log representing the outcome of an specific activity
    *
    * @param level The granurality of the log.
    * @param info  Verbose information specifying the log.
    */
  def addGenericLog(level: Level, logType: LogType, info: String): Boolean = {
    addLog(GenericLog(level, info, logType))
  }


  /**
    * Orders the list by the timestamp of its creation from older to newer.
    *
    * @param logs list to order
    * @return a new list containing the same elements ordered from older to newer creation timestamp
    */
  def orderByTimestamp(logs: List[Log]): List[Log] = {
    logs.sortWith(_.timestamp.getTime < _.timestamp.getTime)
  }

  /**
    * Returns all the logs with the same or higher granularity that the provided one.
    *
    * @param level granularity level to filter with
    * @param logs  list of logs that will be filtered
    * @return a filtered list
    */
  def getLogsByLevel(level: Level, logs: List[Log]): List[Log] = {
    level match {
      case Level.DEBUG => logs
      case Level.INFO => logs.filter(l => !l.level.equals(Level.DEBUG))
      case Level.WARN => logs.filter(l => !l.level.equals(Level.DEBUG) && !l.level.equals(Level.INFO))
      case Level.ERROR => logs.filter(l => !l.level.equals(Level.DEBUG) && !l.level.equals(Level.INFO) && !l.level.equals(Level.WARN))
      case Level.FATAL => logs.filter(l => l.level.equals(Level.FATAL))
    }
  }

  /**
    * Returns all the logs with the same or higher granularity that the provided one.
    *
    * @param level granularity level to filter with
    * @param logs  list of logs that will be filtered
    * @return a filtered list
    */
  def getLogsByLevel(level: String, logs: List[Log]): List[Log] = {
    val levels = Level.values.toList.map(l => (l.toString.toLowerCase(), l))
    val mapLevel = levels.find(pair => pair._1.equals(level.toLowerCase()))

    if (mapLevel.isDefined) getLogsByLevel(mapLevel.get._2, logs)
    else List()
  }

  /**
    * Produces a JSON string from the list of logs
    *
    * The pattern used for the dates is "yyyy-MM-dd, HH:mm:ss:SSS"
    *
    * @param logs
    * @return
    */
  def produceJsonFromLogs(logs: List[Log]): String = {
    //    implicit val formats = DefaultFormats
    implicit val formats = new DefaultFormats {
      override def dateFormatter = new SimpleDateFormat("yyyy-MM-dd, HH:mm:ss:SSS")
    }
    write(logs)
  }

  /**
    * Produces a JSON content report file.
    *
    * @param file
    */
  def produceJsonReport(file: File): Unit = {
    implicit val formats = new DefaultFormats {
      override def dateFormatter = new SimpleDateFormat("yyyy-MM-dd, HH:mm:ss:SSS")
    }
    val fileContent = writePretty(logs)
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(fileContent)
    bw.close()
  }

}
