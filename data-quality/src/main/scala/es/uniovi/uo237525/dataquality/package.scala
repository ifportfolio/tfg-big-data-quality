package es.uniovi.uo237525

import java.time.Instant

/**
  * Package object that serves as global constants/variable definition site.
  * Contains the following elements:
  *   - SPARK_JOB_DEFAULT_NAME: The default name used for every defined planned spark job.
  *   - EXEC_UNIQUE_ID: The global identifier for the curent execution. EpochMillis of the execution start.
  *   - CommandLineConstants: Container for all the constants related to the command line arguments.
  *   - InputTypes
  *   - OutputTypes
  *   - HiveConstants
  *   - LogConstants
  *   - ConfigFileConstants
  *
  * @author Ignacio Fernandez Fernandez
  */
package object dataquality {

  val SPARK_JOB_DEFAULT_NAME: String = "big-data-quality"
  val EXEC_UNIQUE_ID: String = Instant.now().toEpochMilli.toString
  val CONFIG_DEFAULT_NAME = "default.conf"

  /**
    * Container for all the constants related to the command line arguments.
    *   -  RULE_ARG_SHORT: The short abbreviation for the rules prefix command.
    *   -  CONFIG_ARG_SHORT: The short abbreviation for the config prefix command.
    */
  object CommandLineConstants {
    val RULE_ARG_SHORT = 'r'
    val CONFIG_ARG_SHORT = 'c'
  }


  /**
    * Enumeration for the available input types in the application.
    * Currently:
    *   - CSV
    *   - HIVE
    */
  object InputTypes extends Enumeration {
    type InputType = Value
    val CSV: String = "csv"
    val HIVE: String = "hive"
  }

  /**
    * Enumeration for the available output types in the application.
    * Currently:
    *   - CSV
    *   - HIVE
    */
  object OutputTypes extends Enumeration {
    type OutputType = Value
    val CSV: String = InputTypes.CSV
    val HIVE: String = InputTypes.HIVE
  }

  /**
    * Constants that are al related to Hive:
    *   - SPARK_WAREHOUSE_DIR_HEADER: Accepted Spark property header for the specification of a custom warehouse dir for hive.
    *   - HIVE_METASTORE_URIS_HEADER  Accepted Spark property header for the specification of a custom metastore location for hive.
    *   - SPARK_CATALOG_IMPLEMENTATION_HEADER: Accepted Spark property header for the specification of the Spark catalog.
    *
    * @see https://spark.apache.org/docs/latest/sql-data-sources-hive-tables.html
    */
  object HiveConstants {
    val SPARK_WAREHOUSE_DIR_HEADER = "spark.sql.warehouse.dir"
    val HIVE_METASTORE_URIS_HEADER = "hive.metastore.uris"
    val SPARK_CATALOG_IMPLEMENTATION_HEADER = "spark.sql.catalogImplementation"
  }

  /**
    * Properties related to the logs part of the bdq component.
    */
  object LogConstants {

    /**
      * Enumeration of all possible log granularity levels:
      *   - DEBUG
      *   - INFO
      *   - WARN
      *   - ERROR
      *   - FATAL
      */
    object Level extends Enumeration {
      type Level = Value

      val DEBUG: LogConstants.Level.Value = Value("DEBUG")
      val INFO: LogConstants.Level.Value = Value("INFO")
      val WARN: LogConstants.Level.Value = Value("WARN")
      val ERROR: LogConstants.Level.Value = Value("ERROR")
      val FATAL: LogConstants.Level.Value = Value("FATAL")
    }

    /**
      * Enumeration of all possible log types:
      *   - CONFIG
      *   - INPUT
      *   - RULE
      *   - OUTPUT
      *   - ALERT
      */
    object LogType extends Enumeration {
      type LogType = Value

      val CONFIG: LogConstants.LogType.Value = Value("CONFIG")
      val INPUT: LogConstants.LogType.Value = Value("INPUT")
      val RULE: LogConstants.LogType.Value = Value("RULE")
      val OUTPUT: LogConstants.LogType.Value = Value("OUTPUT")
      val ALERT: LogConstants.LogType.Value = Value("ALERT")
    }

  }

  /**
    * Specification of all the defined properties headers and sections allowed to be defined for the configuration.
    * Generic property without section:
    *   - CONFIG_DEFAULT_NAME: The name for the default config file localted under the resources folder.*
    * Sections:
    *   - SmtpProperties
    *   - KafkaProperties
    *   - RedisProperties
    */
  object ConfigFileConstants {
    val REPORT_EXPORT_DIR = "report.export.dir"

    /**
      * SMTP Properties definition in the config file:
      *  - MAIL_AUTH
      *  - MAIL_USER
      *  - MAIL_PASS
      */
    object SmtpProperties {
      val SMTP_HEADER = "mail.smtp"
      val MAIL_AUTH = s"$SMTP_HEADER.auth"
      val MAIL_USER = s"$SMTP_HEADER.username"
      val MAIL_PASS = s"$SMTP_HEADER.password"
    }

    /**
      * Kafka Properties definition in the config file.
      */
    object KafkaProperties {
      val KAFKA_HEADER = "kafka"
    }

    /**
      * REDIS Properties definition in the config file:
      *  - REDIS_HOST: Host for the Redis server/cluster
      *  - REDIS_PORT: Port for the Redis port.
      */
    object RedisProperties {
      val REDIS_HEADER = "redis"
      val REDIS_HOST = s"$REDIS_HEADER.host"
      val REDIS_PORT = s"$REDIS_HEADER.port"
    }

  }

}
