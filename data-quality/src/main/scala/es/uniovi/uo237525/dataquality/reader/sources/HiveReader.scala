package es.uniovi.uo237525.dataquality.reader.sources

import es.uniovi.uo237525.dataquality.HiveConstants._
import es.uniovi.uo237525.dataquality.LogConstants.{Level, LogType}
import es.uniovi.uo237525.dataquality.logmanager.LogManager
import es.uniovi.uo237525.dataquality.parser.models.InputParams
import es.uniovi.uo237525.dataquality.utils.SparkUtils
import es.uniovi.uo237525.dataquality.{OutputTypes, _}
import org.apache.log4j.Logger
import org.apache.spark.sql.DataFrame

import scala.util.{Failure, Success}


/**
  * Reader implemented to extract the data from a Hive table
  *
  * @constructor creates a new instance of a Hive reader associating a set of input parameters
  *              that will be used later to read the data.
  * @param inputParams the params used by the reader to extract the desired data.
  */
case class HiveReader(inputParams: InputParams) extends Reader {

  val logger: Logger = Logger.getRootLogger


  /**
    * Reads the data from the specified Hive source.
    *
    * The following parameters are used by the reader to extract the data
    *
    * - hive.metastore.uris (optional)
    * - spark.sql.warehouse.dir (optional)
    * - hive.database (optional)
    * - hive.tableName (mandatory)
    *
    * @return a dataframe containing the data stored in the source
    */
  override def createDF: DataFrame = {

    // metastore
    val properties = inputParams.hive_metastore_uris match {
      case Some(x) =>
        logger.warn(s"""Adding property "$HIVE_METASTORE_URIS_HEADER".""")
        LogManager.addGenericLog(Level.DEBUG, LogType.INPUT, s"""Adding property "$HIVE_METASTORE_URIS_HEADER = $x".""")
        Map(HIVE_METASTORE_URIS_HEADER -> x)
      case None =>
        LogManager.addGenericLog(Level.DEBUG, LogType.INPUT, s"Inheriting $HIVE_METASTORE_URIS_HEADER from default Spark Hive configuration.")
        Map[String, String]()
    }

    // warehouse dir
    val propertiesWithWarehouse = inputParams.hive_warehouse_dir match {
      case Some(loc) =>
        logger.warn(s"""Adding property "$SPARK_WAREHOUSE_DIR_HEADER".""")
        LogManager.addGenericLog(Level.DEBUG, LogType.INPUT, s"""Adding property "$SPARK_WAREHOUSE_DIR_HEADER = $loc".""")
        properties + (SPARK_WAREHOUSE_DIR_HEADER -> loc)
      case None =>
        LogManager.addGenericLog(Level.DEBUG, LogType.INPUT, s"Inheriting $SPARK_WAREHOUSE_DIR_HEADER from default Spark Hive configuration.")
        properties
    }

    if (propertiesWithWarehouse.nonEmpty) {
      LogManager.addGenericLog(Level.DEBUG, LogType.INPUT,
        s"Hive reader configuration: ${propertiesWithWarehouse.toList.map(el => s"${el._1} -> ${el._2}").mkString("\n")}")
    }

    val session = SparkUtils.getSparkSession(enableHiveSupport = true,
      appName = s"$SPARK_JOB_DEFAULT_NAME-${OutputTypes.HIVE}-reader",
      propertiesWithWarehouse)

    // database
    val dbase = inputParams.hive_database match {
      case Some(db) =>
        SparkUtils.changeSessionDatabase(session, db) match {
          case Success(_) =>
            logger.warn(s"""Using DB: "$db".""")
            LogManager.addGenericLog(Level.DEBUG, LogType.INPUT, s"Using DB: $db for reading")
            db
          case Failure(exception) => throw exception
        }
      case None =>
        val current = SparkUtils.getCurrentDatabase(session)
        logger.warn(s"""Using DB: "$current".""")
        LogManager.addGenericLog(Level.WARN, LogType.INPUT, s"Unspecified DB, using current Hive one: $current for reading")
        current
    }

    // table
    val tableName = inputParams.hive_table.getOrElse(
      throw new IllegalArgumentException(s"""Missing mandatory input HIVE param "hive.tableName"."""))

    LogManager.addGenericLog(Level.INFO, LogType.INPUT, s"Reading data from table: $dbase.$tableName")

    logger.warn(s"""Reading data from $tableName.""")

    val df = session.sqlContext.table(tableName)
    df
  }

}

