package es.uniovi.uo237525.dataqualityserver.service

import java.io.{BufferedWriter, File, FileWriter}

import org.apache.logging.log4j.LogManager
import org.springframework.stereotype.Service

/**
  * Provides functionality related to files generation.
  */
@Service
class FileSystemService {

  private val logger = LogManager.getRootLogger


  /**
    * Generates a file using the provided path and the content
    *
    * @param filePath the path where the file will be generated
    * @param content  the content for the generated file
    * @return a File object that represents the once that was just created
    */
  def createFile(filePath: String, content: String): File = {
    val file = new File(filePath)
    logger.warn(s"Generating file -> ${file.getCanonicalPath} with ${content.length} content chars")
    val bw = new BufferedWriter(new FileWriter(file))
    bw.write(content)
    bw.close()
    file
  }

}
