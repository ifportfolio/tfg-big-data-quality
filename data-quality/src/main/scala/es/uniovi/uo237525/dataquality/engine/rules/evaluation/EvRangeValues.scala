package es.uniovi.uo237525.dataquality.engine.rules.evaluation

import java.util.UUID

import es.uniovi.uo237525.dataquality.LogConstants.Level
import es.uniovi.uo237525.dataquality.engine.EvaluationInfo
import es.uniovi.uo237525.dataquality.engine.rules.{EvaluationRule, RuleOptions}
import scala.util.Try

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StringType

case class EvRangeValuesOptions(accepted: Array[String], caseSensitive: Boolean = false) extends RuleOptions

/**
  * The purpose of this rule is to evaluate whether or not the value of the field record belongs to a specified list of elements.
  * Can only be applied to String values
  *
  * @param id      Identifier for the rule. Automatically generated if not provided
  * @param field   Field in which the rule is going to be applied
  * @param options EvDateFormatOptions needed for the rule application.
  */
case class EvRangeValues(id: String = UUID.randomUUID().toString,
                         field: String,
                         options: EvRangeValuesOptions) extends EvaluationRule {

  val accepted = options.accepted
  val caseSensitive = options.caseSensitive


  /**
    * Checks the needed preconditions for the rule.
    *
    * The preconditions that need to be fulfilled are:
    *   - The field exists
    *   - The field is of type StringType
    *   - The list containing all the possible values is not empty
    *
    * @param df the dataframe to evaluate
    * @return The error string in case a condition is not fulfilled. None otherwise
    */
  override def checkPreconditions(df: DataFrame): Option[String] = {
    val exists = Try(df(field)).isSuccess
    if (!exists) {
      val errorMsg = s"Field $field does not exists."
      return Some(errorMsg)
    }

    val columnType = df.select(field).schema.head.dataType
    columnType match {
      case StringType => None
      case _ => return Some(s"This rule can not be applied to fields of type $columnType")
    }

    if (accepted.isEmpty) Some("List of accepted values is empty")
    else None
  }

  /**
    * Calculates the number of records in a field that are not in the specified set of acceptable values.
    *
    * Null records are included as non-matched by the rule.
    *
    * @param df the base dataframe where the rule is going to be applied
    * @return A pair containing the resulting DataFrame and
    *         the results of the application of the rule
    */
  override def apply(df: DataFrame): Try[EvaluationInfo] = Try {
    val totalRows = df.select(field).count()
    val isInSetUdf = udf((value: String) => {
      if (caseSensitive) accepted.contains(value)
      else accepted.map(_.toLowerCase()).contains(value.toLowerCase())
    })
    val countRecordsAccepted = df.select(field).filter(isInSetUdf(col(field))).count()

    EvaluationInfo(Level.INFO,
      s"Accepted values set: ${accepted.mkString("[", ",", "]")}",
      countRecordsAccepted,
      totalRows)
  }

}
