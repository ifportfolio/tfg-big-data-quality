package es.uniovi.uo237525.dataquality.engine.rules.remediation

import java.text.NumberFormat
import java.util.{Locale, UUID}

import es.uniovi.uo237525.dataquality.LogConstants.Level
import es.uniovi.uo237525.dataquality.engine.RemediationInfo
import es.uniovi.uo237525.dataquality.engine.rules.{RemediationRule, RuleOptions}
import es.uniovi.uo237525.dataquality.utils.{SparkUtils, Utils}
import scala.util.Try
import scala.util.matching.Regex

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

/**
  * TODO
  *
  * @param newThouSep
  * @param newDecSep
  * @param newDecMax
  * @param newDecMin
  */
case class RemNumericFormatOptions(newThouSep: String, newDecSep: String,
                                   newDecMax: Int, newDecMin: Int = 0) extends RuleOptions

/**
  * This remediation rule, replaces all the records ina field that do not match a provided numeric format.
  *
  * @param id      Identifier for the rule. Automatically generated if not provided
  * @param field   Field in which the rule is going to be applied
  * @param options RemNumericFormatOptions needed for the rule application.
  */
case class RemNumericFormat(id: String = UUID.randomUUID().toString,
                            field: String,
                            options: RemNumericFormatOptions) extends RemediationRule {

  private val NEGATIVE_CHAR = '-'

  /**
    * Builds the regular expression needed to check the numeric format of the values.
    * The used RegEx for the integer part is:
    *   - If an thousSep is provided it will use: (^-?(?!0)\d{1,3}($formattedThousSep\d{3})*...
    *   - Else ^-?\d+...
    * Then for the decimal part:
    *   - if can have or have not decimals: ($formattedDecSep\d{1,$maxDecimals})?$)|(^0($formattedDecSep\d{1,$maxDecimals})?$)
    *   - if exactly 0 decimals
    *   - if decimals between [0, >0]: ...($formattedDecSep\d{1,$maxDecimals})?$)|(^0($formattedDecSep\d{$minDecimals,$maxDecimals})?$)
    *   - if decimals between [>0, >0] ... $formattedDecSep\d{$minDecimals,$maxDecimals}$)|(^0$formattedDecSep\d{$minDecimals,$maxDecimals}$)
    *
    *
    * @param thousandsSep the separator used for the thousands
    * @param decimalSep   the separator used for the decimals
    * @param minDecimals  the min number of decimals the number must have
    * @param maxDecimals  the max number of decimals the number is allowed to have
    * @return the regular expression used to evaluate the values.
    */
  private def getNumericFormatRegex(thousandsSep: String, decimalSep: String, minDecimals: Int, maxDecimals: Int): Regex = {
    val formattedDecSep = Utils.escapeSpecialRegexChars(decimalSep)
    val formattedThousSep = Utils.escapeSpecialRegexChars(thousandsSep)

    val sb = new StringBuilder()
    // if provided thousSep
    if (formattedThousSep.length > 0) {
      sb.append(s"""(^-?(?!0)\\d{1,3}($formattedThousSep\\d{3})*""")
    }
    // not provided thousSep
    else {
      sb.append("""(^-?\d+""")
    }
    // if can have no decimals (
    if (minDecimals == 0) {
      // can have or have not decimals
      if (maxDecimals > 0) {
        sb.append(s"""($formattedDecSep\\d{1,$maxDecimals})?$$)|(^0($formattedDecSep\\d{1,$maxDecimals})?$$)""")
      }
      // only non decimal numbers (we include the 0 here)
      else {
        sb.append(s"""$$)|(^0$$)""")
      }
    }
    else {
      // always have decimals (and because of the precondition,the ax decimals always >= minDecimals
      //      if (maxDecimals > 0) {
      sb.append(s"""$formattedDecSep\\d{$minDecimals,$maxDecimals}$$)|(^0$formattedDecSep\\d{$minDecimals,$maxDecimals}$$)""")

    }
    sb.toString().r
  }

  /**
    * Checks the needed preconditions for the rule.
    *
    * The preconditions that need to be fulfilled are:
    *   - The field exists
    *   - The provided field is of StringType
    *   - The numeric parameters are >= 0
    *   - The options newDecMin <= newDecMax
    *   - The options newDecSep and newThouSep are not null.
    *   - The options newDecSep and newThouSep are not equal.
    *
    * @param df the dataframe to evaluate
    * @return The error string in case a condition is not fulfilled. None otherwise
    */
  override def checkPreconditions(df: DataFrame): Option[String] = {
    // checking that the fields exists
    val res = Try(df(field)).isSuccess
    if (!res) {
      return Some(s"Field $field does not exists.")
    }

    // checking the valid types of the column
    val columnType = df.select(field).schema.head.dataType
    columnType match {
      case StringType => None
      case _ =>
        val errorMsg = "Rule cannot be applied to fields of type: \"" + columnType + "\"."
        return Some(errorMsg)
    }

    if (options.newDecMax < 0) return Some("'newDecMax' must be equal or greater than 0.")
    if (options.newDecMin < 0) return Some("'newDecMin' must be equal or greater than 0.")
    if (options.newDecMin > options.newDecMax) return Some("'newDecMax' must be equal or greater than 'newDecMin'.")
    if (options.newDecSep == null) return Some(s"""Provided decimal separator '${options.newDecSep}' is not valid.""")

    if (options.newThouSep == null) return Some(s"""Provided thousands separator '${options.newThouSep}' is not valid.""")

    if (options.newThouSep.length > 0 && options.newThouSep.equals(options.newDecSep)) Some(s"""Separators can not be the same.""")
    else None
  }

  /**
    * TODO
    *
    * @param df the base dataframe where the rule is going to be applied
    * @return A pair containing the resulting DataFrame and
    *         the results of the application of the rule
    */
  override def apply(df: DataFrame): Try[RemediationInfo] = Try {
    val spark = SparkUtils.getSparkSession()
    import spark.implicits._

    val newDecSep = options.newDecSep
    val newThousSep = options.newThouSep
    val newDecMax = options.newDecMax
    val newDecMin = options.newDecMin

    val regex = getNumericFormatRegex(newThousSep, newDecSep, newDecMin, newDecMax)

    // udf
    val replaceUdf = udf((value: String) => {
      // check the value is not empty or null
      if (value != null && value.length > 0 && regex.findFirstIn(value).isEmpty && value.filter(_.isDigit).length > 0) {
        // get the number of different chars apart from digits present in the string
        val isNegative = value.head == NEGATIVE_CHAR
        val charsInValue = value.filterNot(c => c.isDigit || c == NEGATIVE_CHAR).groupBy(_.toLower).keySet.toList
        val numChars = charsInValue.size
        // in range [0,2] is possible to parse it
        if (numChars >= 0 && numChars <= 2) {
          val double = numChars match {
            // create double from digits
            case 0 =>
              if (isNegative) value.filter(_.isDigit).toDouble * (-1)
              else value.filter(_.isDigit).toDouble
            // get the number of decimals and parse the double using US locale
            case _ =>
              val pair = value.reverse.zipWithIndex.find(!_._1.isDigit).get
              val digits = value.filter(_.isDigit)
              val index = digits.length - pair._2
              val intPart = digits.substring(0, index)
              val decimalPart = digits.substring(index)
              val usFormatter = NumberFormat.getNumberInstance(Locale.US)
              if (isNegative) usFormatter.parse(s"$NEGATIVE_CHAR$intPart.$decimalPart").doubleValue()
              else usFormatter.parse(s"$intPart.$decimalPart").doubleValue()
          }
          val stdUSThousandsSep = ","
          val stdUSDecimalsSep = "."
          val auxThSep = "auxThSep"
          val auxDecSep = "auxDecSep"

          // set number of decimals
          val usFormatter = NumberFormat.getNumberInstance(Locale.US)
          usFormatter.setMaximumFractionDigits(newDecMax)
          usFormatter.setMinimumFractionDigits(newDecMin)

          // make sure there is no confusion in case some separators match the previous ones when replacing
          val usFormattedWithDecimals = usFormatter.format(double)
            .replace(stdUSThousandsSep, auxThSep)
            .replace(stdUSDecimalsSep, auxDecSep)

          // substitute the new thousands separator
          val thousandsFormatted = usFormattedWithDecimals.replace(auxThSep, newThousSep)
          // substitute the new decimal separator
          (thousandsFormatted.replace(auxDecSep, newDecSep), true)
        }
        else {
          (value, false)
        }
      }
      else (value, false)
    })

    val nomRemField = s"${UUID.randomUUID()}"
    val dfWithCalc = df.withColumn(field, replaceUdf(col(field)))
      .withColumn(nomRemField, $"$field._2").withColumn(field, $"$field._1")

    // calculate num replaced values
    val numRemValues = dfWithCalc.filter(dfWithCalc(nomRemField) === true).count()

    val modifiedDf = dfWithCalc.drop(nomRemField)
    val totalRows = df.select(field).count()

    RemediationInfo(Level.INFO,
      s"New Format (TSep: '$newThousSep', DSep: '$newDecSep', NumDec: $newDecMax).",
      numRemValues,
      totalRows,
      modifiedDf)
  }
}
