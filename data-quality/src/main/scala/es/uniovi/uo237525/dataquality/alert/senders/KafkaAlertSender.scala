package es.uniovi.uo237525.dataquality.alert.senders

import java.util.Properties
import es.uniovi.uo237525.dataquality.EXEC_UNIQUE_ID
import es.uniovi.uo237525.dataquality.ConfigFileConstants.KafkaProperties
import es.uniovi.uo237525.dataquality.alert.AlertSender
import es.uniovi.uo237525.dataquality.configuration.Configuration
import es.uniovi.uo237525.dataquality.logmanager.{Log, LogManager}
import es.uniovi.uo237525.dataquality.parser.models.KafkaAlert
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.log4j.Logger

import scala.util.Try

/**
  * This class is used to send all the alerts through an Apache Kafka server.
  * The alert message will be generated in JSON format containing all the logs associated to the specified log level.
  *
  * @param config the configuration that contains all the parameters needed to send configure the sender.
  * @param al     the mail alert instance that contains information to send the alert
  * @author Ignacio Fernandez Fernandez
  */
case class KafkaAlertSender(config: Configuration, al: KafkaAlert) extends AlertSender {

  val logger: Logger = Logger.getRootLogger

  /**
    * Sends all the log alerts through kafka.
    * It uses the configured properties for the kafka server ("kafka.bootstrap-servers", "kafka.group.id"...) and the specific
    * information provided for the kafka alert in the constructor, such as the topic.
    *
    * @param logs all the logs that are going to be sent.
    * @return a Try Unit element that returns:
    *         - Success in case the message is successfully produced to the topic.
    *         - Failure if either the configuration or the sending process throws an exception.
    */
  override def sendAlert(logs: Iterable[Log]): Try[Unit] = Try {
    if (logs.nonEmpty && al.enabled) {
      val properties = new Properties()
      logger.warn("Configuring Kafka alerts")
      config.getChildPropertiesOptionFromPath(KafkaProperties.KAFKA_HEADER, includePrefix = false).getOrElse(Map()).foreach {
        case (key, value) =>
          logger.warn(s"$key -> $value")
          properties.put(key, value.toString)
      }

      val producer = new KafkaProducer[String, String](properties)
      val sentTry = Try({
        val record = new ProducerRecord(al.topic, EXEC_UNIQUE_ID, LogManager.produceJsonFromLogs(logs.toList))
        producer.send(record)
      })
      producer.close()
      sentTry.get
    }
    else {
      if (!al.enabled) {
        logger.error("Alert not enabled")
        throw new Exception("Alert not enabled")
      }
      if (logs.isEmpty) {
        throw new Exception("No logs to send in this alert, aborted send operation")
      }
    }
  }
}
