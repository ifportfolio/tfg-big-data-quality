package es.uniovi.uo237525.dataquality.engine.rules.remediation

import java.sql.{Date, Timestamp}
import java.text.SimpleDateFormat
import java.time.Instant

import es.uniovi.uo237525.dataquality.SparkSessionTestWrapper
import org.scalatest.FunSuite

import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

class FillEmptyRecordsTest extends FunSuite with SparkSessionTestWrapper {

  import spark.implicits._

  private val ts = Timestamp.from(Instant.now())
  private val date = new Date(System.currentTimeMillis())
  private val map = Map("example1" -> "ex1", "example2" -> "ex2")
  private val mulArray = Array(Array("array of array1"), Array("array of array2"))
  private val seq = Seq(Row(1, Long.MaxValue, null, false, 0.5, ts, Array("val1", "val2"), mulArray, date, map),
    Row(2, Long.MaxValue, "value", null, null, ts, Array("val1", "val2"), mulArray, null, map),
    Row(3, null, "", true, 0.004, ts, null, null, date, null),
    Row(null, null, "", null, null, null, null, null, null, null))

  val someSchema = StructType(
    StructField("int", IntegerType) ::
      StructField("long", LongType) ::
      StructField("string", StringType) ::
      StructField("boolean", BooleanType) ::
      StructField("double", DoubleType) ::
      StructField("timestamp", TimestampType) ::
      StructField("array", ArrayType(StringType)) ::
      StructField("multiple", ArrayType(ArrayType(StringType))) ::
      StructField("date", DateType) ::
      StructField("map", MapType(StringType, StringType)) :: Nil)

  val someDf: DataFrame = spark.createDataFrame(
    spark.sparkContext.parallelize(seq),
    StructType(someSchema))

  private def testAllNullsAndEmptyReplaced(r: FillEmptyRecords, df: DataFrame) = {
    val nullsBefore = df.filter(col(r.field).isNull || length(col(r.field)) === lit(0)).count()
    assume(nullsBefore >= 0)
    val res = r.apply(df).get
    assert(res.modifiedDF.filter(col(r.field).isNull || length(col(r.field)) === lit(0)).count() == 0)
  }

  /** REMEDIATION PRE-CONDITIONS TEST */
  test("test REMEDIATION PRE-CONDITIONS: apply on un-existing field") {
    val rem: FillEmptyRecordsOption = FillEmptyRecordsOption("")
    val rule: FillEmptyRecords = FillEmptyRecords(field = "non-existing ", options = rem)
    val someDF: DataFrame = Seq(1, 2, 3).toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }
  test("test REMEDIATION PRE-CONDITIONS: defaultValue equal null") {
    val rem: FillEmptyRecordsOption = FillEmptyRecordsOption(null)
    val rule: FillEmptyRecords = FillEmptyRecords(field = "nums", options = rem)
    val someDF: DataFrame = Seq(1, 2, 3).toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }
  test("test REMEDIATION PRE-CONDITIONS: defaultValue equal None") {
    val rem: FillEmptyRecordsOption = FillEmptyRecordsOption(None)
    val rule: FillEmptyRecords = FillEmptyRecords(field = "nums", options = rem)
    val someDF: DataFrame = Seq(1, 2, 3).toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }
  test("test REMEDIATION PRE-CONDITIONS: String column") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "string", options = FillEmptyRecordsOption("default"))
    assert(r.checkPreconditions(someDf).isEmpty)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "string", options = FillEmptyRecordsOption(20.7))
    assert(r1.checkPreconditions(someDf).isEmpty)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "string",
      options = FillEmptyRecordsOption(Array(1, 2, 3, 4, 5, 6)))
    assert(r2.checkPreconditions(someDf).isEmpty)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "string",
      options = FillEmptyRecordsOption(Map("hello" -> 3)))
    assert(r3.checkPreconditions(someDf).isEmpty)
  }
  test("test REMEDIATION PRE-CONDITIONS: INT TYPE column with compatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "int", options = FillEmptyRecordsOption(-50))
    assert(r.checkPreconditions(someDf).isEmpty)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "int", options = FillEmptyRecordsOption("10"))
    assert(r1.checkPreconditions(someDf).isEmpty)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "int", options = FillEmptyRecordsOption("-23450"))
    assert(r2.checkPreconditions(someDf).isEmpty)
  }
  test("test REMEDIATION PRE-CONDITIONS: INT TYPE column with incompatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "int", options = FillEmptyRecordsOption(Array("50")))
    assert(r.checkPreconditions(someDf).isDefined)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "int", options = FillEmptyRecordsOption(date))
    assert(r1.checkPreconditions(someDf).isDefined)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "int", options = FillEmptyRecordsOption(Double.NaN))
    assert(r2.checkPreconditions(someDf).isDefined)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "int", options = FillEmptyRecordsOption(10.5))
    assert(r3.checkPreconditions(someDf).isDefined)

    val r4: FillEmptyRecords = FillEmptyRecords(field = "int", options = FillEmptyRecordsOption("10.5"))
    assert(r4.checkPreconditions(someDf).isDefined)
  }
  test("test REMEDIATION PRE-CONDITIONS: LONG TYPE column with compatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "long", options = FillEmptyRecordsOption(Integer.MAX_VALUE))
    assert(r.checkPreconditions(someDf).isEmpty)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "long", options = FillEmptyRecordsOption("10"))
    assert(r1.checkPreconditions(someDf).isEmpty)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "long", options = FillEmptyRecordsOption(Long.MinValue))
    assert(r2.checkPreconditions(someDf).isEmpty)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "long", options = FillEmptyRecordsOption(Long.MaxValue))
    assert(r3.checkPreconditions(someDf).isEmpty)
  }
  test("test REMEDIATION PRE-CONDITIONS: LONG TYPE column with incompatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "long", options = FillEmptyRecordsOption(Array("50")))
    assert(r.checkPreconditions(someDf).isDefined)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "long", options = FillEmptyRecordsOption(date))
    assert(r1.checkPreconditions(someDf).isDefined)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "long", options = FillEmptyRecordsOption(Double.NaN))
    assert(r2.checkPreconditions(someDf).isDefined)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "long", options = FillEmptyRecordsOption(10.5))
    assert(r3.checkPreconditions(someDf).isDefined)

    val r4: FillEmptyRecords = FillEmptyRecords(field = "long",
      options = FillEmptyRecordsOption(Timestamp.from(Instant.now())))
    assert(r4.checkPreconditions(someDf).isDefined)
  }
  test("test REMEDIATION PRE-CONDITIONS: DOUBLE TYPE column with compatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "double", options = FillEmptyRecordsOption(-50.23))
    assert(r.checkPreconditions(someDf).isEmpty)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "double", options = FillEmptyRecordsOption("10"))
    assert(r1.checkPreconditions(someDf).isEmpty)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "double", options = FillEmptyRecordsOption("-23450"))
    assert(r2.checkPreconditions(someDf).isEmpty)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "double", options = FillEmptyRecordsOption("-8.5"))
    assert(r3.checkPreconditions(someDf).isEmpty)

    val r4: FillEmptyRecords = FillEmptyRecords(field = "double", options = FillEmptyRecordsOption(Double.NaN))
    assert(r4.checkPreconditions(someDf).isEmpty)

    val r5: FillEmptyRecords = FillEmptyRecords(field = "double", options = FillEmptyRecordsOption(Double.MaxValue))
    assert(r5.checkPreconditions(someDf).isEmpty)
  }
  test("test REMEDIATION PRE-CONDITIONS: DOUBLE TYPE column with incompatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "double", options = FillEmptyRecordsOption(Array("50")))
    assert(r.checkPreconditions(someDf).isDefined)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "double", options = FillEmptyRecordsOption(date))
    assert(r1.checkPreconditions(someDf).isDefined)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "double",
      options = FillEmptyRecordsOption(Timestamp.from(Instant.now())))
    assert(r2.checkPreconditions(someDf).isDefined)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "double",
      options = FillEmptyRecordsOption("string value"))
    assert(r3.checkPreconditions(someDf).isDefined)
  }
  test("test REMEDIATION PRE-CONDITIONS: BOOLEAN TYPE column with compatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption(true))
    assert(r.checkPreconditions(someDf).isEmpty)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption(false))
    assert(r1.checkPreconditions(someDf).isEmpty)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption("true"))
    assert(r2.checkPreconditions(someDf).isEmpty)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption("false"))
    assert(r3.checkPreconditions(someDf).isEmpty)
  }
  test("test REMEDIATION PRE-CONDITIONS: BOOLEAN TYPE column with incompatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption(1.0))
    assert(r.checkPreconditions(someDf).isDefined)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption(0.0))
    assert(r1.checkPreconditions(someDf).isDefined)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption("0"))
    assert(r2.checkPreconditions(someDf).isDefined)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption("1"))
    assert(r3.checkPreconditions(someDf).isDefined)

    val r4: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption(0))
    assert(r4.checkPreconditions(someDf).isDefined)

    val r5: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption(1))
    assert(r5.checkPreconditions(someDf).isDefined)
  }
  test("test REMEDIATION PRE-CONDITIONS: TIMESTAMP TYPE column with compatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "timestamp", options = FillEmptyRecordsOption(ts))
    assert(r.checkPreconditions(someDf).isEmpty)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "timestamp", options = FillEmptyRecordsOption(ts.toString))
    assert(r1.checkPreconditions(someDf).isEmpty)
  }
  test("test REMEDIATION PRE-CONDITIONS: TIMESTAMP TYPE column with incompatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "timestamp",
      options = FillEmptyRecordsOption("08/01/1995"))
    assert(r.checkPreconditions(someDf).isDefined)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "timestamp",
      options = FillEmptyRecordsOption(ts.getTime))
    assert(r1.checkPreconditions(someDf).isDefined)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "timestamp", options = FillEmptyRecordsOption(date))
    assert(r2.checkPreconditions(someDf).isDefined)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "timestamp",
      options = FillEmptyRecordsOption(date.toString))
    assert(r3.checkPreconditions(someDf).isDefined)
  }
  test("test REMEDIATION PRE-CONDITIONS: DATE TYPE column with compatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "date",
      options = FillEmptyRecordsOption(new SimpleDateFormat("yyyy-MM-dd").format(date)))
    assert(r.checkPreconditions(someDf).isEmpty)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "date",
      options = FillEmptyRecordsOption(new SimpleDateFormat("yyyy-M-d").format(date)))
    assert(r1.checkPreconditions(someDf).isEmpty)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "date", options = FillEmptyRecordsOption("2019-02-28"))
    assert(r2.checkPreconditions(someDf).isEmpty)
  }
  test("test REMEDIATION PRE-CONDITIONS: DATE TYPE column with incompatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "date",
      options = FillEmptyRecordsOption(date.getTime))
    assert(r.checkPreconditions(someDf).isDefined)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "date", options = FillEmptyRecordsOption("hello"))
    assert(r1.checkPreconditions(someDf).isDefined)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "date",
      options = FillEmptyRecordsOption(new java.util.Date()))
    assert(r2.checkPreconditions(someDf).isDefined)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "date",
      options = FillEmptyRecordsOption(new java.util.Date().toString))
    assert(r3.checkPreconditions(someDf).isDefined)
  }
  test("test REMEDIATION PRE-CONDITIONS: any OTHER TYPE, is not allowed") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "array",
      options = FillEmptyRecordsOption(Array("v1", "v2")))
    assert(r.checkPreconditions(someDf).isDefined)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "map",
      options = FillEmptyRecordsOption(Map("v" -> 1)))

    assert(r1.checkPreconditions(someDf).isDefined)
  }

  /** REMEDIATION APPLICATION TEST */

  test("test REMEDIATION APPLY: apply on un-existing field") {
    val rem: FillEmptyRecordsOption = FillEmptyRecordsOption("")
    val rule: FillEmptyRecords = FillEmptyRecords(field = "non-existing ", options = rem)
    val someDF = Seq(1, 2, 3).toDF()
    assert(rule.apply(someDF).isFailure)
  }
  test("test REMEDIATION APPLY: defaultValue equal null") {
    val rem: FillEmptyRecordsOption = FillEmptyRecordsOption(null)
    val rule: FillEmptyRecords = FillEmptyRecords(field = "", options = rem)
    val someDF = Seq(1, 2, 3).toDF()
    assert(rule.apply(someDF).isFailure)
  }
  test("test REMEDIATION APPLY: defaultValue equal None") {
    val rem: FillEmptyRecordsOption = FillEmptyRecordsOption(None)
    val rule: FillEmptyRecords = FillEmptyRecords(field = "", options = rem)
    val someDF = Seq(1, 2, 3).toDF()
    assert(rule.apply(someDF).isFailure)
  }
  test("test REMEDIATION APPLY: String column") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "string", options = FillEmptyRecordsOption("default"))
    testAllNullsAndEmptyReplaced(r, someDf)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "string", options = FillEmptyRecordsOption(20.7))
    testAllNullsAndEmptyReplaced(r1, someDf)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "string",
      options = FillEmptyRecordsOption(Array(1, 2, 3, 4, 5, 6)))
    testAllNullsAndEmptyReplaced(r2, someDf)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "string",
      options = FillEmptyRecordsOption(Map("hello" -> 3)))
    testAllNullsAndEmptyReplaced(r3, someDf)
  }
  test("test REMEDIATION APPLY: INT TYPE column with compatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "int", options = FillEmptyRecordsOption(-50))
    testAllNullsAndEmptyReplaced(r, someDf)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "int", options = FillEmptyRecordsOption("10"))
    testAllNullsAndEmptyReplaced(r1, someDf)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "int", options = FillEmptyRecordsOption("-23450"))
    testAllNullsAndEmptyReplaced(r2, someDf)
  }
  test("test REMEDIATION APPLY: INT TYPE column with incompatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "int", options = FillEmptyRecordsOption(Array("50")))
    assert(r.apply(someDf).isFailure)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "int", options = FillEmptyRecordsOption(date))

    assert(r1.apply(someDf).isFailure)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "int", options = FillEmptyRecordsOption(Double.NaN))

    assert(r2.apply(someDf).isFailure)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "int", options = FillEmptyRecordsOption(10.5))

    assert(r3.apply(someDf).isFailure)

    val r4: FillEmptyRecords = FillEmptyRecords(field = "int", options = FillEmptyRecordsOption("10.5"))

    assert(r4.apply(someDf).isFailure)
  }
  test("test REMEDIATION APPLY: LONG TYPE column with compatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "long", options = FillEmptyRecordsOption(Integer.MAX_VALUE))

    testAllNullsAndEmptyReplaced(r, someDf)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "long", options = FillEmptyRecordsOption("10"))

    testAllNullsAndEmptyReplaced(r1, someDf)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "long", options = FillEmptyRecordsOption(Long.MinValue))

    testAllNullsAndEmptyReplaced(r2, someDf)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "long", options = FillEmptyRecordsOption(Long.MaxValue))

    testAllNullsAndEmptyReplaced(r3, someDf)
  }
  test("test REMEDIATION APPLY: LONG TYPE column with incompatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "long", options = FillEmptyRecordsOption(Array("50")))

    assert(r.apply(someDf).isFailure)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "long", options = FillEmptyRecordsOption(date))

    assert(r1.apply(someDf).isFailure)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "long", options = FillEmptyRecordsOption(Double.NaN))

    assert(r2.apply(someDf).isFailure)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "long", options = FillEmptyRecordsOption(10.5))

    assert(r3.apply(someDf).isFailure)

    val r4: FillEmptyRecords = FillEmptyRecords(field = "long",
      options = FillEmptyRecordsOption(Timestamp.from(Instant.now())))

    assert(r4.apply(someDf).isFailure)
  }
  test("test REMEDIATION APPLY: DOUBLE TYPE column with compatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "double", options = FillEmptyRecordsOption(-50.23))

    testAllNullsAndEmptyReplaced(r, someDf)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "double", options = FillEmptyRecordsOption("10"))

    testAllNullsAndEmptyReplaced(r1, someDf)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "double", options = FillEmptyRecordsOption("-23450"))

    testAllNullsAndEmptyReplaced(r2, someDf)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "double", options = FillEmptyRecordsOption("-8.5"))

    testAllNullsAndEmptyReplaced(r3, someDf)

    val r4: FillEmptyRecords = FillEmptyRecords(field = "double", options = FillEmptyRecordsOption(Double.NaN))

    testAllNullsAndEmptyReplaced(r4, someDf)

    val r5: FillEmptyRecords = FillEmptyRecords(field = "double", options = FillEmptyRecordsOption(Double.MaxValue))

    testAllNullsAndEmptyReplaced(r5, someDf)
  }
  test("test REMEDIATION APPLY: DOUBLE TYPE column with incompatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "double", options = FillEmptyRecordsOption(Array("50")))

    assert(r.apply(someDf).isFailure)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "double", options = FillEmptyRecordsOption(date))

    assert(r1.apply(someDf).isFailure)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "double",
      options = FillEmptyRecordsOption(Timestamp.from(Instant.now())))

    assert(r2.apply(someDf).isFailure)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "double",
      options = FillEmptyRecordsOption("string value"))

    assert(r3.apply(someDf).isFailure)
  }
  test("test REMEDIATION APPLY: BOOLEAN TYPE column with compatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption(true))

    testAllNullsAndEmptyReplaced(r, someDf)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption(false))

    testAllNullsAndEmptyReplaced(r1, someDf)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption("true"))

    testAllNullsAndEmptyReplaced(r2, someDf)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption("false"))

    testAllNullsAndEmptyReplaced(r3, someDf)
  }
  test("test REMEDIATION APPLY: BOOLEAN TYPE column with incompatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption(1.0))

    assert(r.apply(someDf).isFailure)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption(0.0))

    assert(r1.apply(someDf).isFailure)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption("0"))

    assert(r2.apply(someDf).isFailure)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption("1"))

    assert(r3.apply(someDf).isFailure)

    val r4: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption(0))

    assert(r4.apply(someDf).isFailure)

    val r5: FillEmptyRecords = FillEmptyRecords(field = "boolean", options = FillEmptyRecordsOption(1))

    assert(r5.apply(someDf).isFailure)
  }
  test("test REMEDIATION APPLY: TIMESTAMP TYPE column with compatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "timestamp", options = FillEmptyRecordsOption(ts))

    testAllNullsAndEmptyReplaced(r, someDf)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "timestamp", options = FillEmptyRecordsOption(ts.toString))

    testAllNullsAndEmptyReplaced(r1, someDf)
  }
  test("test REMEDIATION APPLY: TIMESTAMP TYPE column with incompatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "timestamp",
      options = FillEmptyRecordsOption("hello"))

    assert(r.apply(someDf).isFailure)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "timestamp",
      options = FillEmptyRecordsOption(ts.getTime))

    assert(r1.apply(someDf).isFailure)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "timestamp", options = FillEmptyRecordsOption(date))

    assert(r2.apply(someDf).isFailure)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "timestamp",
      options = FillEmptyRecordsOption(date.toString))

    assert(r3.apply(someDf).isFailure)
  }
  test("test REMEDIATION APPLY: DATE TYPE column with compatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "date",
      options = FillEmptyRecordsOption(new SimpleDateFormat("yyyy-MM-dd").format(date)))

    testAllNullsAndEmptyReplaced(r, someDf)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "date",
      options = FillEmptyRecordsOption(new SimpleDateFormat("yyyy-M-d").format(date)))

    testAllNullsAndEmptyReplaced(r1, someDf)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "date", options = FillEmptyRecordsOption("2019-02-28"))

    testAllNullsAndEmptyReplaced(r2, someDf)
  }
  test("test REMEDIATION APPLY: DATE TYPE column with incompatible default value") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "date",
      options = FillEmptyRecordsOption(date.getTime))

    assert(r.apply(someDf).isFailure)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "date", options = FillEmptyRecordsOption("hello"))

    assert(r1.apply(someDf).isFailure)

    val r2: FillEmptyRecords = FillEmptyRecords(field = "date",
      options = FillEmptyRecordsOption(new java.util.Date()))

    assert(r2.apply(someDf).isFailure)

    val r3: FillEmptyRecords = FillEmptyRecords(field = "date",
      options = FillEmptyRecordsOption(new java.util.Date().toString))

    assert(r3.apply(someDf).isFailure)
  }

  test("test REMEDIATION APPLY: any OTHER TYPE, is not allowed") {
    val r: FillEmptyRecords = FillEmptyRecords(field = "array",
      options = FillEmptyRecordsOption(Array("v1", "v2")))

    assert(r.apply(someDf).isFailure)

    val r1: FillEmptyRecords = FillEmptyRecords(field = "map",
      options = FillEmptyRecordsOption(Map("v" -> 1)))

    assert(r1.apply(someDf).isFailure)
  }

}
