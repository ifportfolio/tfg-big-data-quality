package es.uniovi.uo237525.dataquality.engine.rules.evaluation

import java.util.UUID

import es.uniovi.uo237525.dataquality.LogConstants.Level
import es.uniovi.uo237525.dataquality.engine.EvaluationInfo
import es.uniovi.uo237525.dataquality.engine.rules.{EvaluationRule, RuleOptions}
import es.uniovi.uo237525.dataquality.utils.Utils
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

import scala.util.Try
import scala.util.matching.Regex

case class EvNumericFormatOptions(thouSep: String, decSep: String, decMax: Int, decMin: Int = 0) extends RuleOptions

/**
  * The purpose of this rule is the detection the number of records ina field that follow an specific numeric pattern.
  *
  * @param id      Identifier for the rule. Automatically generated if not provided
  * @param field   Field in which the rule is going to be applied
  * @param options EvDateFormatOptions needed for the rule application.
  */
case class EvNumericFormat(id: String = UUID.randomUUID().toString,
                           field: String,
                           options: EvNumericFormatOptions) extends EvaluationRule {

  private val decMax: Int = options.decMax
  private val decMin: Int = options.decMin
  private val decSep: String = options.decSep
  private val thousSep: String = options.thouSep

  /**
    * Builds the regular expression needed to check the numeric format of the values.
    * The used RegEx for the integer part is:
    *   - If an thousSep is provided it will use: (^-?(?!0)\d{1,3}($formattedThousSep\d{3})*...
    *   - Else ^-?\d+...
    * Then for the decimal part:
    *   - if can have or have not decimals: ($formattedDecSep\d{1,$maxDecimals})?$)|(^0($formattedDecSep\d{1,$maxDecimals})?$)
    *   - if exactly 0 decimals
    *   - if decimals between [0, >0]: ...($formattedDecSep\d{1,$maxDecimals})?$)|(^0($formattedDecSep\d{$minDecimals,$maxDecimals})?$)
    *   - if decimals between [>0, >0] ... $formattedDecSep\d{$minDecimals,$maxDecimals}$)|(^0$formattedDecSep\d{$minDecimals,$maxDecimals}$)
    *
    * @param thousandsSep the separator used for the thousands
    * @param decimalSep   the separator used for the decimals
    * @param minDecimals  the min number of decimals the number must have
    * @param maxDecimals  the max number of decimals the number is allowed to have
    * @return the regular expression used to evaluate the values.
    */
  private def getNumericFormatRegex(thousandsSep: String, decimalSep: String, minDecimals: Int, maxDecimals: Int): Regex = {
    val formattedDecSep = Utils.escapeSpecialRegexChars(decimalSep)
    val formattedThousSep = Utils.escapeSpecialRegexChars(thousandsSep)

    val sb = new StringBuilder()
    // if provided thousSep
    if (formattedThousSep.length > 0) {
      sb.append(s"""(^-?(?!0)\\d{1,3}($formattedThousSep\\d{3})*""")
    }
    // not provided thousSep
    else {
      sb.append("""(^-?\d+""")
    }
    // if can have no decimals (
    if (minDecimals == 0) {
      // can have or have not decimals
      if (maxDecimals > 0) {
        sb.append(s"""($formattedDecSep\\d{1,$maxDecimals})?$$)|(^0($formattedDecSep\\d{1,$maxDecimals})?$$)""")
      }
      // only non decimal numbers (we include the 0 here)
      else {
        sb.append(s"""$$)|(^0$$)""")
      }
    }
    else {
      // always have decimals (and because of the precondition,the ax decimals always >= minDecimals
      //      if (maxDecimals > 0) {
      sb.append(s"""$formattedDecSep\\d{$minDecimals,$maxDecimals}$$)|(^0$formattedDecSep\\d{$minDecimals,$maxDecimals}$$)""")

    }
    sb.toString().r
  }

  /**
    * Checks the needed preconditions for the rule.
    *
    * The preconditions that need to be fulfilled are:
    *   - The field exists
    *   - The field is of type StringType
    *   - The provided decmax is greater than 0.
    *   - The provided decSep is not null
    *   - The provided thouSep is not null
    *   - The provided field is of StringType
    *
    * @param df the dataframe to evaluate
    * @return The error string in case a condition is not fulfilled. None otherwise
    */
  override def checkPreconditions(df: DataFrame): Option[String] = {
    val exists = Try(df(field)).isSuccess
    if (!exists) {
      val errorMsg = s"Field $field does not exists."
      return Some(errorMsg)
    }

    val columnType = df.select(field).schema.head.dataType
    columnType match {
      case StringType => None // do nothing
      case _ =>
        val errorMsg = s"Rule cannot be applied to fields of type: '$columnType'."
        return Some(errorMsg)
    }

    if (decMax < 0) return Some("'decMax' must be equal or greater than 0.")
    if (decMin < 0) return Some("'decMin' must be equal or greater than 0.")
    if (decMin > decMax) return Some("'decMax' must be equal or greater than 'decMin'.")
    if (decSep == null) return Some(s"""Provided decimal separator '$decSep' is not valid.""")
    if (thousSep == null) return Some(s"""Provided thousands separator '$thousSep' is not valid.""")
    if (thousSep.length > 0 && thousSep.equals(decSep)) Some(s"""Separators can not be the same.""")
    else None
  }

  /**
    * it checks whether a record in the field follows the numeric pattern or not
    *
    * @param df the base dataframe where the rule is going to be applied
    * @return A pair containing the resulting DataFrame and
    *         the results of the application of the rule
    */
  override def apply(df: DataFrame): Try[EvaluationInfo] = Try {
    val totalRows = df.select(field).count()

    val regex = getNumericFormatRegex(thousSep, decSep, decMin, decMax)
    val fullFillRegexUdf = udf((s: String) => s != null && regex.findFirstIn(s).isDefined)
    val fullFillNumericFormat = df.select(field).filter(fullFillRegexUdf(col(field))).count()

    EvaluationInfo(Level.INFO,
      s"Evaluation of Format: (TSep: '$thousSep', DSep: '$decSep', NumDec: $decMax).",
      fullFillNumericFormat,
      totalRows)
  }

}
