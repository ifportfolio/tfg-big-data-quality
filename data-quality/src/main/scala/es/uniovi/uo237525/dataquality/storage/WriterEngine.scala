package es.uniovi.uo237525.dataquality.storage

import java.net.ConnectException

import es.uniovi.uo237525.dataquality.LogConstants.{Level, LogType}
import es.uniovi.uo237525.dataquality.OutputTypes
import es.uniovi.uo237525.dataquality.logmanager.LogManager
import es.uniovi.uo237525.dataquality.parser.models.OutputParams
import es.uniovi.uo237525.dataquality.storage.writers.{CSVWriter, HiveWriter}
import org.apache.log4j.Logger
import scala.util.{Failure, Success}

import org.apache.spark.sql.{DataFrame, SaveMode}

/**
  * The engine that controls all the writing process.
  * It is responsible for creating the suitable writers for the data destiny, and to command the write operation through the created one.
  */
class WriterEngine() {

  val logger: Logger = Logger.getRootLogger


  def write(df: DataFrame, writer: Writer, saveMode: String, repartition: Option[Int]): Unit = {
    val writerSignature = writer.getClass.getSimpleName
    LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"Using $writerSignature")
    val connection = writer.open()
    connection match {
      case Success(conn) =>
        LogManager.addGenericLog(Level.INFO, LogType.OUTPUT, "Connection successfully opened to writer.")
        val overwriteStr = SaveMode.Overwrite.name.toLowerCase
        val errorIfExistsStr = SaveMode.ErrorIfExists.name.toLowerCase
        val appendStr = SaveMode.Append.name.toLowerCase
        val ignoreStr = SaveMode.Ignore.name.toLowerCase

        val dfPartitioned = if (repartition.isDefined) {
          val numR = repartition.get
          LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"Number of repartition: $numR")
          df.repartition(numR)
        }
        else df

        val intent = saveMode.toLowerCase match {
          case `overwriteStr` =>
            LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"'$overwriteStr' writing SaveMode selected.")
            writer.overwrite(dfPartitioned, conn)
          case `errorIfExistsStr` =>
            LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"'$errorIfExistsStr' writing SaveMode selected.")
            writer.errorIfExists(dfPartitioned, conn)
          case `appendStr` =>
            LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"'$appendStr' writing SaveMode selected.")
            writer.append(dfPartitioned, conn)
          case `ignoreStr` =>
            LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"'$ignoreStr' writing SaveMode selected.")
            writer.ignore(dfPartitioned, conn)
          case _ => throw new IllegalArgumentException(s"Unrecognized SaveMode: $saveMode")
        }
        intent match {
          case Success(_) => logger.warn("Successfully written results.")
            LogManager.addGenericLog(Level.INFO, LogType.OUTPUT, "Successfully written results")
          case Failure(exception) =>
            LogManager.addGenericLog(Level.FATAL, LogType.OUTPUT, exception.getMessage)
            throw exception
        }
      case Failure(exception) =>
        LogManager.addGenericLog(Level.FATAL, LogType.OUTPUT, exception.getMessage)
        throw new ConnectException(exception.getMessage)
    }
  }

  /**
    * Creates the suitable data source to read the data
    *
    * @return the reader most suitable for the data-set
    */
  def createWriterFromOutParams(outParams: Option[OutputParams]): Option[Writer] = {
    if (outParams.isDefined) {
      val outType = outParams.get.target.toLowerCase
      val writer = outType match {
        case OutputTypes.HIVE =>
          logger.warn("Creating Hive Writer")
          new HiveWriter(outParams.get)
        case OutputTypes.CSV =>
          logger.warn("Creating CSV Writer")
          CSVWriter(outParams.get)
        case _ => throw new IllegalArgumentException( s"""Provided source "$outType" is not supported.""")
      }
      Some(writer)
    }
    else {
      LogManager.addGenericLog(Level.INFO, LogType.OUTPUT, "The output section is not defined.")
      None
    }
  }
}