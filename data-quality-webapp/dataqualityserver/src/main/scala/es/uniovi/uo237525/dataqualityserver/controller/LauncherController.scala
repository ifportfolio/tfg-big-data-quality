package es.uniovi.uo237525.dataqualityserver.controller

import java.io.File
import java.sql.Timestamp
import java.time.Instant

import es.uniovi.uo237525.dataqualityserver.model.LaunchResult

import es.uniovi.uo237525.dataqualityserver.service.{FileSystemService, JarExecutorService, RedisService}
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.web.bind.annotation._

/**
  * Controller for the management of launching executions of the BDQ component.
  *
  * @param jarExecService Service that allows to launch JAR componentes through Spark.
  * @param redisService Service that provides functionality for reading Redis
  * @param fsService Service that provide functionality for generating files
  * @param env Environment variables containing configuration properties
  */
@CrossOrigin(origins = Array("http://localhost:4200"))
@RestController
class LauncherController @Autowired()(jarExecService: JarExecutorService,
                                      redisService: RedisService,
                                      fsService: FileSystemService,
                                      env: Environment) {

  private final val BDQ_JAR_PATH = env.getProperty("bdq.jar.path")
  private final val FILES_GEN_PATH = env.getProperty("files.path")
  private final val REDIS_HOST = env.getProperty("redis.host")
  private final val REDIS_PORT = env.getProperty("redis.port").toInt
  private final val REDIS_LIST_KEY = env.getProperty("redis.executions.key")

  /**
    * This method receives the client petition for launching the BDQ component
    *
    * @param rulesFile     rules filename that will be used for the execution
    * @param rulesContent  rules content that will be used for the execution
    * @param configFile    config filename that will be used for the execution
    * @param configContent config content that will be used for the execution
    * @return a LaunchResult object that contains all the information related to the execution
    */
  @RequestMapping(value = Array("/launch"), method = Array(RequestMethod.POST))
  def launchValidador(@RequestParam("rules-file") rulesFile: String,
                      @RequestParam("rules-content") rulesContent: String,
                      @RequestParam(value = "conf-file", required = false) configFile: String,
                      @RequestParam(value = "conf-content", required = false) configContent: String): LaunchResult = {

    // creation of files
    val rPath = s"$FILES_GEN_PATH/$rulesFile"
    val rFile = fsService.createFile(rPath, rulesContent)
    val cFile: Option[File] = if ((configFile != null && configFile.nonEmpty) && configContent != null) {
      val cPath = s"$FILES_GEN_PATH/$configFile"
      Some(fsService.createFile(cPath, configContent))
    }
    else None

    // execution
    val start = System.currentTimeMillis()
    val startTs = Timestamp.from(Instant.ofEpochMilli(start)).toString

    val executionLogs = if (cFile.isDefined) {
      jarExecService.sparkSubmitJar(BDQ_JAR_PATH, "-r", rFile.getCanonicalPath, "-c", cFile.get.getCanonicalPath)
    }
    else jarExecService.sparkSubmitJar(BDQ_JAR_PATH, "-r", rFile.getCanonicalPath)
    val end = System.currentTimeMillis()
    //end of execution

    // get redis message
    val alerts = redisService.getLastMessageFromList(REDIS_LIST_KEY, REDIS_HOST, REDIS_PORT) match {
      case Some(ident) => {
        val time = ident.toLong
        //if the id represents a date between the start and now, it means it is the correct one
        if (time > start && time < end) redisService.getValueFromKey(ident, REDIS_HOST, REDIS_PORT).getOrElse("[]")
        else "[]"
      }
      case None => "[]"
    }

    // create
    val configMsg = cFile match {
      case Some(x) => x.getCanonicalPath
      case None => "Not provided, used the inner default config file"
    }
    val launchResult = LaunchResult(startTs,
      s"${end - start} ms",
      executionLogs._1,
      executionLogs._2,
      rFile.getCanonicalPath,
      configMsg,
      alerts)
    launchResult
  }
}
