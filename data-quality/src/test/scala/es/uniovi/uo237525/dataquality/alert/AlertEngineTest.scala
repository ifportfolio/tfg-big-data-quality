package es.uniovi.uo237525.dataquality.alert

import es.uniovi.uo237525.dataquality.configuration.Configuration
import org.scalatest.{BeforeAndAfterAll, FunSuite}

class AlertEngineTest extends FunSuite with BeforeAndAfterAll {

  private val arrWithRules = Array("-r", "")
  private val alertMinPropConfigFilePath: String = getClass.getResource("/conf/alert-min-properties.conf").getPath
  //  val logs: List[Log] = Event.values.map(event => GenericLog(event, event.toString)).toList

  private def getCommandLinePropString(key: String, value: Any): String = s"-D$key=$value"

  private def getConfig(properties: Seq[String], configFilePath: String): Configuration = {
    Configuration(arrWithRules ++ Array("-c", configFilePath) ++ properties)
  }

  private def getConfig(properties: Seq[String]): Configuration = {
    Configuration(arrWithRules ++ properties)
  }


  test("test SEND MAIL ALERTS No Alerts") {
    val aEngine = new AlertEngine(getConfig(Seq(), "./src/test/resources/conf/test.conf"))
    aEngine.sendMailAlerts(Seq(), Array())
    succeed
  }

//  test("test SEND MAIL ALERTS No Alerts") {
//    val aEngine = new AlertEngine(getConfig(Seq(), "./src/test/resources/conf/test.conf"))
//    aEngine.sendMailAlerts(Seq(), Array(MailAlert(true, "DEBUG", "to", subject = "subject")))
//    succeed
//  }

  /** TESTS */
}

