package es.uniovi.uo237525.dataqualityserver.service

import java.io.File

import org.apache.logging.log4j.LogManager
import org.apache.spark.launcher.SparkLauncher
import org.springframework.stereotype.Service

import scala.io.Source

/**
  * Provides functionality related with the launch of jar files through spark
  */
@Service
class JarExecutorService {

  private val logger = LogManager.getRootLogger

  /**
    * Submits the JAR to the Spark distribution deployed in the environment.
    * In order for this function to work, it is necessary for the system to declare the environment variable SPARK_HOME.
    *
    * @param jarPath   the path to the executed jar file
    * @param jarParams the parameters to pass to the executed JAR file
    * @return a tuple containing the logs && the execution state of the spark exec
    */
  def sparkSubmitJar(jarPath: String, jarParams: String*): (String, String) = {

    val logFile = new File("./log-output")
    val launcher = new SparkLauncher()
      .setVerbose(true)
      .setAppResource(jarPath)
      .addAppArgs(jarParams: _*)
      .redirectError(logFile)
      .redirectOutput(logFile)

    //    launcher.redirectOutput()
    val handler = launcher.startApplication()
    while (!handler.getState.isFinal) {
      logger.info("Current state: " + handler.getState)
      logger.info("App Id " + handler.getAppId)
      Thread.sleep(3000)
    }
    logger.info("Current state: " + handler.getState)
    logger.info("App Id " + handler.getAppId)
    val logContent = Source.fromFile(logFile).mkString
    logFile.delete()
    logger.info(logContent)
    (handler.getState.toString, logContent)
  }

}
