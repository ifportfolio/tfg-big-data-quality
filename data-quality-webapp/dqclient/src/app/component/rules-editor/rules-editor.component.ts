import 'brace';

import 'brace/mode/json';
import 'brace/theme/chrome';

import { Component, AfterViewInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { NewFileDialogData } from 'src/app/model/NewFileDialogData';
import { AceComponent, AceDirective, AceConfigInterface } from 'ngx-ace-wrapper';
import { AssetLoaderService } from 'src/app/services/asset-loader.service';

/**
 * Component for the rules file editor.
 * 
 * Sub-component of the AppComponent.
 */
@Component({
  selector: 'app-rules-editor',
  templateUrl: './rules-editor.component.html',
  styleUrls: ['./rules-editor.component.css']
})
export class RulesEditorComponent implements AfterViewInit {

  /**
   * The template for the rule file.
   */
  private rulesJsonTemplate;

  /**
   * Configuration for the AceEditor
   */
  public config: AceConfigInterface = {
    mode: 'json',
    theme: 'chrome',
    readOnly: true,
    fontSize: "9pt"
  };

  /**
   * Receives the config file content from the parent.
   */
  @Input() fileContent: string;
  /**
   * Emits on file content change to the parent.
   */
  @Output() fileContentChange = new EventEmitter()
  /**
   * Receives the filename from the parent.
   */
  @Input() filename: string;
  /**
   * Emits on file name change to the parent.
   */
  @Output() filenameChange = new EventEmitter()

  /**
   *  The label that represents whether the editor in in read-only or editable state.
   */
  private editorMode = this.config.readOnly ? "Readonly" : "Editable";

  /**
   * Reference to the AceEditor component.
   */
  @ViewChild(AceComponent) componentRef?: AceComponent;
  /**
   * Reference to the AceEditor directive
   */
  @ViewChild(AceDirective) directiveRef?: AceDirective;

  /**
   * Injects the needed services for the component.
   * 
   * Loads the rules file predefined template stores in the assets folder.
   * 
   * @param assetLoader Service for loading the file that contains the template
   * @param createRulesFileDialog Dialog for the generation a new file
   */
  constructor(assetLoader: AssetLoaderService, public createRulesFileDialog: MatDialog) {
    assetLoader.loadTextFileAsset("rulesTemplate.json").subscribe(data => {
      //console.log(data)
      this.rulesJsonTemplate = JSON.parse(data);
    });
  }

  /**  
   * No functionality 
   */
  ngAfterViewInit(): void { }

  /**
   * The event thrown when the selects a file to load its content from the file explorer.
   * 
   * @param event the event containing the target files.
   */
  fileSelected(event: any) {
    console.log(event)
    let file = event.target.files[0];
    this.filename = file.name
    var reader = new FileReader();
    reader.onloadend = (fr: any) => {
      this.config.readOnly = true
      this.editorMode = this.config.readOnly ? "Readonly" : "Editable"
      this.fileContent = fr.target.result
      this.fileContentChange.emit(this.fileContent)
      this.filenameChange.emit(this.filename)
    }
    reader.readAsText(file);
  }

  /**
   * Inverts the state of the readonly toogle.
   */
  public toggleReadonly(): void {
    this.config.readOnly = this.config.readOnly ? false : true;
    this.editorMode = this.config.readOnly ? "Readonly" : "Editable"
  }

  /**
   * This function is triggered on each change of the editor.
   */
  onEditorChange() {
    this.fileContentChange.emit(this.fileContent)
  }

  /**
   * Handles the process of creation a new file using the CreateRulesFileDialog.
   * 
   * Executes when the user clicks on the create new button.
   */
  createNewFile() {
    const dialogRef = this.createRulesFileDialog.open(CreateRulesFileDialog, { disableClose: true });
    dialogRef.afterClosed().subscribe(ruleFileCreationTemplate => {
      console.log("Closing creation dialog")
      if (ruleFileCreationTemplate != undefined) {
        this.config.readOnly = this.config.readOnly = false;
        this.editorMode = "Editable";
        if (ruleFileCreationTemplate.useTemplate) {
          this.fileContentChange.emit(JSON.stringify(this.rulesJsonTemplate, null, 2));
        }
        else {
          this.fileContentChange.emit("{}")
        }
        this.filenameChange.emit(ruleFileCreationTemplate.filename + ".json")
      }
    });
  }
}

/**
 * Modal component that provides the functinality for generating a new rules file.
 */
@Component({
  selector: 'create-rules-file-dialog',
  templateUrl: 'create-rules-file-dialog.html',
})
export class CreateRulesFileDialog {

  /** 
   * Filename provided by the user
   */
  filename = ""
  /** 
   * Flag that indicates if the checkbox for the template usage is checked
   */
  useTemplate = false

  /**
   * Injects the dialog instance in the component.
   * 
   * @param dialogRef The CreateConfigDialog instance
   */
  constructor(public dialogRef: MatDialogRef<CreateRulesFileDialog>) { }

  /**
   * Triggered when the user cancels the generating file process.
   * Closes the dialog.
   */
  cancel() {
    this.dialogRef.close()
  }

  /**
   * Triggered on the user confirmation of the generation of a new file action.
   */
  onConfirm() {
    let ruleFile = new NewFileDialogData();
    ruleFile.filename = this.filename.trim();
    ruleFile.useTemplate = this.useTemplate;
    this.dialogRef.close(ruleFile);
  }

}
