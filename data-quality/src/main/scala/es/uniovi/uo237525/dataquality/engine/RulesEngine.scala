package es.uniovi.uo237525.dataquality.engine

import es.uniovi.uo237525.dataquality.LogConstants.Level
import es.uniovi.uo237525.dataquality.engine.rules.{RemediationRule, Rule}
import es.uniovi.uo237525.dataquality.logmanager.LogManager
import es.uniovi.uo237525.dataquality.logmanager.logs.RuleLog
import org.apache.log4j.Logger
import scala.util.{Failure, Success}

import org.apache.spark.sql.DataFrame

/**
  * The engine that applies eachof the rules over a Spark dataframe.
  *
  * @param df    the dataframe in which the rule will b applied.
  * @param rules the rules list that will be applied
  * @author Ignacio Fernandez Fernandez
  */
class RulesEngine(df: DataFrame, rules: List[Rule]) {

  val logger: Logger = Logger.getRootLogger

  /**
    * This methods executes the rule engine.
    * It applies each of the rules iteratively in a strict definition order. This means, each rule will be applied over the
    * dataframe resulting of applying the previous rule.
    * If any of the rules does not comply with the preconditions, that rule will be skipped and it will jump to the next one.
    * If any of the rules fail when the application phase, it will also jump to the next one.
    * All the information recabed by each rule application will be stored using the LogManager
    *
    * @return the modified dataframe.
    */
  def execute(): DataFrame = {
    logger.warn("Executing Rules Engine.")
    val resultingDf = rules.foldLeft(df)((actualDf, rule) => {
      logger.warn(s"Checking pre-conditions of rule: ${rule.id}.")
      rule.checkPreconditions(actualDf) match { // If rule can be applied
        case None =>
          logger.warn(s"Applying rule: ${rule.id}.")
          val pair = rule.apply(actualDf) match {
            case Success(info) =>
              logger.warn(s"Rule ${rule.id} APPLIED.")
              val rLog = RuleLog(
                rule.id,
                info.level,
                rule.getClass.getSimpleName,
                rule.field,
                info.matched,
                info.total,
                info.info)
              if (rule.isInstanceOf[RemediationRule]) (info.asInstanceOf[RemediationInfo].modifiedDF, rLog)
              else (actualDf, rLog)
            case Failure(ex) =>
              logger.warn(s"Rule ${rule.id} FAILED.")
              val rLog = RuleLog(
                rule.id,
                Level.ERROR,
                rule.getClass.getSimpleName,
                rule.field,
                0,
                0,
                ex.getMessage)
              (actualDf, rLog)
          }
          LogManager.addLog(pair._2) // Store the log
          pair._1

        case Some(reason) =>
          logger.warn(s"SKIPPED Rule ${rule.id}: $reason")
          LogManager.addLog(RuleLog(
            rule.id,
            Level.WARN,
            rule.getClass.getSimpleName,
            rule.field,
            0,
            0,
            s"Skipped rule: $reason"))
          actualDf
      }
    }

    )
    logger.warn("Rules Engine execution finished.")
    resultingDf
  }

}
