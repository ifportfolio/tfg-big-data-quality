package es.uniovi.uo237525.dataquality.alert.senders

import es.uniovi.uo237525.dataquality._
import es.uniovi.uo237525.dataquality.ConfigFileConstants._
import es.uniovi.uo237525.dataquality.alert.AlertSender
import es.uniovi.uo237525.dataquality.configuration.Configuration
import es.uniovi.uo237525.dataquality.logmanager.Log
import es.uniovi.uo237525.dataquality.logmanager.logs.{GenericLog, RuleLog}
import es.uniovi.uo237525.dataquality.parser.models.MailAlert
import es.uniovi.uo237525.dataquality.utils.MailService
import org.apache.log4j.Logger
import scala.util.Try


/**
  * This class is used to send all the alerts through smtp mail channel.
  * It is important to set the timeout connection property in the configuration.
  *
  * The generated alert sent will be an HTML chunk of code.
  * If the alert is not enabled, it will not be sent.
  *
  * @param config the configuration that contains all the parameters needed to send configure the sender.
  * @param al     the mail alert instance that contains information to send the alert
  * @author Ignacio Fernandez Fernandez
  */
case class EmailAlertSender(config: Configuration, al: MailAlert) extends AlertSender {

  private val SMTP_PROTOCOL = "smtp"
  val logger: Logger = Logger.getRootLogger

  /**
    * Creates an html piece of code to show all the logs that are going to be sent.
    *
    * @param logs the logs used to generate the html code.
    * @return the html code containing all the logs.
    */
  private def createHtmlMailContent(logs: List[Log]): String = {
    val generic = logs.filter(_.isInstanceOf[GenericLog])
    val rules = logs.filter(_.isInstanceOf[RuleLog]).map(_.asInstanceOf[RuleLog])

    val genPart = if (generic.nonEmpty) {
      s"""
         |<h3><strong>Execution ID: ${EXEC_UNIQUE_ID}</strong></h3>
         |<h3><strong>Generic Logs</strong></h3>
         |<table style="width:100%">
         |<tr>
         | <th>TIMESTAMP</th>
         | <th>LEVEL</th>
         | <th>LOG TYPE</th>
         | <th>INFO</th>
         |</tr>
         |${generic.map(gen => s"<tr><td>${gen.timestamp}</td><td>${gen.level.toString}</td><td>${gen.logType.toString}</td><td>${gen.info}</td></tr>").mkString("\n")}
         |</table>
      """.stripMargin
    }
    else ""

    val rulesPart = if (rules.nonEmpty) {
      s"""
         |<h3><strong>Rule Logs</strong></h3>
         |<table style="width:100%">
         |<tr>
         |	<th>TIMESTAMP</th>
         |	<th>LEVEL</th>
         |	<th>ID</th>
         |	<th>TYPE</th>
         |	<th>FIELD</th>
         |	<th>MATCHED</th>
         |	<th>TOTAL</th>
         |	<th>INFO</th>
         |</tr>
         |${rules.map(rl => s"<tr><td>${rl.timestamp}</td><td>${rl.level.toString}</td><td>${rl.id}</td><td>${rl.logType}</td><td>${rl.field}</td><td>${rl.matched}</td><td>${rl.total}</td><td>${rl.info}</td></tr>").mkString("\n")}
         |</table>
      """.stripMargin
    }
    else ""

    s"""<html>
       |<head>
       |<style>
       |table, th, td {
       |  border: 1px solid black;
       |  border-collapse: collapse;
       |}
       |th, td {
       |  padding: 15px;
       |}
       |</style>
       |</head>
       |<body>
       |$genPart
       |$rulesPart
       |</body>
       |</html>""".stripMargin
  }


  /**
    * Sends all the log alerts through an smtp server.
    * It uses the configured properties for the smtp server ("smtp.host", "smtp.port"...) and the specific
    * information provided for the mail alert in the constructor, such as the topic.
    *
    * @param logs all the logs that are going to be sent.
    * @return a Try Unit element that returns:
    *         - Success in case the message is successfully sent.
    *         - Failure if either the configuration or the sending process throws an exception.
    */
  override def sendAlert(logs: Iterable[Log]): Try[Unit] = Try {
    if (logs.nonEmpty && al.enabled) {
      logger.warn("Configuring Mail Alerts")
      val mailProperties = config.getChildPropertiesOptionFromPath(
        SmtpProperties.SMTP_HEADER).getOrElse(Map()) map {
        case (key, value) =>
          logger.warn(s"$key -> $value")
          (key, value.toString)
      }

      logger.warn("Generating alert content")
      val content = createHtmlMailContent(logs.toList)
      logger.warn(content)
      val session = MailService.createSession(mailProperties)
      logger.warn("Mail session created.")
      val message = MailService.createMimeMessage(session, content, "text/html")
      logger.warn("Mail created.")
      logger.warn("Attempting to send mail.")
      val auth = config.getOptionalBooleanProperty(SmtpProperties.MAIL_AUTH).getOrElse(false)
      val user = config.getOptionalStringProperty(SmtpProperties.MAIL_USER).getOrElse("")
      val pass = config.getOptionalStringProperty(SmtpProperties.MAIL_PASS).getOrElse("")
      MailService.sendMimeMessage(session, SMTP_PROTOCOL, message, al.subject, al.to, al.cc, al.cco, auth, user, pass)
      logger.warn("Mail sent")

    }
    else {
      if (!al.enabled) {
        logger.error("Alert not enabled")
        throw new Exception("Alert not enabled")
      }
      if (logs.isEmpty) {
        throw new Exception("No logs to send in this alert, aborted send operation")
      }
    }
  }
}