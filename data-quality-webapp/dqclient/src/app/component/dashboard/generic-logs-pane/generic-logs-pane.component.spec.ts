import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericLogsPaneComponent } from './generic-logs-pane.component';

describe('GenericLogsPaneComponent', () => {
  let component: GenericLogsPaneComponent;
  let fixture: ComponentFixture<GenericLogsPaneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenericLogsPaneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericLogsPaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
