package es.uniovi.uo237525.dataquality.alert.senders

import com.redis.RedisClient
import es.uniovi.uo237525.dataquality.ConfigFileConstants.RedisProperties
import es.uniovi.uo237525.dataquality._
import es.uniovi.uo237525.dataquality.alert.AlertSender
import es.uniovi.uo237525.dataquality.configuration.Configuration
import es.uniovi.uo237525.dataquality.logmanager.{Log, LogManager}
import es.uniovi.uo237525.dataquality.parser.models.RedisAlert
import org.apache.log4j.Logger

import scala.util.Try

/**
  * Sender used to store the an alert in Redis.
  * The alerts ID will be stored in a redis list which key is the specified d in the alert using an LPUSH operation.
  * The alerts content will be stored as a JSON string using the ID.
  *
  * @param config the configuration that contains all the parameters needed to send configure the sender.
  * @param al     the redis alert that will be emitted
  * @author Ignacio Fernandez Fernandez
  */
case class RedisAlertSender(config: Configuration, al: RedisAlert) extends AlertSender {

  val logger: Logger = Logger.getRootLogger

  /**
    * Pushed the identifier in the specified key list, then, it stores the JSON alert message keyed by that ID.
    *
    * @param logs all the logs that are going to be sent.
    * @return a Try Unit element that returns:
    *         - Success in case the message is successfully produced to the topic.
    *         - Failure if either the configuration or the sending process throws an exception.
    */
  override def sendAlert(logs: Iterable[Log]): Try[Unit] = Try {
    if (logs.nonEmpty && al.enabled) {
      val sentTry = Try({
        val host = config.getMandatoryStringProperty(RedisProperties.REDIS_HOST)
        val port = config.getMandatoryIntProperty(RedisProperties.REDIS_PORT)
        val client = new RedisClient(host, port.toInt)
        client.lpush(al.key, s"$EXEC_UNIQUE_ID")
        client.set(EXEC_UNIQUE_ID, LogManager.produceJsonFromLogs(logs.toList))
        //        client.shutdown
      })
      sentTry.get
    }
    else {
      if (!al.enabled) {
        logger.error("Alert not enabled")
        throw new Exception("Alert not enabled")
      }
      if (logs.isEmpty) {
        throw new Exception("No logs to send in this alert, aborted send operation")
      }
    }
  }
}
