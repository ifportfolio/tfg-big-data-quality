package es.uniovi.uo237525.dataquality.engine.rules.evaluation

import java.util.UUID

import es.uniovi.uo237525.dataquality.LogConstants.Level
import es.uniovi.uo237525.dataquality.engine.EvaluationInfo
import es.uniovi.uo237525.dataquality.engine.rules.{EvaluationRule, RuleOptions}

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._

import scala.util.Try

/**
  * This evaluation rule provides the number of null or empty values in one field.
  *
  * @param id      Identifier for the rule. Automatically generated if not provided
  * @param field   Field in which the rule is going to be applied
  * @param options Specific options for this trule
  */
case class EvRequiredFields(id: String = UUID.randomUUID().toString,
                            field: String,
                            options: RuleOptions = null) extends EvaluationRule {

  /**
    * Checks the needed preconditions for the rule.
    *
    * The preconditions that need to be fulfilled are:
    *   - The field exists
    * @param df
    * @return The error string in case a condition is not fulfilled. None otherwise
    */
  override def checkPreconditions(df: DataFrame): Option[String] = {
    val res = Try(df(field)).isSuccess
    if (!res) {
      Some(s"Field $field does not exists.")
    }
    else None
  }

  /**
    * Counts the number of records in a field with null or empty value.
    *
    * @param df the base dataframe where the rule is going to be applied
    * @return A pair containing the resulting DataFrame and
    *         the results of the application of the rule
    */
  override def apply(df: DataFrame): Try[EvaluationInfo] = Try {
    val totalRows = df.select(field).count()
    val numNulls = df.filter(col(field).isNull || length(col(field)) === lit(0)).count()
    EvaluationInfo(
      Level.INFO,
      s"Null or empty values found: $numNulls.",
      numNulls,
      totalRows)
  }
}
