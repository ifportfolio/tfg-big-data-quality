package es.uniovi.uo237525.dataquality.engine.rules.remediation

import java.sql.{Date, Timestamp}
import java.util.UUID

import es.uniovi.uo237525.dataquality.LogConstants.Level
import es.uniovi.uo237525.dataquality.engine.RemediationInfo
import es.uniovi.uo237525.dataquality.engine.rules.{RemediationRule, RuleOptions}
import scala.util.{Failure, Success, Try}

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

case class FillEmptyRecordsOption(defaultValue: Any) extends RuleOptions

/**
  * This remediation rule substitutes the null values in one field with a default value.
  * It currently has support for the following column SparkTypes:
  *   - StringType
  *   - IntegerType
  *   - LongType
  *   - DoubleType
  *   - BooleanType
  *   - TimestampType
  *   - DateType
  *
  * @param id      Name of the rule
  * @param field   Field in which the rule is going to be applied
  * @param options RemRequiredFieldsOptions needed for the rule application.
  */
case class FillEmptyRecords(id: String = UUID.randomUUID().toString,
                            field: String,
                            options: FillEmptyRecordsOption) extends RemediationRule {

  private val defaultValue = options.defaultValue

  /**
    * Checks the pre-conditions that need to be fulfilled in order that the remediation can be applied.
    *
    * Pre-conditions:
    *   - The existence of the column in the dataframe
    *   - If the default value is null or None
    *   - If the column type declared is suitable for the rule
    *   - If the edfault value is compatible with the column type
    *
    * @param df the dataframe where the remediation is going to be applied
    * @return a verbose description of the reason why a precondition is not fulfilled,
    *         None if no pre-condition is broken,
    */
  override def checkPreconditions(df: DataFrame): Option[String] = {
    // checking that the fields exists
    val res = Try(df(field)).isSuccess
    if (!res) {
      return Some(s"Field $field does not exists.")
    }

    // Checking defaultValue is not null or None
    val defaultValue = options.defaultValue
    if (defaultValue == null || defaultValue.equals(None)) {
      return Some(s"""Provided default value $defaultValue is not valid.""")
    }

    // Checking that the default value introduced is compatible with the column type
    val columnType = df.select(field).schema.head.dataType
    if (
      Try(columnType match {
        case StringType => defaultValue.toString
        case IntegerType => defaultValue.toString.toInt
        case LongType => defaultValue.toString.toLong
        case DoubleType => defaultValue.toString.toDouble
        case FloatType => defaultValue.toString.toFloat
        case DecimalType() => defaultValue.toString.toDouble
        case ShortType => defaultValue.toString.toShort
        case BooleanType => defaultValue.toString.toBoolean
        // timestamp in format yyyy-[m]m-[d]d hh:mm:ss[.f...]
        case TimestampType => Timestamp.valueOf(defaultValue.toString)
        // only accepted format is "yyyy-[m]m-[d]d".
        case DateType => Date.valueOf(defaultValue.toString)
        case _ => return Some("Invalid column type: " + columnType)
      }).isFailure) {
      Some(s"""default value '$defaultValue' is not compatible with column type: '$columnType'.""")
    }
    else None
  }

  /**
    * Substitutes the null / empty value by a default provided one.
    *
    * @param df the base dataframe where the rule is going to be applied
    * @return the rule log if the rule was successfully applied, an exception otherwise
    */
  override def apply(df: DataFrame): Try[RemediationInfo] = Try {
    val defaultValue = options.defaultValue
    val fieldType = df.select(field).schema.head.dataType
    val be4 = df.filter(col(field).isNull || length(col(field)) === lit(0)).count()
    val modifiedDf = fieldType match {
      case StringType =>
        val str = defaultValue.toString
        df.na.fill(str, Seq(field)).withColumn(field, when(length(col(field)) === lit(0), str).otherwise(col(field)))
      case IntegerType =>
        val num = defaultValue.toString.toInt
        df.na.fill(num, Seq(field)).withColumn(field, when(length(col(field)) === lit(0), num).otherwise(col(field)))
      case LongType =>
        val long = defaultValue.toString.toLong
        df.na.fill(long, Seq(field)).withColumn(field, when(length(col(field)) === lit(0), long).otherwise(col(field)))
      case DoubleType =>
        val doub = defaultValue.toString.toDouble
        df.na.fill(doub, Seq(field)).withColumn(field, when(length(col(field)) === lit(0), doub).otherwise(col(field)))
      case FloatType =>
        val float = defaultValue.toString.toFloat
        df.na.fill(float, Seq(field)).withColumn(field, when(length(col(field)) === lit(0), float).otherwise(col(field)))
      case DecimalType() =>
        val dec = defaultValue.toString.toDouble
        df.na.fill(dec, Seq(field)).withColumn(field, when(length(col(field)) === lit(0), dec).otherwise(col(field)))
      case ShortType =>
        val short = defaultValue.toString.toShort
        df.na.fill(short, Seq(field)).withColumn(field, when(length(col(field)) === lit(0), short).otherwise(col(field)))
      case BooleanType =>
        val bool = defaultValue.toString.toBoolean
        df.na.fill(bool, Seq(field)).withColumn(field, when(length(col(field)) === lit(0), bool).otherwise(col(field)))
      case TimestampType =>
        val tsStr = defaultValue.toString
        Try(Timestamp.valueOf(tsStr)) match {
          case Success(_) =>
            df.na.fill(Map(field -> tsStr)).withColumn(field, when(length(col(field)) === lit(0), tsStr).otherwise(col(field)))
          case Failure(exception) => throw exception
        }
      case DateType =>
        val dateStr = defaultValue.toString
        Try(Date.valueOf(dateStr)) match {
          case Success(_) =>
            df.na.fill(Map(field -> dateStr)).withColumn(field, when(length(col(field)) === lit(0), dateStr).otherwise(col(field)))
          case Failure(exception) => throw exception
        }
      case _ => throw new Exception(s"Invalid column type: $fieldType")
    }
    val after = modifiedDf.filter(col(field).isNull || length(col(field)) === lit(0)).count()
    val total = modifiedDf.count()

    RemediationInfo(Level.INFO,

      s"""Null or missing values in "$field" have been replaced by $defaultValue.""",
      be4 - after,
      total,
      modifiedDf)
  }
}
