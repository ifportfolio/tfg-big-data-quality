package es.uniovi.uo237525.dataquality.engine.rules.remediation

import es.uniovi.uo237525.dataquality.SparkSessionTestWrapper
import es.uniovi.uo237525.dataquality.engine.rules.evaluation.{EvNumericFormat, EvNumericFormatOptions}
import org.scalatest.{BeforeAndAfterAll, FunSuite}

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types.StringType

class RemNumericFormatTest extends FunSuite with SparkSessionTestWrapper with BeforeAndAfterAll {

  import spark.implicits._

  val txtPath: String = getClass.getResource("/txt/numeric-format-dataset.txt").getPath
  private val someDf: DataFrame = spark.read.option("delimiter", "|").option("header", value = true).csv(txtPath).cache()


  /** REMEDIATION PRE-CONDITIONS TEST */

  test("test REMEDIATION PRE-CONDITIONS: apply on un-existing field") {
    val rule = RemNumericFormat(field = "non-existing",
      options = RemNumericFormatOptions(",", ".", 4))
    val someDF: DataFrame = Seq(1, 2, 3).toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }

  test("test CHECK PRE-CONDITIONS: can be applied only on columns of type: StringType") {
    val opts = RemNumericFormatOptions(",", ".", 0)
    for (c <- someDf.schema) {
      val columnName = c.name
      val rule = RemNumericFormat(field = columnName, options = opts)
      c.dataType match {
        case StringType => assert(rule.apply(someDf).isSuccess)
        case _ => assert(rule.apply(someDf).isSuccess)
      }
    }
  }
  test("test REMEDIATION PRE-CONDITIONS: non-string column") {
    val rule = RemNumericFormat(field = "nums",
      options = RemNumericFormatOptions(",", ".", 4))
    val someDF: DataFrame = Seq(1, 2, 3).toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }
  test("test CHECK PRE-CONDITIONS: decMax >= 0 are allow to apply the rule") {
    val opts0 = RemNumericFormatOptions(".", ",", 10)
    val ruleWith0 = RemNumericFormat(field = "c1", options = opts0)
    assert(ruleWith0.checkPreconditions(someDf).isEmpty)

    val optsPos = RemNumericFormatOptions(".", ",", 0)
    val ruleWithPos = RemNumericFormat(field = "c1", options = optsPos)
    assert(ruleWithPos.checkPreconditions(someDf).isEmpty)
  }
  test("test REMEDIATION PRE-CONDITIONS: newDecMax < 0") {
    val rule = RemNumericFormat(field = "nums",
      options = RemNumericFormatOptions(",", ".", -3))
    val someDF: DataFrame = Seq("1", "2", "3").toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }
  test("test REMEDIATION PRE-CONDITIONS: newDecSep is null") {
    val rule = RemNumericFormat(field = "nums",
      options = RemNumericFormatOptions(".", null, 1))
    val someDF: DataFrame = Seq("1", "2", "3").toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }
  test("test REMEDIATION PRE-CONDITIONS: newThousSep is null") {
    val rule = RemNumericFormat(field = "nums",
      options = RemNumericFormatOptions(null, ".", 1))
    val someDF: DataFrame = Seq("1", "2", "3").toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }
  test("test REMEDIATION PRE-CONDITIONS: newThousSep equals newDecSep") {
    val rule = RemNumericFormat(field = "nums",
      options = RemNumericFormatOptions(".", ".", 1))
    val someDF: DataFrame = Seq("1", "2", "3").toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }

  /** APPLICATION TEST */
  test("test APPLY: generic") {
    val rule = RemNumericFormat(field = "c1",
      options = RemNumericFormatOptions("-", ",", 3))

    val rem = rule.apply(someDf)
    val expectedRemedied = someDf.count() - someDf.select(rule.field)
      .filter(row => row.get(0) == null || row.getString(0).count(_.isDigit) == 0).count()
    assert(rem.get.matched == expectedRemedied)
  }

  test("test APPLY: can not be applied on un-existing field") {
    val rule = RemNumericFormat(field = "nonExisting",
      options = RemNumericFormatOptions("-", ",", 3))

    assert(rule.apply(someDf).isFailure)
  }

  test("test APPLY: 0 as decMax match only integers") {
    val sequence = Seq("-121", "121", "0", "221.0", "3.", "-223.4")
    val numNotIntegers = sequence.count(el => {
      val intRegex = """^-?\d+$""".r
      el match {
        case intRegex(_*) => false
        case _ => true
      }
    })
    val expectedToMatch = sequence.size - numNotIntegers
    val df = sequence.toDF("decimals")

    val opts = RemNumericFormatOptions(".", ",", 0)
    val rule = RemNumericFormat(field = "decimals", options = opts)
    val result = rule.apply(df).get
    assert(result.matched == expectedToMatch)
  }

  test("test APPLY: using special character as separator and positive decMax") {
    val sequence = Seq("-121\\.", "121.123", "0", "221\\.0", "3.", "-223\\.4", "225.9")

    val df = sequence.toDF("decimals")

    val opts = RemNumericFormatOptions(".", "\\.", 2)
    val rule = RemNumericFormat(field = "decimals", options = opts)
    val log = rule.apply(df).get
    log.modifiedDF.show()
    assert(log.matched === 3)

    val optsq = EvNumericFormatOptions(".", "\\.", 2)
    val ruleq = EvNumericFormat(field = "decimals", options = optsq)
    val qualityInOriginal = ruleq.apply(df).get
    assert(qualityInOriginal.matched == sequence.size - log.matched)

    val qualityInModified = ruleq.apply(log.modifiedDF).get
    assert(qualityInModified.matched == sequence.size)
  }
  test("test APPLY: using normal character as separator and 0 as decMax")(pending)

  test("test APPLY: using normal character as separator and positive decMax") {
    val sequence = Seq("-121", "121", "0", "221.0", "3.", "-223.4", null, "-5.7", "5,60000000", "5.6000000")
    val numDecimals = 5
    val minDecs = 2
    val expectedMatches = sequence.count(_ != null)
    val df = sequence.toDF("decimals")

    val opts = RemNumericFormatOptions(".", ",", numDecimals, minDecs)
    val rule = RemNumericFormat(field = "decimals", options = opts)
    val log = rule.apply(df).get
    log.modifiedDF.show()
    //
    //    // only numbers that have /. as separator and their #decimals are max = 1
    assert(log.matched === expectedMatches)
  }

}
