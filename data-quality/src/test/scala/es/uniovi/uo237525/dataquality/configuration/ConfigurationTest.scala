package es.uniovi.uo237525.dataquality.configuration

import java.io.File

import com.typesafe.config.ConfigException.{BadPath, Missing, UnresolvedSubstitution, WrongType}
import com.typesafe.config.{Config, ConfigFactory}
import es.uniovi.uo237525.dataquality.CONFIG_DEFAULT_NAME
import org.scalatest.PrivateMethodTester._
import org.scalatest.{BeforeAndAfterAll, FunSuite}

class ConfigurationTest extends FunSuite with BeforeAndAfterAll {

  val testConfPath: String = getClass.getResource("/conf/test.conf").getPath
  val wrongConfPath: String = getClass.getResource("/conf/wrong.conf").getPath
  val defaultConfig: Config = ConfigFactory.load(ConfigFactory.load(CONFIG_DEFAULT_NAME))
  private val arrWithRules = Array("-r", "")

  private def getCustomConfig(path: String): Config = {
    ConfigFactory.parseFile(new File(path))
  }

  /** PRIORITY TESTS */
  test("test command line properties have most priority") {
    val prop = "el.string"
    val commandLineValue = "commandLineValue"
    val input = "-D" + prop + "=" + commandLineValue
    val c = Configuration(arrWithRules ++ Array(input, "-c", testConfPath))
    assertResult(commandLineValue) {
      c.getMandatoryStringProperty(prop)
    }
  }

  test("test custom config file properties have second most priority") {
    val usrHeader = "mail.smtp.auth.username"
    val associatedEnvVar = usrHeader.toUpperCase.replace('.', '_')
    assume(sys.env.get(associatedEnvVar).isDefined,
      s": environment var $associatedEnvVar must be defined in order to perform this test.")

    val usrValueInDefault = defaultConfig.getString(usrHeader)
    val usrValueInCustom = getCustomConfig(testConfPath).getString(usrHeader)
    assume(usrValueInDefault != usrValueInCustom,
      "The values of the property must be different in the default and custom config files.")
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val usrValueInCurrent = c.getMandatoryStringProperty(usrHeader)
    assert(usrValueInCurrent == usrValueInCustom,
      s": the value of [$usrHeader] -> [$usrValueInDefault] should have been" +
        s" overwritten by [$usrValueInCustom].")

  }

//  test("test default config file properties have third most priority") {
  //    val propHeader = "alert.mail.enabled"
  //    val args = arrWithRules ++ Array("-c", testConfPath)
  //    assume(Configuration(args).commandLineProperties.get(propHeader).isEmpty,
  //      s"The command line must not contain $propHeader in order to perform the test")
  //    withClue(s"The custom config must not contain a definition for $propHeader") {
  //      assertThrows[ConfigException.Missing] {
  //        getCustomConfig(testConfPath).getString(propHeader)
  //      }
  //    }
  //
  //    val c = Configuration(args)
  //    val defPropVal = defaultConfig.getString(propHeader)
  //    val actualValue = c.getMandatoryStringProperty(propHeader)
  //    assert(defPropVal == actualValue,
  //      s"The value for $propHeader should be the one declared in the default config file but is $actualValue")
  //  }

  test("test environment vars overwrites substituted properties") {
    val propHeader = "el.string"
    val propValue = "string"
    val envVar = "STR_VAL"
    sys.props.put(envVar, "envarvalue")
    assume(sys.env.get(envVar).isDefined,
      s": environment var $envVar must be defined in order to perform this test.")
    assume(propValue != sys.env(envVar),
      s": the value of $envVar must be different from $propValue")
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    assert(c.getMandatoryStringProperty(propHeader) == sys.env(envVar))
  }

  test("test substitute conf file property by un-existing reference throws an \"UnresolvedSubstitution\"") {
    assertThrows[UnresolvedSubstitution] {
      Configuration(arrWithRules ++ Array("-c", wrongConfPath))
    }
  }

  /** PRIVATE METHODS TESTS */

  test("test isDeclaredOnConfigFile with existing property header returns true") {
    val propHeader = "el.string"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")

    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val isDeclaredOnConfigFile = PrivateMethod[Boolean]('isDeclaredOnConfigFile)
    assert(c invokePrivate isDeclaredOnConfigFile(propHeader))
  }
  test("test isDeclaredOnConfigFile with un-existing property header return false") {
    val propHeader = "undeclared-path"
    assume(!getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must not be declared $testConfPath in order that this test is performed.")

    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val isDeclaredOnConfigFile = PrivateMethod[Boolean]('isDeclaredOnConfigFile)
    assert(!(c invokePrivate isDeclaredOnConfigFile(propHeader)))
  }
  test("test isDeclaredOnConfigFile with null throws NullPointerException") {
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val isDeclaredOnConfigFile = PrivateMethod[Boolean]('isDeclaredOnConfigFile)
    assert(!(c invokePrivate isDeclaredOnConfigFile(null)))
  }
  test("test isDeclaredOnConfigFile with invalidPath throws ConfigException.BadPath") {
    val invalidPath = "invalid,path"
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val isDeclaredOnConfigFile = PrivateMethod[Boolean]('isDeclaredOnConfigFile)
    assertThrows[BadPath] {
      c invokePrivate isDeclaredOnConfigFile(invalidPath)
    }
  }

  /** getOptionalStringProperty TESTS */

  test("test getOptionalStringProperty with existing string prop in cl returns Some") {
    val key = "key"
    val value = "value"
    val option = s"-D$key=$value"
    val c = Configuration(arrWithRules ++ Array(option))
    val result = c.getOptionalStringProperty(key)
    assert(result.isDefined && result.get === value)
    assert(result.get.isInstanceOf[String])
  }
  test("test getOptionalStringProperty with existing string prop in config file returns Some") {
    val propHeader = "el.string"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val expectedValue = getCustomConfig(testConfPath).resolve.getString(propHeader)
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val result = c.getOptionalStringProperty(propHeader)
    assert(result.isDefined && result.get === expectedValue)
    assert(result.get.isInstanceOf[String])
  }
  test("test getOptionalStringProperty with existing int prop in config file returns Some") {
    val propHeader = "el.int"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val expectedValue = getCustomConfig(testConfPath).resolve.getInt(propHeader)
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val result = c.getOptionalStringProperty(propHeader)
    assert(result.isDefined && result.get == expectedValue.toString)
    assert(result.get.isInstanceOf[String])
  }
  test("test getOptionalStringProperty with existing double prop in config file returns Some") {
    val propHeader = "el.double"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val expectedValue = getCustomConfig(testConfPath).resolve.getDouble(propHeader)
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val result = c.getOptionalStringProperty(propHeader)
    assert(result.isDefined && result.get == expectedValue.toString)
    assert(result.get.isInstanceOf[String])
  }
  test("test getOptionalStringProperty with existing boolean prop in config file returns Some") {
    val propHeader = "el.boolean"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val expectedValue = getCustomConfig(testConfPath).resolve.getBoolean(propHeader)
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val result = c.getOptionalStringProperty(propHeader)
    assert(result.isDefined && result.get == expectedValue.toString)
    assert(result.get.isInstanceOf[String])
  }
  test("test getOptionalStringProperty with existing list prop in config file returns None") {
    val propHeader = "el.list"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    assert(c.getOptionalStringProperty(propHeader).isEmpty)
  }
  test("test getOptionalStringProperty with un-existing property return None") {
    val propHeader = "non-existing.property"
    assume(!getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must not be defined in the provided configuration file in order that this test is performed.")
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    assert(c.getOptionalStringProperty(propHeader).isEmpty)
  }
  test("test getOptionalStringProperty with null") {
    val c = Configuration(arrWithRules)
    assert(c.getOptionalStringProperty(null).isEmpty)
  }

  /** getMandatoryStringProperty TESTS */

  test("test getMandatoryStringProperty with existing string prop in cl returns the value as a string") {
    val key = "key"
    val value = "value"
    val option = s"-D$key=$value"
    val c = Configuration(arrWithRules ++ Array(option))
    val result = c.getMandatoryStringProperty(key)
    assert(result === value)
  }
  test("test getMandatoryStringProperty with null") {
    val c = Configuration(arrWithRules)
    assertThrows[NullPointerException] {
      c.getMandatoryStringProperty(null)
    }
  }
  test("test getMandatoryStringProperty with existing string property in config file config") {
    val propHeader = "el.string"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val expectedValue = getCustomConfig(testConfPath).resolve.getString(propHeader)
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val result = c.getMandatoryStringProperty(propHeader)
    assert(result === expectedValue)
  }
  test("test getMandatoryStringProperty with existing int prop in config file returns value") {
    val propHeader = "el.int"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val expectedValue = getCustomConfig(testConfPath).resolve.getInt(propHeader)
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val result = c.getMandatoryStringProperty(propHeader)
    assert(result === expectedValue.toString)
  }
  test("test getMandatoryStringProperty with existing boolean prop in config file returns value") {
    val propHeader = "el.boolean"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val expectedValue = getCustomConfig(testConfPath).resolve.getBoolean(propHeader)
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val result = c.getMandatoryStringProperty(propHeader)
    assert(result === expectedValue.toString)
  }
  test("test getMandatoryStringProperty with existing double prop in config file returns value") {
    val propHeader = "el.double"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val expectedValue = getCustomConfig(testConfPath).resolve.getDouble(propHeader)
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val result = c.getMandatoryStringProperty(propHeader)
    assert(result === expectedValue.toString)
  }
  test("test getMandatoryStringProperty with existing int prop in config file throws WrongType") {
    val propHeader = "el.list"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    assertThrows[WrongType] {
      c.getMandatoryStringProperty(propHeader)
    }
  }
  test("test getMandatoryStringProperty with un-existing property throws Missing") {
    val propHeader = "non-existing.property"
    assume(!getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must not be defined in the provided configuration file in order that this test is performed.")
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    assertThrows[Missing] {
      c.getMandatoryStringProperty(propHeader)
    }
  }

  /** getOptionalBooleanProperty TESTS */

  test("test getOptionalBooleanProperty with existing boolean prop in command line config returns Some.") {
    val key = "key"
    val value = true
    val option = s"-D$key=$value"
    val c = Configuration(arrWithRules ++ Array(option))
    val result = c.getOptionalBooleanProperty(key)
    assert(result.get === value)
  }
  test("test getOptionalBooleanProperty with non-boolean prop returns None") {
    val key = "key"
    val value = 10
    val option = s"-D$key=$value"
    val c = Configuration(arrWithRules ++ Array(option))
    assert(c.getOptionalBooleanProperty(key).isEmpty)
  }
  test("test getOptionalBooleanProperty with null") {
    val c = Configuration(arrWithRules)
    assert(c.getOptionalBooleanProperty(null).isEmpty)
  }
  test("test getOptionalBooleanProperty with existing bool prop in config file config") {
    val propHeader = "el.boolean"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val expectedValue = getCustomConfig(testConfPath).resolve.getBoolean(propHeader)
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val result = c.getOptionalBooleanProperty(propHeader)
    assert(result.get === expectedValue)
  }
  test("test getOptionalBooleanProperty with existing non-boolean property in config file config.") {
    val propHeader = "el.int"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    assertThrows[WrongType] {
      c.getMandatoryBooleanProperty(propHeader)
    }
  }
  test("test getOptionalBooleanProperty with un-existing property returns None") {
    val propHeader = "nonexistent.property"
    assume(!getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")

    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    assert(c.getOptionalBooleanProperty(propHeader).isEmpty)
  }

  /** getMandatoryBooleanProperty TESTS */

  test("test getMandatoryBooleanProperty with existing boolean prop in command line config returns value.") {
    val key = "key"
    val value = true
    val option = s"-D$key=$value"
    val c = Configuration(arrWithRules ++ Array(option))
    val result = c.getMandatoryBooleanProperty(key)
    assert(result === value)
  }
  test("test getMandatoryBooleanProperty with non-boolean prop in cl throws WrongType") {
    val key = "key"
    val value = 0
    val option = s"-D$key=$value"
    val c = Configuration(arrWithRules ++ Array(option))
    assertThrows[WrongType] {
      c.getMandatoryBooleanProperty(key)
    }
  }
  test("test getMandatoryBooleanProperty with null throws NullPointerException") {
    val c = Configuration(arrWithRules)
    assertThrows[NullPointerException] {
      c.getMandatoryBooleanProperty(null)
    }
  }
  test("test getMandatoryBooleanProperty with existing bool prop in config file config returns bool value") {
    val propHeader = "el.boolean"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val expectedValue = getCustomConfig(testConfPath).resolve.getBoolean(propHeader)
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val result = c.getMandatoryBooleanProperty(propHeader)
    assert(result === expectedValue)
  }
  test("test getMandatoryBooleanProperty with existing non-boolean property in config file config throws WrongType") {
    val propHeader = "el.string"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    assertThrows[WrongType] {
      c.getMandatoryBooleanProperty(propHeader)
    }
  }
  test("test getMandatoryBooleanProperty with un-existing property throws Missing") {
    val propHeader = "nonexistent.property"
    assume(!getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")

    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    assertThrows[Missing] {
      c.getMandatoryBooleanProperty(propHeader)
    }
  }

  /** getOptionalIntProperty TESTS */

  test("test getOptionalIntProperty with existing int prop in command line config.") {
    val key = "key"
    val value = -150
    val option = s"-D$key=$value"
    val c = Configuration(arrWithRules ++ Array(option))
    val result = c.getOptionalIntProperty(key)
    assert(result.get === value)
  }
  test("test getOptionalIntProperty with null") {
    val c = Configuration(arrWithRules)
    assert(c.getOptionalIntProperty(null).isEmpty)
  }
  test("test getOptionalIntProperty with existing int property in config file config") {
    val propHeader = "el.int"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val expectedValue = getCustomConfig(testConfPath).resolve.getInt(propHeader)
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val result = c.getOptionalIntProperty(propHeader)
    assert(result.get === expectedValue)
  }
  test("test getOptionalIntProperty with existing non-int parsable property in command line config returns None") {
    val key = "key"
    val value = "non-int-parsable"
    val option = s"-D$key=$value"
    val c = Configuration(arrWithRules ++ Array(option))
    assert(c.getOptionalIntProperty(key).isEmpty)
  }
  test("test getOptionalIntProperty with existing double property in config file config ") {
    val propHeader = "el.double"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val expectedValue = getCustomConfig(testConfPath).resolve.getDouble(propHeader)
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val result = c.getOptionalIntProperty(propHeader)
    assert(result.get === expectedValue.toInt)
  }
  test("test getOptionalIntProperty with existing non-int property in config file config ") {
    val propHeader = "el.list"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    assert(c.getOptionalIntProperty(propHeader).isEmpty)
  }
  test("test getOptionalIntProperty with un-existing property") {
    val propHeader = "non.existent"
    assume(!getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be not defined in the provided configuration file in order that this test is performed.")
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    assert(c.getOptionalIntProperty(propHeader).isEmpty)
  }

  /** getMandatoryIntProperty TESTS */

  test("test getMandatoryIntProperty with null throws NullPointerException") {
    val c = Configuration(arrWithRules)
    assertThrows[NullPointerException] {
      c.getMandatoryIntProperty(null)
    }
  }
  test("test getMandatoryIntProperty with existing int prop in command line returns the value") {
    val key = "key"
    val value = -0
    val option = s"-D$key=$value"
    val c = Configuration(arrWithRules ++ Array(option))
    val result = c.getMandatoryIntProperty(key)
    assert(result === value)
  }
  test("test getMandatoryIntProperty with existing int property in config file config returns the value") {
    val propHeader = "el.int"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val expectedValue = getCustomConfig(testConfPath).resolve.getInt(propHeader)
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    val result = c.getMandatoryIntProperty(propHeader)
    assert(result === expectedValue)
  }
  test("test getMandatoryIntProperty with existing non-int prop non-parsable to int in cl throws WrongType") {
    val key = "key"
    val value = "hello"
    val option = s"-D$key=$value"
    val c = Configuration(arrWithRules ++ Array(option))
    assertThrows[WrongType] {
      c.getMandatoryIntProperty(key)
    }
  }
  test("test getMandatoryIntProperty with existing non-int property in config file config ") {
    val propHeader = "el.list"
    assume(getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    assertThrows[WrongType] {
      c.getMandatoryIntProperty(propHeader)
    }
  }
  test("test getMandatoryIntProperty with un-existing property") {
    val propHeader = "nonexistent.property"
    assume(!getCustomConfig(testConfPath).resolve.hasPath(propHeader),
      s"$propHeader must be defined in the provided configuration file in order that this test is performed.")
    val c = Configuration(arrWithRules ++ Array("-c", testConfPath))
    assertThrows[Missing] {
      c.getMandatoryIntProperty(propHeader)
    }
  }

}
