package es.uniovi.uo237525.dataquality.engine.rules.evaluation

import es.uniovi.uo237525.dataquality.SparkSessionTestWrapper
import es.uniovi.uo237525.dataquality.parser.JsonParser
import org.scalatest.{BeforeAndAfterAll, FunSuite}
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types._

class EvNumericFormatTest extends FunSuite with SparkSessionTestWrapper with BeforeAndAfterAll {

  import spark.implicits._

  val txtPath: String = getClass.getResource("/txt/numeric-format-dataset.txt").getPath
  private val someDf: DataFrame = spark.read.option("delimiter", "|").option("header", value = true).csv(txtPath).cache()


  override def afterAll(): Unit = someDf.unpersist()

  /** TEST CHECK PRECONDITIONS */
  test("test CHECK PRE-CONDITIONS: application on un-existing field") {
    val opts = EvNumericFormatOptions(".", ",", 0)
    val rule = EvNumericFormat(field = "non-existing", options = opts)
    assert(rule.checkPreconditions(someDf).isInstanceOf[Some[_]])
  }
  test("test CHECK PRE-CONDITIONS: can be applied only on columns of type: StringType") {
    val opts = EvNumericFormatOptions(",", ".", 0)
    for (c <- someDf.schema) {
      val columnName = c.name
      val rule = EvNumericFormat(field = columnName, options = opts)
      val canBeApplied = rule.checkPreconditions(someDf).isEmpty
      c.dataType match {
        case StringType => assert(canBeApplied)
        case _ => assert(!canBeApplied)
      }
    }
  }

  test("test QUALITY PRE-CONDITIONS: application on existing non-string field") {
    val rule = EvNumericFormat(field = "nums",
      options = EvNumericFormatOptions(",", ".", 4))
    val someDF = Seq(1, 2, 3).toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }

  test("test CHECK PRE-CONDITIONS: decMax < 0 prevent from applying the rule") {
    val opts = EvNumericFormatOptions(",", ".", -5)
    val rule = EvNumericFormat(field = "c1", options = opts)
    assert(rule.checkPreconditions(someDf).isDefined)

  }
  test("test CHECK PRE-CONDITIONS: decMax >= 0 are allow to apply the rule") {
    val opts0 = EvNumericFormatOptions(",", ".", 0)
    val ruleWith0 = EvNumericFormat(field = "nums", options = opts0)
    val someDf = Seq("1", "2", "3").toDF("nums")
    assert(ruleWith0.checkPreconditions(someDf).isEmpty)

    val optsPos = EvNumericFormatOptions(",", ".", 5)
    val ruleWithPos = EvNumericFormat(field = "nums", options = optsPos)
    assert(ruleWithPos.checkPreconditions(someDf).isEmpty)
  }

  test("test QUALITY PRE-CONDITIONS: thousands separator null") {
    val rule = EvNumericFormat(field = "nums",
      options = EvNumericFormatOptions(null, ".", 2))
    val someDF = Seq("1", "2", "3").toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: decimal separator null") {
    val rule = EvNumericFormat(field = "nums",
      options = EvNumericFormatOptions(",", null, 2))
    val someDF = Seq("1", "2", "3").toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }

  /** TEST APPLICATION OF THE RULE */
  test("test QUALITY APPLY: application on existing fields (generic)") {
    val rule = EvNumericFormat(field = "c1",
      options = EvNumericFormatOptions(",", ".", 1))
    assert(rule.apply(someDf).get.matched == 0)

    val rule1 = EvNumericFormat(field = "c1",
      options = EvNumericFormatOptions(",", ".", 2))
    // knowing in advance the result
    // this column contains null, contains non-grouped valid numbers
    assert(rule1.apply(someDf).get.matched == 11)
  }

  test("test QUALITY APPLY: application on un-existing fields") {
    val rule = EvNumericFormat(field = "non-existing",
      options = EvNumericFormatOptions(",", ".", 4))
    val someDF = Seq("1", "2", "3").toDF("nums")
    assert(rule.apply(someDF).isFailure)
  }

  test("test APPLY: 0 as decMax match only integers") {
    val sequence = Seq("-121", "121", "0", "221.0", "3.", "-223.4")
    val numIntegers = sequence.count(el => {
      val intRegex = """^-?\d+$""".r
      el match {
        case intRegex(_*) => true
        case _ => false
      }
    })
    val df = sequence.toDF("decimals")

    val opts = EvNumericFormatOptions(",", ".", 0)
    val rule = EvNumericFormat(field = "decimals", options = opts)
    val log = rule.apply(df).get

    assert(log.matched === numIntegers)
  }

  test("test APPLY: using special character as separator and positive decMax match digit-sep-digit") {
    val sequence = Seq("-121\\.", "121.123", "0", "221\\.0", "3.", "-223\\.4", "225.9")

    val df = sequence.toDF("decimals")

    val opts = EvNumericFormatOptions(".", "\\.", 1)
    val rule = EvNumericFormat(field = "decimals", options = opts)
    val log = rule.apply(df).get

    // only numbers that have /. as separator and their #decimals are max = 1
    assert(log.matched === 4)
  }

  test("test APPLY: using normal character as separator and 0 as decMax match only only-digit-numbers") {
    val sequence = Seq("-121", "121", "0", "221.0", "3.", "-223.4", null, "5,6", "5.6")

    val numIntegers = sequence.count(el => {
      val intRegex = """^-?\d+$""".r
      el match {
        case intRegex(_*) => true
        case _ => false
      }
    })
    val df = sequence.toDF("decimals")

    val opts = EvNumericFormatOptions(".", ",", 0)
    val rule = EvNumericFormat(field = "decimals", options = opts)
    val log = rule.apply(df).get
    assert(log.matched === numIntegers)
  }

  test("test thousands separator") {
    val sequence = Seq("1.256", "57.895.256", "112.21", "-121", "121", "0", "221.0", "3.", "-223.4", null, "5,6", "5.6", "221,0")
    val df = sequence.toDF("decimals")

    val opts = EvNumericFormatOptions(".", "", 0)
    val rule = EvNumericFormat(field = "decimals", options = opts)
    val log = rule.apply(df).get
    assert(log.matched == 5)
  }

  test("test thousands sep 2") {
    val wrapper = JsonParser.extractJsonSchema("F:\\Nacho\\TFG-DQ\\VafrantVM\\vagrantvm-bdq\\queries\\read-hive.json", isAbsolute = true)
    val rule = wrapper.rules.tail.head
    val df = Seq("276", "276", "690", "134", "587", "586", "102", "264", "688").toDF("duration")
    val res = rule.apply(df)
    res
  }

  test("test APPLY: using normal character as separator and positive decMax") {
    val sequence = Seq("-121", "121", "0", "221.0", "3.", "-223.4", null, "5,6", "5.6", "221,0")
    val numDecimals = 5
    //    val expectedMatches = sequence.count(el => {
    //      val regex = (s"^-?\\d+,\\d+{1,$numDecimals}" + "$").r
    //      el match {
    //        case regex(_*) => true
    //        case _ => false
    //      }
    //    })
    val df = sequence.toDF("decimals")

    val opts = EvNumericFormatOptions(".", ",", 5)
    val rule = EvNumericFormat(field = "decimals", options = opts)
    val log = rule.apply(df).get

    // only numbers that have /. as separator and their #decimals are max = 1
    assert(log.matched === 5)
  }

  test("test APPLY: null elements makes rule nor fail nor count null rows as matched") {
    val sequence = Seq("", null, null, null, null, null, null, null, null)

    val df = sequence.toDF("decimals")

    val opts = EvNumericFormatOptions(",", ",", 5)
    val rule = EvNumericFormat(field = "decimals", options = opts)
    val log = rule.apply(df).get

    // only numbers that have /. as separator and their #decimals are max = 1
    assert(log.matched === 0)
  }

}
