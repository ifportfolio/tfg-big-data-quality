#!/bin/bash

VERSION=1.0.0
WEBAPP_JAR_ARCHIVE=dataqualityserver-$VERSION.jar
BDQ_JAR_ARCHIVE=big-data-quality-$VERSION-jar-with-dependencies.jar

# Fill the Redis Properties
REDIS_HOST=localhost
REDIS_PORT=6379
REDIS_ALERTS_LIST_KEY=bdqWebExecList

BIN_DIR=$(cd `dirname $0` && pwd)/../bin
LOGS_DIR=$BIN_DIR/../logs
FILES_DIR=$BIN_DIR/../files

nohup java -jar $BIN_DIR/$WEBAPP_JAR_ARCHIVE --bdq.jar.path=$BIN_DIR/$BDQ_JAR_ARCHIVE --files.path=$FILES_DIR --redis.host=$REDIS_HOST --redis.port=$REDIS_PORT --redis.executions.key=$REDIS_ALERTS_LIST_KEY > $LOGS_DIR/dataqualityserver-$VERSION.log 2>&1 &
echo $! > $LOGS_DIR/pid-bdq-web
