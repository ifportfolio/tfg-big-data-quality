import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { LaunchBdqService } from 'src/app/services/launch-bdq.service';
import { LaunchResult } from 'src/app/model/LaunchResult';

/**
 * Component where all the results of a certain executinos are shown.
 * It is also the component from the user can launch a new execution.
 */
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  /**
   * Injects the launch service to launch a new execution.
   * @param launchService LaunchService instance injected
   */
  constructor(private launchService: LaunchBdqService) { }

  /**
   * Indicates whether the client is waiting for an execution response.
   */
  @Input() loading: boolean;
  /**
   * In case of error, receives the error reason from the parent
   */
  @Input() errorMsg:string;
  /**
   * Received launch result for the last execution
   */
  @Input() launchResult: LaunchResult;
  /**
   * Emitter that communicates the launch event to the parent.
   */
  @Output() launch = new EventEmitter();

  /**
   * Empty
   */
  ngOnInit() {}
  
  /**
   * Triggered when the user click on the laun button.
   * It emits the event to the parent component.
   */
  launcherClick() {
    console.log("launching from child")
    this.launch.next()
  }

}
