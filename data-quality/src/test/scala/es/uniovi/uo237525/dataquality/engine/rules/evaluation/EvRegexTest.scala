package es.uniovi.uo237525.dataquality.engine.rules.evaluation

import java.sql.{Date, Timestamp}
import java.time.Instant

import es.uniovi.uo237525.dataquality.SparkSessionTestWrapper
import org.scalatest.{BeforeAndAfterAll, FunSuite}

import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._


class EvRegexTest extends FunSuite with SparkSessionTestWrapper with BeforeAndAfterAll {

  private val ts = Timestamp.from(Instant.now())
  private val date = new Date(System.currentTimeMillis())
  private val map = Map("example1" -> "ex1", "example2" -> "ex2")
  private val mulArray = Array(Array("array of array1"), Array("array of array2"))
  private val seq = Seq(Row(1, Long.MaxValue, null, false, 0.5, ts, Array("val1", "val2"), mulArray, date, map),
    Row(2, Long.MaxValue, "value", null, null, ts, Array("val1", "val2"), mulArray, null, map),
    Row(3, null, "", true, 0.004, ts, null, null, date, null),
    Row(4, 3434343.toLong, "value2345", true, 0.00344, ts, Array(), null, date, null),
    Row(null, null, "6757656", null, null, null, null, null, null, null))

  val someSchema = StructType(
    StructField("int", IntegerType) ::
      StructField("long", LongType) ::
      StructField("string", StringType) ::
      StructField("boolean", BooleanType) ::
      StructField("double", DoubleType) ::
      StructField("timestamp", TimestampType) ::
      StructField("array", ArrayType(StringType)) ::
      StructField("multiple", ArrayType(ArrayType(StringType))) ::
      StructField("date", DateType) ::
      StructField("map", MapType(StringType, StringType)) :: Nil)

  val someDf: DataFrame = spark.createDataFrame(
    spark.sparkContext.parallelize(seq),
    StructType(someSchema))


  override def afterAll(): Unit = someDf.unpersist()

  /** TEST CHECK PRECONDITIONS */
  test("test CHECK PRE-CONDITIONS: application on un-existing field") {
    val opts = EvRegexOptions("", 0)
    val rule = EvRegex(field = "non-existing", options = opts)
    assert(rule.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: application on existing field") {
    val opts = EvRegexOptions("string")
    val rule = EvRegex(field = someDf.schema.fields.head.name, options = opts)
    assert(rule.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: null regex") {
    val opts = EvRegexOptions(null, 0)
    val rule = EvRegex(field = someDf.schema.fields.head.name, options = opts)
    assert(rule.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: incorrect regex") {
    val rule = EvRegex(field = "string", options = EvRegexOptions("((][", 0))
    assert(rule.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: negative capturing group") {
    val rule = EvRegex(field = "string", options = EvRegexOptions("\\d+", -1))
    assert(rule.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: check preconditions in allowed field type") {
    val rule = EvRegex(field = "string", options = EvRegexOptions("\\d+"))
    assert(rule.checkPreconditions(someDf).isEmpty)
  }

  test("test QUALITY PRE-CONDITIONS: numeric fields") {
    // int
    val intF = "int"
    val ruleInt = EvRegex(field = intF, options = EvRegexOptions("^\\d+"))
    assert(ruleInt.checkPreconditions(someDf).isDefined)
    // long
    val longF = "long"
    val ruleLong = EvRegex(field = longF, options = EvRegexOptions("^\\d+"))
    assert(ruleLong.checkPreconditions(someDf).isDefined)
    // decimal / double
    val doubleF = "double"
    val ruleDouble = EvRegex(field = doubleF, options = EvRegexOptions("^\\d+\\.\\d+$"))
    assert(ruleDouble.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: BOOLEAN && TIMESTAMP fields") {

    // bool
    val boolF = "boolean"
    val ruleBool = EvRegex(field = boolF, options = EvRegexOptions("^\\d+"))
    assert(ruleBool.checkPreconditions(someDf).isDefined)
    // timestamp
    val tsF = "timestamp"
    val ruleTs = EvRegex(field = tsF, options = EvRegexOptions("^\\d+"))
    assert(ruleTs.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: ARRAY && MAP field types") {
    val rule = EvRegex(field = "array", options = EvRegexOptions("\\d+"))
    assert(rule.checkPreconditions(someDf).isDefined)

    val rule2 = EvRegex(field = "multiple", options = EvRegexOptions("\\d+"))
    assert(rule2.checkPreconditions(someDf).isDefined)

    val rule3 = EvRegex(field = "map", options = EvRegexOptions("\\d+"))
    assert(rule3.checkPreconditions(someDf).isDefined)
  }


  /** TEST APPLICATION OF THE RULE */
  test("test QUALITY APPLY: string fields") {
    val rule = EvRegex(field = "string", options = EvRegexOptions("\\d+"))
    val matchedInt = rule.apply(someDf).get
    val expectedValue = someDf.select("string").filter(col("string").isNotNull).collect()
      .map(_.getString(0)).count(el => el.nonEmpty && el.exists(_.isDigit))
    assert(matchedInt.matched == expectedValue)
  }

}
