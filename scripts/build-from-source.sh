#!/bin/bash

VERSION=1.0.0
SH_LOCATION=$(cd `dirname $0` && pwd)
DEPLOY_PATH=$SH_LOCATION/bdq-$VERSION

##Git Variables
REPO_NAME=tfg-big-data-quality
REPO_URL=https://ignaciofdez@bitbucket.org/ignaciofernandezsoloteam/$REPO_NAME.git
REPO_BRANCH=release/$VERSION

## SCRIPTS
SCRIPTS_DIR=$SH_LOCATION/$REPO_NAME/scripts

## BDQ COMPONENT
BDQ_ROOT_DIR=$SH_LOCATION/$REPO_NAME/data-quality
BDQ_JAR_ARCHIVE=big-data-quality-$VERSION-jar-with-dependencies.jar

## BDQ WEBAPP COMPONENT
WEBAPP_ROOT_DIR=$SH_LOCATION/$REPO_NAME/data-quality-webapp
WEBAPP_JAR_ARCHIVE=dataqualityserver-$VERSION.jar

#Git clone
function cloneFromBitBucket {
	echo "=================================================="
	echo "Step 1/6 -> Cloning from BitBucket repository"
	echo "=================================================="
	git clone $REPO_URL --branch $REPO_BRANCH
}

function installComponents {
	echo "=================================================="
	echo "Step 2/6 -> Building BDQ component"
	echo "=================================================="
	cd $BDQ_ROOT_DIR
	echo "Packaging maven bdq jar in batch mode"
	mvn -B clean package
	echo "Copying generated bdq jar to root folder"
	cp -a $BDQ_ROOT_DIR/target/$BDQ_JAR_ARCHIVE $SH_LOCATION && rm -rf $BDQ_ROOT_DIR/target
	echo "=================================================="
	echo "Step 3/6 -> Building BDQ WEBAPP component"
	echo "=================================================="
	cd $WEBAPP_ROOT_DIR
	echo "Packaging maven bdq jar in batch mode"
	mvn -B clean install
	echo "Copying generated bdq webapp jar to root folder"
	cp -a $WEBAPP_ROOT_DIR/dataqualityserver/target/$WEBAPP_JAR_ARCHIVE $SH_LOCATION && rm -rf $WEBAPP_ROOT_DIR/dataqualityserver/target
}

function deployComponents {
	echo "=================================================="
	echo "Step 4/6 -> Deploying built components"
	echo "=================================================="
	echo "Creating deployment directory"
	mkdir -p $DEPLOY_PATH
	echo "Creating files dir"
	mkdir -p $DEPLOY_PATH/files
	echo "Creating logs dir"
	mkdir -p $DEPLOY_PATH/logs
	echo "Creating bin dir"
	mkdir -p $DEPLOY_PATH/bin
	echo "Copying JAR files to bin directory"
	cp -a $SH_LOCATION/$BDQ_JAR_ARCHIVE $DEPLOY_PATH/bin && rm -r $SH_LOCATION/$BDQ_JAR_ARCHIVE
	cp -a $SH_LOCATION/$WEBAPP_JAR_ARCHIVE $DEPLOY_PATH/bin && rm -r $SH_LOCATION/$WEBAPP_JAR_ARCHIVE
	echo "Create scripts dir"
	cp -a $SCRIPTS_DIR $DEPLOY_PATH && rm -r $SCRIPTS_DIR
	rm -rf $SH_LOCATION/$REPO_NAME
}

function cleanFiles {
	echo "=================================================="
	echo "Step 5/6 -> Cleaning useless files"
	echo "=================================================="
	rm -rf $SH_LOCATION/$REPO_NAME
}

function runWebapp {
	echo "=================================================="
	echo "Step 6/6 -> Running BDQ Web App (webapp)"
	echo "=================================================="
	BDQ_CLIENT_RUN_SH=start-bdq-client.sh
	chmod 777 $DEPLOY_PATH/scripts/$BDQ_CLIENT_RUN_SH
	$DEPLOY_PATH/scripts/$BDQ_CLIENT_RUN_SH
}

cloneFromBitBucket
installComponents
deployComponents
cleanFiles
# disabled for the moment
#runWebapp
