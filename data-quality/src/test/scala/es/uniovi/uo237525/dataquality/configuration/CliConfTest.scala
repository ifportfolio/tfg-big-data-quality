package es.uniovi.uo237525.dataquality.configuration

import java.io.{File, FileNotFoundException}

import com.typesafe.config.{Config, ConfigFactory}
import org.rogach.scallop.exceptions._
import org.scalatest.{BeforeAndAfterAll, FunSuite}

class CliConfTest extends FunSuite with BeforeAndAfterAll {

  private val RULES_PARAM_SHORT = "-r"
  private val RULES_PARAM_LONG = "--rules"
  private val CONFIG_PARAM_SHORT = "-c"
  private val CONFIG_PARAM_LONG = "--config"

  private def getCustomConfig(path: String): Config = {
    ConfigFactory.parseFile(new File(path))
  }

  /** RULES PARAM TESTS */
  test("test required argument \"rules\": check correct value parsing") {
    val pathToRules = "path/to/rules"

    val confShort = Configuration(Array(RULES_PARAM_SHORT, pathToRules))
    withClue(s"The path recognized by the config should have been: $pathToRules") {
      assertResult(pathToRules) {
        confShort.getRulesPath
      }
    }
  }
  test("test required argument \"rules\": empty path") {
    val empty = Configuration(Array(RULES_PARAM_SHORT, ""))
    withClue(s"The path recognized should have been empty") {
      assert(empty.getRulesPath.isEmpty)
    }
  }
  test("test required argument \"rules\": check long flag") {
    val pathToRules = "path/to/rules"
    val confLong = Configuration(Array(RULES_PARAM_LONG, pathToRules))
    withClue(s"The path recognized by the config should be $pathToRules") {
      assertResult(pathToRules) {
        confLong.getRulesPath
      }
    }
  }
  test("test required argument \"rules\": check short flag") {
    val pathToRules = "path/to/rules"
    val confLong = Configuration(Array(RULES_PARAM_SHORT, pathToRules))
    withClue(s"The path recognized by the config should be $pathToRules") {
      assertResult(pathToRules) {
        confLong.getRulesPath
      }
    }
  }
  test("test required argument \"rules\": not provided") {
    // if rules not provided the program calls sys.exit
    withClue("rules argument is not provided so the program should have finished") {
      assertThrows[RequiredOptionNotFound] {
        Configuration(Seq())
      }
    }
  }

  /** CONFIG PARAM */
  test("test optional argument \"config\": valid path") {
    val pathToConfig = getClass.getResource("/conf/test.conf").getPath
    val expectedFilename = getCustomConfig(pathToConfig).resolve.origin.filename
    val confShort = Configuration(Array(RULES_PARAM_SHORT, "", CONFIG_PARAM_SHORT, pathToConfig))
    val actualConfig = confShort.confFile
    assert(actualConfig.origin.description.contains(expectedFilename))
  }
  test("test optional argument \"config\": invalid path throws FileNotFound") {
    val pathToConfig = "/conf/non-existent.conf"
    assertThrows[FileNotFoundException] {
      Configuration(Array(RULES_PARAM_SHORT, "", CONFIG_PARAM_SHORT, pathToConfig))
    }
  }
  test("test optional argument \"config\": empty path throws FileNotFound") {
    val pathToConfig = ""
    assertThrows[FileNotFoundException] {
      Configuration(Array(RULES_PARAM_SHORT, "", CONFIG_PARAM_SHORT, pathToConfig))
    }
  }
  test("test optional argument \"config\": not provided loads default.conf") {
    withClue("config argument is not provided so it should load the default.conf by default") {
      val c = Configuration(Seq(RULES_PARAM_SHORT, "")).confFile
      assert(c.origin.description.contains("default.conf"))
    }
  }

  /** NATIVE OPTIONS HELP & VERSION */
  test("test native --help option") {
    Configuration(Array("--help"))
    succeed
  }

  test("test native --version option") {
    Configuration(Array("--version"))
    succeed
  }

  /** PROPERTIES */
  test("test passing null properties throws WrongOptionFormat") {
    val key = null
    val value = ""
    val option = s"-D$key=$value"
    assertThrows[WrongOptionFormat] {
      Configuration(Array(RULES_PARAM_SHORT, "", option))
    }
  }

  test("test CLI correct properties are recognized") {
    val key = "key"
    val value = "value"
    val option = s"-D$key=$value"
    val conf = Configuration(Array(RULES_PARAM_SHORT, "", option))
    assert(conf.commandLineProperties.contains(key))
    assert(conf.commandLineProperties(key) === value)
  }

  test("test CLI correct properties overrides previous defined values") {
    val envVar = sys.env.toList.tail.head
    val newVal = "newValueFromCli"
    val envVarHeader = envVar._1
    val envVarPrevVal = envVar._2
    assume(newVal !== envVarPrevVal)
    val option = s"-D$envVarHeader=$newVal"
    val conf = Configuration(Array(RULES_PARAM_SHORT, "", option))
    assert(conf.commandLineProperties(envVarHeader) === newVal)
  }


}
