import 'brace';
import 'brace/mode/properties';
import 'brace/theme/chrome';
import { AceComponent, AceDirective, AceConfigInterface } from 'ngx-ace-wrapper';
import { Component, AfterViewInit, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { NewFileDialogData } from 'src/app/model/NewFileDialogData';
import { MatDialogRef, MatDialog } from '@angular/material';
import { AssetLoaderService } from 'src/app/services/asset-loader.service';

/**
 * Component for the configuration file editor.
 * 
 * Sub-component of the AppComponent.
 */
@Component({
  selector: 'app-config-editor',
  templateUrl: './config-editor.component.html',
  styleUrls: ['./config-editor.component.css']
})
export class ConfigEditorComponent implements AfterViewInit {

  /**
   * The template for the configuration file.
   */
  private configTemplate;

  /**
   * Configuration for the AceEditor
   */
  public config: AceConfigInterface = {
    mode: 'properties',
    theme: 'chrome',
    readOnly: true,
    fontSize: "9pt"
  };

  /**
   * Receives the config file content from the parent.
   */
  @Input() fileContent: string;
  /**
   * Emits on file content change to the parent.
   */
  @Output() fileContentChange = new EventEmitter()
  /**
   * Receives the filename from the parent.
   */
  @Input() filename: string;
  /**
   * Emits on file name change to the parent.
   */
  @Output() filenameChange = new EventEmitter()

  /**
   * Reference to the AceEditor component.
   */
  @ViewChild(AceComponent) componentRef?: AceComponent;
  /**
   * Reference to the AceEditor directive
   */
  @ViewChild(AceDirective) directiveRef?: AceDirective;

  /**
   * The label that represents whether the editor in in read-only or editable state.
   */
  private editorMode = this.config.readOnly ? "Readonly" : "Editable";


  /**
   * Injects the needed services for the component.
   * Loads the config file predefined template stores in the assets folder.
   * 
   * @param assetLoader Service for injecting the file that contains the template
   * @param createNewConfigDialog Dialog for generation a new file
   */
  constructor(assetLoader: AssetLoaderService, public createNewConfigDialog: MatDialog) {
    assetLoader.loadTextFileAsset("configTemplate.conf").subscribe(data => {
      // console.log(data)
      this.configTemplate = data;
    });
  }

  /**  
   * No functionality 
   */
  ngAfterViewInit(): void { }

  /**
   * The event thrown when the selects a file to load its content from the file explorer.
   * 
   * @param event the event containing the target files.
   */
  fileSelected(event: any) {
    let file = event.target.files[0];
    this.filename = file.name
    var reader = new FileReader();
    reader.onloadend = (fr: any) => {
      this.config.readOnly = true
      this.editorMode = this.config.readOnly ? "Readonly" : "Editable"
      this.fileContent = fr.target.result;
      this.fileContentChange.emit(this.fileContent)
      this.filenameChange.emit(this.filename)
    }
    reader.readAsText(file);
  }

  /**
   * Inverts the state of the readonly toogle.
   */
  public toggleReadonly(): void {
    this.config.readOnly = this.config.readOnly ? false : true;
    this.editorMode = this.config.readOnly ? "Readonly" : "Editable"
  }

  /**
   * This function is triggered on each change of the editor.
   */
  onEditorChange() {
    this.fileContentChange.emit(this.fileContent)
  }

  /**
   * Handles the process of creation a new file using the CreateConfigFileDialog.
   * 
   * Executes when the user clicks on the create new button.
   */
  createNewFile() {
    const dialogRef = this.createNewConfigDialog.open(CreateConfigFileDialog, { disableClose: true });
    dialogRef.afterClosed().subscribe(configFileCreationTemplate => {
      console.log("Closing creation dialog")
      if (configFileCreationTemplate != undefined) {
        this.config.readOnly = this.config.readOnly = false;
        this.editorMode = "Editable";
        if (configFileCreationTemplate.useTemplate) {
          this.fileContentChange.emit(this.configTemplate);
        }
        else {
          this.fileContentChange.emit(" ")
        }
        this.filenameChange.emit(configFileCreationTemplate.filename + ".conf")
      }
    });
  }

}

/**
 * Modal component that provides the functinality for generating a new configuration file.
 */
@Component({
  selector: 'create-config-file-dialog',
  templateUrl: 'create-config-file-dialog.html',
})
export class CreateConfigFileDialog {

  /** 
   * Filename provided by the user
   */
  filename = ""
  /** 
   * Flag that indicates if the checkbox for the template usage is checked
   */
  useTemplate = false

  /**
   * Injects the dialog instance in the component.
   * @param dialogRef The CreateConfigDialog instance
   */
  constructor(public dialogRef: MatDialogRef<CreateConfigFileDialog>) { }

  /**
   * Triggered when the user cancels the generating file process.
   * Closes the dialog.
   */
  cancel() {
    this.dialogRef.close()
  }

  /**
   * Triggered on the user confirmation of the generation of a new file action.
   */
  onConfirm() {
    let cFile = new NewFileDialogData();
    cFile.filename = this.filename.trim();
    cFile.useTemplate = this.useTemplate;
    this.dialogRef.close(cFile);
  }

}
