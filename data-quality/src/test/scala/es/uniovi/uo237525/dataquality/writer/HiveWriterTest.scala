package es.uniovi.uo237525.dataquality.writer

import java.sql.{Date, Timestamp}
import java.time.Instant
import java.util.UUID

import es.uniovi.uo237525.dataquality.{InputTypes, SparkSessionTestWrapper}
import es.uniovi.uo237525.dataquality.engine.rules.remediation.{FillEmptyRecords, FillEmptyRecordsOption}
import es.uniovi.uo237525.dataquality.parser.models.OutputParams
import es.uniovi.uo237525.dataquality.storage.writers.HiveWriter
import org.apache.spark.sql._
import org.apache.spark.sql.catalyst.analysis.NoSuchDatabaseException
import org.apache.spark.sql.catalyst.parser.ParseException
import org.apache.spark.sql.types._
import org.scalatest.tagobjects.Slow
import org.scalatest.{BeforeAndAfterAll, FunSuite}

class HiveWriterTest extends FunSuite with SparkSessionTestWrapper with BeforeAndAfterAll {

  import spark.implicits._

  private val DEFAULT_DIR_LOCATION: String = "./"
  private val DEFAULT_DIR_NAME: UUID = UUID.randomUUID()

  private val ts = Timestamp.from(Instant.now())
  private val date = new Date(System.currentTimeMillis())
  private val map = Map("example1" -> "ex1", "example2" -> "ex2")
  private val mulArray = Array(Array("array of array1"), Array("array of array2"))
  private val seq = Seq(
    Row(1, Long.MaxValue, null, false, 0.5, ts, Array("val1", "val2"), mulArray, date, map),
    Row(2, Long.MaxValue, "val,ue", null, null, ts, Array("val1", "val2"), mulArray, null, map),
    Row(3, null, null, true, 0.004, ts, null, null, date, null),
    Row(null, null, null, null, null, null, null, null, null, null))

  val someSchema = StructType(
    StructField("id", IntegerType) ::
      StructField("long", LongType) ::
      StructField("word", StringType) ::
      StructField("bool", BooleanType) ::
      StructField("double", DoubleType) ::
      StructField("timestamp", TimestampType) ::
      StructField("array", ArrayType(StringType)) ::
      StructField("multiple", ArrayType(ArrayType(StringType))) ::
      StructField("date", DateType) :: Nil)

  val someDf: DataFrame = spark.createDataFrame(
    spark.sparkContext.parallelize(seq),
    StructType(someSchema)
  ).cache()

  private val db = "hiveWriter"

  override def beforeAll(): Unit = {
    spark.sql(s"DROP DATABASE IF EXISTS $db CASCADE")
    spark.sql(s"CREATE DATABASE $db")
    spark.sql(s"USE $db")
  }


  override def afterAll(): Unit = {
    spark.sql(s"DROP DATABASE IF EXISTS $db CASCADE")
    spark.sql(s"DROP TABLE IF EXISTS ")
  }

  /** TEST OVERWRITE */

  /** DATABASE PARAM */
  test("test Overwrite: invalid database name") {
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_database = Some("non-existent")))

    assertThrows[ParseException] {
      writer.overwrite(someDf, spark).get
    }
  }

  test("test Overwrite: non-existent database") {
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_database = Some("nonExistent")))

    assertThrows[NoSuchDatabaseException] {
      writer.overwrite(someDf, spark).get
    }
  }

  test("test Overwrite: null database") {
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_database = Some(null)))

    assertThrows[NoSuchDatabaseException] {
      writer.overwrite(someDf, spark).get
    }
  }

  /** TABLE PARAM */
  test("test Overwrite: invalid table name") {
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_table = Some("non-existent")))

    assert(writer.overwrite(someDf, spark).isFailure)
  }

  test("test Overwrite: non-existent table") {
    val table = "nonExistent"
    spark.sql(s"DROP TABLE IF EXISTS $table")
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_table = Some(table)))

    writer.overwrite(someDf, spark).get
    assert(someDf.count() == spark.sql(s"select * from $table").count())
  }
  /** SCHEMA TESTS */
  test("test OverWrite when target matches schema") {
    val tableName = "testing"
    someDf.write.mode(SaveMode.Overwrite).saveAsTable(tableName)

    spark.catalog.listTables(spark.catalog.currentDatabase).show()

    val r = FillEmptyRecords(field = "id", options = FillEmptyRecordsOption(10))

    val remediedDf = r.apply(someDf).get.modifiedDF

    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_table = Some(tableName)))
    writer.overwrite(remediedDf, spark).get

    assert(spark.sql(s"SELECT * FROM $tableName").count() == someDf.count())
    assert(spark.sql(s"SELECT * FROM $tableName").filter(s"${r.field} is null").count() == 0)
  }

  test("test Overwrite: non-matching schema") {
    val tableName = "testing"
    spark.sql(s"DROP TABLE IF EXISTS $tableName ")
    someDf.write.mode(SaveMode.Overwrite).saveAsTable(tableName)
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_table = Some(tableName)))
    writer.overwrite(someDf, spark).get
    val newDf = Seq("1", "val2", "3").toDF()
    assertThrows[AnalysisException] {
      writer.overwrite(newDf, spark).get
    }
  }

  test("test Overwrite: null table") {
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_table = Some(null)))

    assertThrows[NullPointerException] {
      writer.overwrite(someDf, spark).get
    }
  }

  test("test Overwrite: EXTERNAL TABLE", Slow) {
    val externalTName = "external_table"
    spark.sql(s"DROP TABLE IF EXISTS $externalTName")
    spark.sql(
      s"""
         | CREATE EXTERNAL TABLE $externalTName (
         |  `id_file` bigint,
         |  `name_file` string)
         | LOCATION '/home/nacho/Escritorio/TFG/tfg-big-data-quality/data-quality/spark-warehouse'
       """.stripMargin)
    val newDf = Seq((1, "val1")).toDF("id_file", "name_file")
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_table = Some(externalTName)))
    writer.overwrite(newDf, spark).get
    assert(spark.sql(s"select * from $externalTName").count() == newDf.count())
    spark.sql(s"DROP TABLE $externalTName")
    val newDf2 = Seq((1, "val1"), (2, "val2")).toDF("id_file", "name_file")
    writer.overwrite(newDf2, spark).get
    assert(spark.sql(s"select * from $externalTName").count() == newDf2.count())
    spark.sql(s"DROP TABLE $externalTName")
  }

  /** TEST APPEND */
  test("test APPEND: invalid database name") {
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_database = Some("non-existent")))

    assertThrows[ParseException] {
      writer.append(someDf, spark).get
    }
  }

  test("test APPEND: non-existent database") {
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_database = Some("nonExistent")))

    assertThrows[NoSuchDatabaseException] {
      writer.append(someDf, spark).get
    }
  }

  test("test Append: null database") {
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_database = Some(null)))

    assertThrows[NoSuchDatabaseException] {
      writer.append(someDf, spark).get
    }
  }

  /** TABLE PARAM */
  test("test Append: invalid table name") {
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_table = Some("non-existent")))

    assert(writer.append(someDf, spark).isFailure)
  }

  test("test Append: non-existent table") {
    val table = "nonExistent"
    spark.sql(s"DROP TABLE IF EXISTS $table")
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_table = Some(table)))

    writer.append(someDf, spark).get
    assert(someDf.count() == spark.sql(s"select * from $table").count())
  }

  /** SCHEMA TESTS */
  test("test Append matching schema") {
    val tableName = "testing"
    someDf.write.mode(SaveMode.Overwrite).saveAsTable(tableName)

    val r = FillEmptyRecords(field = "id", options = FillEmptyRecordsOption(10))

    val remediedDf = r.apply(someDf).get.modifiedDF

    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_table = Some(tableName)))
    writer.append(remediedDf, spark).get

    assertResult(someDf.count() + remediedDf.count()) {
      spark.sql(s"select * from $tableName").count()
    }
  }

  test("test Append: non-matching schema") {
    val tableName = "testing"
    spark.sql(s"DROP TABLE IF EXISTS $tableName ")
    someDf.write.mode(SaveMode.Overwrite).saveAsTable(tableName)
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_table = Some(tableName)))
    writer.overwrite(someDf, spark).get
    val newDf = Seq("1", "val2", "3").toDF()
    assertThrows[AnalysisException] {
      writer.append(newDf, spark).get
    }
  }

  test("test Append: null table") {
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_table = Some(null)))

    assertThrows[NullPointerException] {
      writer.append(someDf, spark).get
    }
  }

  /** TEST OPEN */
  test("test OPEN with defined database and table") {
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_database = Some(db),
      hive_table = Some("test")))

    assert(writer.open().get.isInstanceOf[SparkSession])
  }

  test("test OPEN with null database") {
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_database = Some(null),
      hive_table = Some("test")))

    assert(writer.open().get.isInstanceOf[SparkSession])
  }

  test("test OPEN with null table") {
    val writer = new HiveWriter(OutputParams(target = InputTypes.HIVE,
      hive_database = Some(db),
      hive_table = Some(null)))

    assert(writer.open().get.isInstanceOf[SparkSession])
  }

}
