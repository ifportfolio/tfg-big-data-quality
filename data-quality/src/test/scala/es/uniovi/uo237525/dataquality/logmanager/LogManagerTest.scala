package es.uniovi.uo237525.dataquality.logmanager

import java.io.File
import java.sql.Timestamp
import java.text.SimpleDateFormat

import es.uniovi.uo237525.dataquality.EXEC_UNIQUE_ID
import es.uniovi.uo237525.dataquality.LogConstants.{Level, LogType}
import es.uniovi.uo237525.dataquality.logmanager.logs.{GenericLog, RuleLog}
import org.json4s.DefaultFormats
import org.json4s.native.Serialization.write
import org.scalatest.{BeforeAndAfterAll, FunSuite}

import scala.util.Try

class LogManagerTest extends FunSuite with BeforeAndAfterAll {

  override def beforeAll(): Unit = cleanLogManagerLogs()

  private def cleanLogManagerLogs(): Unit = LogManager.logs.remove(0, LogManager.logs.length)

  /** TEST ADD LOG */
  test("test addLog") {
    cleanLogManagerLogs()
    assert(LogManager.addLog(null))
    assert(LogManager.logs.size == 1)
    assert(LogManager.logs.head == null)

    val log = GenericLog(Level.INFO, "info", LogType.ALERT)
    assert(LogManager.addLog(log))
    assert(LogManager.logs.tail.head == log)
  }

  /** TEST ADD GENERIC LOG */
  test("test addGenericLog") {
    cleanLogManagerLogs()
    val level = Level.FATAL
    val typ = LogType.CONFIG
    val info = "info"
    assert(LogManager.addGenericLog(level, typ, info))
    assert(LogManager.logs.size == 1)
    assert(LogManager.logs.head.level === level)
    assert(LogManager.logs.head.logType === typ)
    assert(LogManager.logs.head.info === info)

    val level2 = Level.ERROR
    val typ2 = LogType.OUTPUT
    val info2 = "info2"
    assert(LogManager.addGenericLog(level2, typ2, info2))
    assert(LogManager.logs.tail.head.level === level2)
    assert(LogManager.logs.tail.head.logType === typ2)
    assert(LogManager.logs.tail.head.info === info2)
  }

  /** TEST ORDER BY TIMESTAMP LOG */
  test("test orderByTimestamp") {
    cleanLogManagerLogs()
    LogManager.addLog(GenericLog(Level.INFO, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-12-22 00:00:00")))
    LogManager.addLog(GenericLog(Level.INFO, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-10-30 00:00:00")))
    LogManager.addLog(GenericLog(Level.INFO, "inputinfo", LogType.INPUT, Timestamp.valueOf("2005-12-24 00:00:00")))
    LogManager.addLog(GenericLog(Level.INFO, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-3-31 00:00:00")))
    LogManager.addLog(GenericLog(Level.INFO, "inputinfo", LogType.INPUT, Timestamp.valueOf("2016-03-04 00:00:00")))

    val orderedList = LogManager.orderByTimestamp(LogManager.logs.toList)
    var last = orderedList.head.timestamp.getTime;
    orderedList.foreach(l => {
      assert(l.timestamp.getTime >= last)
      last = l.timestamp.getTime;
    })
  }

  /** TEST GET LOGS BY LEVEL */
  test("test getLogsByLevel") {
    cleanLogManagerLogs()
    LogManager.addLog(GenericLog(Level.DEBUG, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-12-22 00:00:00")))
    LogManager.addLog(GenericLog(Level.DEBUG, "inputinfo", LogType.OUTPUT, Timestamp.valueOf("2019-12-22 00:00:00")))
    LogManager.addLog(GenericLog(Level.INFO, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-10-30 00:00:00")))
    LogManager.addLog(GenericLog(Level.INFO, "inputinfo", LogType.CONFIG, Timestamp.valueOf("2005-12-24 00:00:00")))
    LogManager.addLog(GenericLog(Level.WARN, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-3-31 00:00:00")))
    LogManager.addLog(GenericLog(Level.WARN, "inputinfo", LogType.CONFIG, Timestamp.valueOf("2016-03-04 00:00:00")))
    LogManager.addLog(GenericLog(Level.ERROR, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-3-31 00:00:00")))
    LogManager.addLog(GenericLog(Level.ERROR, "inputinfo", LogType.CONFIG, Timestamp.valueOf("2016-03-04 00:00:00")))
    LogManager.addLog(GenericLog(Level.FATAL, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-3-31 00:00:00")))
    LogManager.addLog(GenericLog(Level.FATAL, "inputinfo", LogType.CONFIG, Timestamp.valueOf("2016-03-04 00:00:00")))

    val logs = LogManager.logs.toList
    // debug all included
    assert(LogManager.getLogsByLevel(Level.DEBUG.toString, logs).size == logs.size)
    assert(LogManager.getLogsByLevel(Level.DEBUG, logs).size == logs.size)

    // all but debug
    val numDebugLogs = logs.count(_.level === Level.DEBUG)
    val infoLevelLogs = LogManager.getLogsByLevel(Level.INFO.toString, logs)
    assert(infoLevelLogs.size == logs.size - numDebugLogs)
    assert(infoLevelLogs.count(_.level === Level.DEBUG) == 0)
    assert(LogManager.getLogsByLevel(Level.INFO, logs) === infoLevelLogs)

    // warn all but debug && info
    val numInfoLogs = logs.count(_.level === Level.INFO)
    val warnLevelLogs = LogManager.getLogsByLevel(Level.WARN.toString, logs)
    assert(warnLevelLogs.size == logs.size - (numDebugLogs + numInfoLogs))
    assert(warnLevelLogs.count(_.level === Level.DEBUG) == 0)
    assert(warnLevelLogs.count(_.level === Level.INFO) == 0)

    assert(LogManager.getLogsByLevel(Level.WARN, logs) === warnLevelLogs)

    // error error and fatal
    val numWarnLogs = logs.count(_.level === Level.WARN)
    val errorLevelLogs = LogManager.getLogsByLevel(Level.ERROR.toString, logs)
    assert(errorLevelLogs.size == logs.size - (numDebugLogs + numInfoLogs + numWarnLogs))
    assert(errorLevelLogs.count(_.level === Level.DEBUG) == 0)
    assert(errorLevelLogs.count(_.level === Level.INFO) == 0)
    assert(errorLevelLogs.count(_.level === Level.WARN) == 0)

    assert(LogManager.getLogsByLevel(Level.ERROR, logs) === errorLevelLogs)

    // fatal, only fatal
    // error error and fatal
    val numErrorLogs = logs.count(_.level === Level.ERROR)
    val fatalLevelLogs = LogManager.getLogsByLevel(Level.FATAL.toString, logs)
    assert(fatalLevelLogs.size == logs.size - (numDebugLogs + numInfoLogs + numWarnLogs + numErrorLogs))
    assert(fatalLevelLogs.count(_.level === Level.DEBUG) == 0)
    assert(fatalLevelLogs.count(_.level === Level.INFO) == 0)
    assert(fatalLevelLogs.count(_.level === Level.WARN) == 0)
    assert(fatalLevelLogs.count(_.level === Level.ERROR) == 0)
    assert(fatalLevelLogs.forall(_.level === Level.FATAL))

    assert(LogManager.getLogsByLevel(Level.ERROR, logs) === errorLevelLogs)

    // unknown loglevel string
    val unrecognizedLevelLogs = LogManager.getLogsByLevel("unrecognized", logs)
    assert(unrecognizedLevelLogs.isEmpty)
  }

  /** TEST PRODUCE JSON FROM LOGS */
  test("test PRODUCE JSON FROM LOGS") {
    cleanLogManagerLogs()
    LogManager.addLog(GenericLog(Level.DEBUG, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-12-22 00:00:00")))
    LogManager.addLog(GenericLog(Level.DEBUG, "inputinfo", LogType.OUTPUT, Timestamp.valueOf("2019-12-22 00:00:00")))
    LogManager.addLog(GenericLog(Level.INFO, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-10-30 00:00:00")))
    LogManager.addLog(GenericLog(Level.INFO, "inputinfo", LogType.CONFIG, Timestamp.valueOf("2005-12-24 00:00:00")))
    LogManager.addLog(GenericLog(Level.WARN, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-3-31 00:00:00")))
    LogManager.addLog(GenericLog(Level.WARN, "inputinfo", LogType.CONFIG, Timestamp.valueOf("2016-03-04 00:00:00")))
    LogManager.addLog(GenericLog(Level.ERROR, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-3-31 00:00:00")))
    LogManager.addLog(GenericLog(Level.ERROR, "inputinfo", LogType.CONFIG, Timestamp.valueOf("2016-03-04 00:00:00")))
    LogManager.addLog(GenericLog(Level.FATAL, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-3-31 00:00:00")))
    LogManager.addLog(GenericLog(Level.FATAL, "inputinfo", LogType.CONFIG, Timestamp.valueOf("2016-03-04 00:00:00")))

    val json = LogManager.produceJsonFromLogs(LogManager.logs.toList)
    implicit val formats: DefaultFormats = DefaultFormats

    import org.json4s.native.JsonMethods._
    assert(Try(parse(json)).isSuccess)

    val jsonFromNull = LogManager.produceJsonFromLogs(null)
    assert(jsonFromNull === "null")
  }

  /** TEST PRODUCE FILE */
  test("test PRODUCE REPORT file") {
    cleanLogManagerLogs()
    LogManager.addLog(GenericLog(Level.DEBUG, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-12-22 00:00:00")))
    LogManager.addLog(GenericLog(Level.DEBUG, "inputinfo", LogType.OUTPUT, Timestamp.valueOf("2019-12-22 00:00:00")))
    LogManager.addLog(GenericLog(Level.INFO, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-10-30 00:00:00")))
    LogManager.addLog(GenericLog(Level.INFO, "inputinfo", LogType.CONFIG, Timestamp.valueOf("2005-12-24 00:00:00")))
    LogManager.addLog(GenericLog(Level.WARN, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-3-31 00:00:00")))
    LogManager.addLog(GenericLog(Level.WARN, "inputinfo", LogType.CONFIG, Timestamp.valueOf("2016-03-04 00:00:00")))
    LogManager.addLog(GenericLog(Level.ERROR, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-3-31 00:00:00")))
    LogManager.addLog(GenericLog(Level.ERROR, "inputinfo", LogType.CONFIG, Timestamp.valueOf("2016-03-04 00:00:00")))
    LogManager.addLog(GenericLog(Level.FATAL, "inputinfo", LogType.INPUT, Timestamp.valueOf("2019-3-31 00:00:00")))
    LogManager.addLog(GenericLog(Level.FATAL, "inputinfo", LogType.CONFIG, Timestamp.valueOf("2016-03-04 00:00:00")))
    LogManager.addLog(RuleLog("id", Level.INFO, "RuleSignature", "field", 10, 20, "info"))
    LogManager.addLog(RuleLog("id2", Level.ERROR, "RuleSignature", "field", 10, 20, "rulefailed"))
    val file = new File(s"./$EXEC_UNIQUE_ID.json")
    assert(!file.exists())
    LogManager.produceJsonReport(file)
    assert(file.exists())
    Try(LogManager.produceJsonReport(file)).isSuccess
    file.deleteOnExit()
  }

}
