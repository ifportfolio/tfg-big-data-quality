# Rules Syntax Guide

This guide provides all the needed information to the available rules. 

##Definition File
All the rules can be defined following the next schema:

- **name\* (*String*):** Serves as a rule identifier for the user. By default it is the rule type.    
- **type\* (*String*):** The rule itself that the user wants to apply
- **field\* (*String*):** Over which the rule is going to be applied.
- **alert (*String*):** Custom alert message associated to the rule. It is undefined by default.
- **options\* (_Complex_)**: Specific information needed for the rule.

_Required fields marked with '*'._
##### _Example:_
```json
{
  "name": "name-id",
  "type": "RemRequiredFields",
  "field": "field",
  "alert": "custom-alert",
  "options": {
    "defaultValue": "defValue"
  }
}
```
## Evaluation Rules

#### DateFormat: 

Checks whether the values of the field comply with the provided date pattern.
The pattern must be parsable by [java.time.format.DateTimeFormatter](https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html).
##### _Structure:_
- **type:** _EvDateFormat_
- **options:** 
    - dateFormat (*String*)
##### _Example:_
```json
{
  "name": "es_date_format",
  "type": "EvDateFormat",
  "field": "field",
  "alert": "custom-alert",
  "options": {
    "dateFormat": "dd/MM/yyyy"
  }
}
```

#### RequiredFields: 

Checks for null values inside the field.
##### _Structure:_
- **type:** _EvRequiredFields_
- **options:** _None_

##### _Example:_
```json
{
  "name": "field-nulls-eval",
  "type": "EvRequiredFields",
  "field": "field",
  "alert": "custom-alert",
  "options": {}
}
```

## Remediation Rules

#### RequiredFields: 
Substitutes all the null values by a default value.

This rule can only be applied on types supported by [Spark na.fill](https://spark.apache.org/docs/2.4.0/api/java/org/apache/spark/sql/DataFrameNaFunctions.html#fill-java.util.Map-).

##### _Structure:_
- **type:** _EvRequiredFields_
- **options:** 
    - defaultValue (*String*)
##### _Example:_
```json
{
  "name": "numeric-field-replacement",
  "type": "RemRequiredFields",
  "field": "numfield",
  "alert": "custom-alert",
  "options": {
    "defaultValue": 0.99
  }
}
```