#!/bin/bash

source ./common.sh

function installLocalJava {
	echo "installing oracle jdk"
	FILE=$RESOURCES_DIR/$JAVA_ARCHIVE
	tar -xzf $FILE -C /usr/local
}

function installRemoteJava {
	echo "install open jdk"
	wget -c --header "Cookie: oraclelicense=accept-securebackup-cookie" -P $RESOURCES_DIR http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.tar.gz 
	FILE=$RESOURCES_DIR/$JAVA_ARCHIVE
	tar -xzf $FILE -C /usr/local
}

function setupJava {
	echo "setting up java"
	#if resourceExists $JAVA_ARCHIVE; then
	ln -s /usr/local/jdk1.8.0_131 /usr/local/java
	#else
		#ln -s /usr/lib/jvm/jre /usr/local/java
	#fi
}

function setupEnvVars {
	echo "creating java environment variables"
	echo export JAVA_HOME=/usr/local/java >> /etc/profile.d/java.sh
	echo export PATH=\${JAVA_HOME}/bin:\${PATH} >> /etc/profile.d/java.sh
}

function installJava {
	if resourceExists $JAVA_ARCHIVE; then
		installLocalJava
	else
		installRemoteJava
	fi
}

echo "setup java"
installJava
setupJava
setupEnvVars
