package es.uniovi.uo237525.dataquality.reader.sources

import es.uniovi.uo237525.dataquality.parser.models.InputParams

import org.apache.spark.sql.DataFrame

/**
  * Trait for the common behaviour of different data sources
  */
trait Reader {

  val inputParams: InputParams

  /**
    * Creates the dataframe from the source
    *
    * @return the resulting dataframe
    */
  def createDF: DataFrame
}
