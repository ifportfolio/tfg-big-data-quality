package es.uniovi.uo237525.dataquality.engine.rules.remediation

import java.util.UUID

import es.uniovi.uo237525.dataquality.LogConstants.Level
import es.uniovi.uo237525.dataquality.engine.RemediationInfo
import es.uniovi.uo237525.dataquality.engine.rules.{RemediationRule, RuleOptions}
import scala.util.Try

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{ArrayType, MapType}

case class SliceFieldOptions(from: Int = 0, length: Int) extends RuleOptions

/**
  * This remediation rule truncates the field until the max length allowed.
  * In case that it is applied in an Array field, it must be taken into account that the index starts in 1.
  *
  * @param id      Identifier for the rule. Automatically generated if not provided
  * @param field   Field in which the rule is going to be applied
  * @param options RemRequiredFieldsOptions needed for the rule application.
  */
case class SliceField(id: String = UUID.randomUUID().toString,
                      field: String,
                      options: SliceFieldOptions) extends RemediationRule {

  private val from = options.from
  private val length = options.length

  /**
    * Checks the pre-conditions that need to be fulfilled in order that the remediation can be applied.
    *
    * Pre-conditions:
    *   - The existence of the column in the dataframe
    *   - The from index provided is >= 0
    *   - THe length provided is >= 0
    *
    * @param df the dataframe where the remediation is going to be applied
    * @return a verbose description of the reason why a precondition is not fulfilled,
    *         None if no pre-condition is broken,
    */
  override def checkPreconditions(df: DataFrame): Option[String] = {
    // checking that the fields exists
    val res = Try(df(field)).isSuccess
    if (!res) {
      return Some(s"Field $field does not exists.")
    }

    if (from < 0) return Some("'from' option must be greater or equal than '0'")

    if (length < 0) Some("'length' option must be greater or equal than 'from'")
    else None
  }

  /**
    * It uses the slice function provided by Spark if it is an Array or a Map, else it will use the substring Spark provided function.
    * It ignores null records.
    * It modifies the record type to string except Arrays
    * The matched field of the RemediationInfo returns -1, it means that is not calculated because performance reasons.
    *
    * @param df the base dataframe where the rule is going to be applied
    * @return the rule log if the rule was successfully applied, an exception otherwise
    */
  override def apply(df: DataFrame): Try[RemediationInfo] = Try {
    //    val modifiedDf = df.withColumn(field, slice(col(field), from, to))
    val columnType = df.select(field).schema.head.dataType

    val modifiedDf = columnType match {
      case ArrayType(_, _) => df.withColumn(field, slice(col(field), from, length))
      case MapType(_, _, _) => df.withColumn(field, slice(col(field), from, length))
      case _ => df.withColumn(field, substring(col(field), from, length))
    }

    val total = modifiedDf.count()

    RemediationInfo(Level.INFO,
      s"""Truncated all possible values from index $from to $length.""",
      -1,
      total,
      modifiedDf)
  }
}
