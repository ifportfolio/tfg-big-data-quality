package es.uniovi.uo237525.dataquality.engine.rules.remediation

import java.sql.{Date, Timestamp}
import java.time.Instant

import es.uniovi.uo237525.dataquality.SparkSessionTestWrapper
import org.scalatest.FunSuite
import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

import scala.util.Try

class SliceFieldTest extends FunSuite with SparkSessionTestWrapper {

  private val ts = Timestamp.from(Instant.now())
  private val date = new Date(System.currentTimeMillis())
  private val map = Map("example1" -> "ex1", "example2" -> "ex2")
  private val mulArray = Array(Array("array of array1"), Array("array of array2"))
  private val seq = Seq(Row(1, Long.MaxValue, null, false, 0.5, ts, Array("val1", "val2"), mulArray, date, map),
    Row(22, Long.MaxValue, "value", null, null, ts, Array("val1", "val2"), mulArray, null, map),
    Row(3345, null, "", true, 0.004, ts, null, null, date, null),
    Row(4, 3434343.toLong, "", true, 0.00344, ts, Array(), null, date, null),
    Row(null, null, "", null, null, null, null, null, null, null))

  val someSchema = StructType(
    StructField("int", IntegerType) ::
      StructField("long", LongType) ::
      StructField("string", StringType) ::
      StructField("boolean", BooleanType) ::
      StructField("double", DoubleType) ::
      StructField("timestamp", TimestampType) ::
      StructField("array", ArrayType(StringType)) ::
      StructField("multiple", ArrayType(ArrayType(StringType))) ::
      StructField("date", DateType) ::
      StructField("map", MapType(StringType, StringType)) :: Nil)

  val someDf: DataFrame = spark.createDataFrame(
    spark.sparkContext.parallelize(seq),
    StructType(someSchema))

  /** REMEDIATION PRE-CONDITIONS TEST */
  test("test CHECK PRE-CONDITIONS: application on un-existing field") {
    val opts = SliceFieldOptions(0, 1)
    val rule = SliceField(field = "non-existing", options = opts)
    assert(rule.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: application on existing field") {
    val opts = SliceFieldOptions(0, 1)
    val rule = SliceField(field = someDf.schema.fields.head.name, options = opts)
    assert(rule.checkPreconditions(someDf).isEmpty)
  }

  test("test QUALITY PRE-CONDITIONS: unspecifying from defaults to 0") {
    val opts = SliceFieldOptions(length = 1)
    val rule = SliceField(field = someDf.schema.fields.head.name, options = opts)
    assert(opts.from == 0)
    assert(rule.checkPreconditions(someDf).isEmpty)
  }

  test("test QUALITY PRE-CONDITIONS: NEGATIVE from OPTION") {
    val opts = SliceFieldOptions(-1, 1)
    val rule = SliceField(field = someDf.schema.fields.head.name, options = opts)
    assert(rule.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: NEGATIVE length OPTION") {
    val opts = SliceFieldOptions(length = -1)
    val rule = SliceField(field = someDf.schema.fields.head.name, options = opts)
    assert(rule.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: from greater than to TO OPTION") {
    val opts = SliceFieldOptions(10, 5)
    val rule = SliceField(field = someDf.schema.fields.head.name, options = opts)
    assert(rule.checkPreconditions(someDf).isEmpty)
  }

  /** REMEDIATION APPLICATION TEST */
  /** TEST APPLICATION OF THE RULE */

  test("test QUALITY APPLY: NUMERIC fields") {
    // int
    val intF = "int"
    val ruleInt = SliceField(field = intF, options = SliceFieldOptions(0, 1))
    val matchedInt = ruleInt.apply(someDf).get
    matchedInt.modifiedDF.select(intF).filter(col(intF).isNotNull).collect()
      .foreach(r => assert(r.getString(0).length <= ruleInt.options.length))

    //     long
    val longF = "long"
    val ruleLong = SliceField(field = longF, options = SliceFieldOptions(1, 3))
    val matchedLong = ruleLong.apply(someDf).get
    matchedLong.modifiedDF.select(longF).filter(col(longF).isNotNull).collect()
      .foreach(r => assert(r.getString(0).length <= ruleLong.options.length))

    // decimal / double
    val doubleF = "double"
    val ruleDouble = SliceField(field = doubleF, options = SliceFieldOptions(1, 3))
    val matchedDouble = ruleDouble.apply(someDf).get
    matchedDouble.modifiedDF.select(doubleF).filter(col(doubleF).isNotNull).collect().foreach(r =>
      assert(r.getString(0).length <= ruleDouble.options.length))
  }

  test("test QUALITY APPLY: STRING && BOOLEAN && TIMESTAMP fields") {
    // str
    val strF = "string"
    val ruleStr = SliceField(field = strF, options = SliceFieldOptions(1, 2))
    val matchedStr = ruleStr.apply(someDf).get
    matchedStr.modifiedDF.select(strF).filter(col(strF).isNotNull).collect().foreach(r => {
      assert(r.getString(0).length <= ruleStr.options.length)
    })

    // bool
    val boolF = "boolean"
    val ruleBool = SliceField(field = boolF, options = SliceFieldOptions(1, 3))
    val matchedBool = ruleBool.apply(someDf).get
    matchedBool.modifiedDF.select(boolF).filter(col(boolF).isNotNull).collect()
      .foreach(r => assert(r.getString(0).length <= ruleBool.options.length))

    // timestamp
    val tsF = "timestamp"
    val ruleTs = SliceField(field = tsF, options = SliceFieldOptions(1, 3))
    val matchedTs = ruleTs.apply(someDf).get
    matchedTs.modifiedDF.select(tsF).filter(col(tsF).isNotNull).collect()
      .foreach(r => assert(r.getString(0).length <= ruleTs.options.length))
  }

  test("test QUALITY APPLY: ARRAY && MAP fields") {
    val arrayF = "array"
    // array from index 0 not allowed
    val ruleArray0 = SliceField(field = arrayF, options = SliceFieldOptions(length = 1))
    assert(Try(ruleArray0.apply(someDf).get.modifiedDF.collect()).isFailure)
    // array
    val ruleArray = SliceField(field = arrayF, options = SliceFieldOptions(1, 1))
    val matchedArray = ruleArray.apply(someDf).get
    matchedArray.modifiedDF.select(arrayF).filter(col(arrayF).isNotNull).collect()
      .foreach(r => assert(r.getList(0).size() <= ruleArray.options.length))

    // multiple array
    val mulArrayF = "multiple"
    val ruleMulArray = SliceField(field = mulArrayF, options = SliceFieldOptions(1, 1))
    val matchedMulArray = ruleMulArray.apply(someDf).get
    matchedMulArray.modifiedDF.select(mulArrayF).filter(col(mulArrayF).isNotNull).collect()
      .foreach(r => assert(r.getList(0).size() <= ruleMulArray.options.length))

    // map not allowed
    val mapF = "map"
    val ruleMap = SliceField(field = mapF, options = SliceFieldOptions(1, 1))
    assert(ruleMap.apply(someDf).isFailure)
  }


}
