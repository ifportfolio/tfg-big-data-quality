package es.uniovi.uo237525.dataquality.engine.rules.remediation

import java.sql.{Date, Timestamp}
import java.time.Instant

import es.uniovi.uo237525.dataquality.SparkSessionTestWrapper
import es.uniovi.uo237525.dataquality.engine.rules.evaluation.{EvRangeValues, EvRangeValuesOptions}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Row}
import org.scalatest.FunSuite

class ReplaceOutOfRangeTest extends FunSuite with SparkSessionTestWrapper {

  import spark.implicits._

  private val ts = Timestamp.from(Instant.now())
  private val date = new Date(System.currentTimeMillis())
  private val map = Map("example1" -> "ex1", "example2" -> "ex2")
  private val mulArray = Array(Array("array of array1"), Array("array of array2"))
  private val seq = Seq(Row(1, Long.MaxValue, null, false, 0.5, ts, Array("val1", "val2"), mulArray, date, map),
    Row(22, Long.MaxValue, "value", null, null, ts, Array("val1", "val2"), mulArray, null, map),
    Row(3345, null, "", true, 0.004, ts, null, null, date, null),
    Row(4, 3434343.toLong, "", true, 0.00344, ts, Array(), null, date, null),
    Row(null, null, "", null, null, null, null, null, null, null))

  val someSchema = StructType(
    StructField("int", IntegerType) ::
      StructField("long", LongType) ::
      StructField("string", StringType) ::
      StructField("boolean", BooleanType) ::
      StructField("double", DoubleType) ::
      StructField("timestamp", TimestampType) ::
      StructField("array", ArrayType(StringType)) ::
      StructField("multiple", ArrayType(ArrayType(StringType))) ::
      StructField("date", DateType) ::
      StructField("map", MapType(StringType, StringType)) :: Nil)

  val someDf: DataFrame = spark.createDataFrame(
    spark.sparkContext.parallelize(seq),
    StructType(someSchema))

  val colorsDf = Seq("red", "blue", "yellow", "Blue", "rEd", "YELLOW", "car", "25", "scala").toDF("colors")


  /** REMEDIATION PRE-CONDITIONS TEST */
  /** TEST CHECK PRECONDITIONS */
  test("test CHECK PRE-CONDITIONS: application on un-existing field") {
    val opts = ReplaceOutOfRangeOptions("default", Array("value"))
    val rule = ReplaceOutOfRange(field = "non-existing", options = opts)
    assert(rule.checkPreconditions(colorsDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: application on existing field") {
    val opts = ReplaceOutOfRangeOptions("default", Array("value"))
    val rule = ReplaceOutOfRange(field = colorsDf.schema.fields.head.name, options = opts)
    assert(rule.checkPreconditions(colorsDf).isEmpty)
  }

  test("test QUALITY PRE-CONDITIONS: empty list not accepted") {
    val opts = ReplaceOutOfRangeOptions("default", Array())
    val rule = ReplaceOutOfRange(field = colorsDf.schema.fields.head.name, options = opts)
    assert(rule.checkPreconditions(colorsDf).isDefined)
  }


  test("test QUALITY PRE-CONDITIONS: check preconditions in allowed field type (StringType)") {
    val opts = ReplaceOutOfRangeOptions("default", Array(""))
    val rule = ReplaceOutOfRange(field = colorsDf.schema.fields.head.name, options = opts)
    assert(rule.checkPreconditions(colorsDf).isEmpty)
  }

  test("test QUALITY PRE-CONDITIONS: numeric fields") {
    // int
    val intF = "int"
    val ruleInt = ReplaceOutOfRange(field = intF, options = ReplaceOutOfRangeOptions("default", Array("")))
    assert(ruleInt.checkPreconditions(someDf).isDefined)
    // long
    val longF = "long"
    val ruleLong = ReplaceOutOfRange(field = longF, options = ReplaceOutOfRangeOptions("default", Array("")))
    assert(ruleLong.checkPreconditions(someDf).isDefined)
    // decimal / double
    val doubleF = "double"
    val ruleDouble = ReplaceOutOfRange(field = doubleF, options = ReplaceOutOfRangeOptions("default", Array("")))
    assert(ruleDouble.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: BOOLEAN && TIMESTAMP fields") {

    // bool
    val boolF = "boolean"
    val ruleBool = ReplaceOutOfRange(field = boolF, options = ReplaceOutOfRangeOptions("default", Array("")))
    assert(ruleBool.checkPreconditions(someDf).isDefined)
    // timestamp
    val tsF = "timestamp"
    val ruleTs = ReplaceOutOfRange(field = tsF, options = ReplaceOutOfRangeOptions("default", Array("")))
    assert(ruleTs.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: ARRAY && MAP field types") {
    val rule = ReplaceOutOfRange(field = "array", options = ReplaceOutOfRangeOptions("default", Array("")))
    assert(rule.checkPreconditions(someDf).isDefined)

    val rule2 = ReplaceOutOfRange(field = "multiple", options = ReplaceOutOfRangeOptions("default", Array("")))
    assert(rule2.checkPreconditions(someDf).isDefined)

    val rule3 = ReplaceOutOfRange(field = "map", options = ReplaceOutOfRangeOptions("default", Array("")))
    assert(rule3.checkPreconditions(someDf).isDefined)
  }


  /** TEST APPLICATION OF THE RULE */
  test("test QUALITY APPLY: TEST CASE SENSITIVE") {
    val field = colorsDf.schema.fields.head.name
    val rule = ReplaceOutOfRange(field = field,
      options = ReplaceOutOfRangeOptions("blue",
        Array("blue", "red", "yellow"),
        caseSensitive = true))
    val res = rule.apply(colorsDf).get
    val elementsNotToBeReplaced = colorsDf.collect().map(_.getString(0)).count(value =>
      value == "blue" || value == "red" || value == "yellow")
    assert(elementsNotToBeReplaced + res.matched == colorsDf.filter(col(field).isNotNull).count())
  }

  test("test QUALITY APPLY: TEST CASE INSENSITIVE") {
    val field = colorsDf.schema.fields.head.name
    val rule = ReplaceOutOfRange(field = field,
      options = ReplaceOutOfRangeOptions("blue",
        Array("blue", "red", "yellow")))
    val res = rule.apply(colorsDf).get
    val elementsNotToBeReplaced = colorsDf.collect().map(_.getString(0)).count(value =>
      value.toLowerCase == "blue" || value.toLowerCase == "red" || value.toLowerCase == "yellow")
    assert(elementsNotToBeReplaced + res.matched == colorsDf.filter(col(field).isNotNull).count())
  }
}
