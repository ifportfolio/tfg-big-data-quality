package es.uniovi.uo237525.dataquality.engine

import es.uniovi.uo237525.dataquality.LogConstants.Level.Level

import org.apache.spark.sql.DataFrame

/**
  * Model that represents the information that needs to be compiled on a rule application
  */
trait RuleInfo {
  val level: Level
  val info: String
  val matched: Long
  val total: Long
}

/**
  * Represents the information about the execution of an evaluation rule.
  *
  * @param level   State of the rule application
  * @param info    Info message about the rule application
  * @param matched the number of rows in which the rule was effective
  * @param total   the total number of rows in the field.
  */
case class EvaluationInfo(level: Level,
                          info: String,
                          matched: Long,
                          total: Long) extends RuleInfo {}

/**
  * Represents the information about the execution of a remediation rule.
  *
  * @param info       Info message about the rule application
  * @param modifiedDF the resulting data frame after applying the remediation rule.
  */
case class RemediationInfo(level: Level,
                           info: String,
                           matched: Long,
                           total: Long,
                           modifiedDF: DataFrame) extends RuleInfo {}