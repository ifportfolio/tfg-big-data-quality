#!/bin/bash

RESOURCES_DIR=$(cd `dirname $0` && pwd)/resources
SCRIPTS_DIR=$(cd `dirname $0` && pwd)

# java
JAVA_ARCHIVE=jdk-8u131-linux-x64.tar.gz

# hadoop
HADOOP_PREFIX=/usr/local/hadoop
HADOOP_CONF=$HADOOP_PREFIX/etc/hadoop
HADOOP_VERSION=hadoop-2.7.0
HADOOP_ARCHIVE=$HADOOP_VERSION.tar.gz
HADOOP_MIRROR_DOWNLOAD=http://archive.apache.org/dist/hadoop/core/$HADOOP_VERSION/$HADOOP_ARCHIVE
HADOOP_RES_DIR=$RESOURCES_DIR/hadoop

# hive
HIVE_VERSION=hive-1.1.0
HIVE_ARCHIVE=apache-$HIVE_VERSION-bin.tar.gz
HIVE_MIRROR_DOWNLOAD=http://archive.apache.org/dist/hive/$HIVE_VERSION/$HIVE_ARCHIVE
HIVE_RES_DIR=$RESOURCES_DIR/hive
HIVE_CONF=/usr/local/hive/conf

# spark
SPARK_VERSION=spark-2.4.0
SPARK_ARCHIVE=$SPARK_VERSION-bin-hadoop2.tgz
SPARK_MIRROR_DOWNLOAD=http://archive.apache.org/dist/spark/$SPARK_VERSION/$SPARK_VERSION-bin-hadoop2.6.tgz
SPARK_RES_DIR=$RESOURCES_DIR/spark
SPARK_CONF_DIR=/usr/local/spark/conf

# kafka
KAFKA_VERSION=2.1.0
KAFKA_SCALA=2.11
KAFKA_ARCHIVE=kafka_$KAFKA_SCALA-$KAFKA_VERSION.tgz
KAFKA_RES_DIR=$RESOURCES_DIR/kafka
KAFKA_CONF=/usr/local/kafka/config
KAFKA_MIRROR_DOWNLOAD=http://archive.apache.org/dist/kafka/$KAFKA_VERSION/$KAFKA_ARCHIVE


# ssh
SSH_RES_DIR=$RESOURCES_DIR/ssh
RES_SSH_COPYID_ORIGINAL=$SSH_RES_DIR/ssh-copy-id.original
RES_SSH_COPYID_MODIFIED=$SSH_RES_DIR/ssh-copy-id.modified
RES_SSH_CONFIG=$SSH_RES_DIR/config

function resourceExists {
	FILE=$RESOURCES_DIR/$1
	if [ -e $FILE ]
	then
		return 0
	else
		return 1
	fi
}

function fileExists {
	FILE=$1
	if [ -e $FILE ]
	then
		return 0
	else
		return 1
	fi
}
