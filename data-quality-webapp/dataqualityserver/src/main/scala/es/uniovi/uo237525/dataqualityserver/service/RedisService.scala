package es.uniovi.uo237525.dataqualityserver.service

import com.redis.RedisClient
import org.apache.logging.log4j.LogManager
import org.springframework.stereotype.Service

import scala.util.{Failure, Success, Try}

/**
  * Provides functionality for retrieving data from Redis.
  */
@Service
class RedisService {

  private val logger = LogManager.getRootLogger

  /**
    * Performs an LINDEX KEY 0 operation that retrieves the last inserted element from a Redis list.
    *
    * @param key  The key for the Redis list
    * @param host The host for the Redis server
    * @param port the port for the Redis server
    * @return An option containing the element, or None if does not exists or the connection with Redis could be done.
    */
  def getLastMessageFromList(key: String, host: String, port: Int): Option[String] = {
    logger.info(s"Retrieving last inserted element from $key")
    Try(new RedisClient(host, port.toInt)) match {
      case Success(client) => client.lindex[String](key, 0)
      case Failure(exception) => logger.error(exception.toString); None
    }

  }

  /**
    * Performs an GET KEY operation that retrieves the associated value for an specific key.
    *
    * @param key  The key for the Redis list
    * @param host The host for the Redis server
    * @param port the port for the Redis server
    * @return An option containing the element, or None if does not exists or the connection with Redis could be done.
    */
  def getValueFromKey(key: String, host: String, port: Int): Option[String] = {
    logger.info(s"Retrieving value for element -> $key")
    Try(new RedisClient(host, port.toInt)) match {
      case Success(client) => client.get(key)
      case Failure(exception) => logger.error(exception.toString); None
    }
  }

}
