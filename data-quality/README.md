## data-quality

A Scala component that analyzes the data based on some quality rules.

## What should I do now?

You can just build the package

    mvn clean package

But if you prefer you can also check if code has a proper style

    mvn clean verify
	
Or maybe you should run the tests

    mvn test

Once you get the code compiled, inside the ```/target``` folder you will find the result fat jar called ```data-quality-0.1.0-jar-with-depencencies.jar```. In order to launch the Spark job use this command in a shell with a configured Spark environment:

    spark-submit \
      --master yarn-cluster \
      data-quality-0.1.0-jar-with-depencencies.jar \
      <your params>

## What are the command line parameters?

| Parameter | short | Description | Type | Required | 
| --------- | ----- | ----------- | ---- | -------- |
| --rules   | -r    | Path to rules file | String | true | 
