package es.uniovi.uo237525.dataqualityserver.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

/**
  * Controller that receives any non-managed request, redirecting it to serve the index.html.
  */
@Controller
class ViewController {

  /**
    * Redirects base requests to index.html
    * @return redirection to index.html
    */
  @RequestMapping(value = Array("/"))
  def redirectToIndex:String = "forward:/index.html"

  /**
    * Redirects to base controller.
    * @return redirection to "/"
    */
  @RequestMapping(value = Array("/**/{path:[^\\.]*}"))
  def redirect = "forward:/"
}
