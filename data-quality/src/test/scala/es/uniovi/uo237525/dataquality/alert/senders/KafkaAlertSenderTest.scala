//package es.uniovi.uo237525.dataquality.alert.senders
//
//import es.uniovi.uo237525.dataquality.LogConstants.{Level, LogType}
//import es.uniovi.uo237525.dataquality.LogProperties._
//import es.uniovi.uo237525.dataquality.configuration.Configuration
//import es.uniovi.uo237525.dataquality.logmanager.Log
//import es.uniovi.uo237525.dataquality.logmanager.logs.GenericLog
//import es.uniovi.uo237525.dataquality.parser.models.KafkaAlert
//import org.apache.kafka.clients.consumer.MockConsumer
//import org.apache.kafka.clients.producer.MockProducer
//import org.scalatest.{BeforeAndAfterAll, FunSuite}
//import org.springframework.kafka.test.rule.KafkaEmbedded
//
//class KafkaAlertSenderTest extends FunSuite with BeforeAndAfterAll {
//
//  private val logs: List[Log] = LogType.values.map(t => GenericLog(Level.INFO, "info", t)).toList
//
//  val kafkaPort = 5001
//  //  val kafkaUnitServer = new KafkaUnit("localhost:5000", s"localhost:$kafkaPort")
//  val embeddedKafka = new KafkaEmbedded(2, true, 2, "messages");
//  val kafkaPort = embeddedKafka.setKafkaPorts()
//
//  private val configSender = Configuration(
//    Array("-r", "", "-Dkafka.bootstrap.servers=localhost", s"-Dkafka.port=$kafkaPort"))
//
//
//  override def beforeAll(): Unit = embeddedKafka.getKafkaServer(0).config.
//  override def afterAll(): Unit = kafkaUnitServer.shutdown
//
//  /** CREATE HTML CONTENT */
//
//  private def getSender(al: KafkaAlert): KafkaAlertSender = KafkaAlertSender(configSender, al)
//
//  /** TEST SEND ALERTS WITH CORRECT HOST PARAMS */
//  test("message is sent") {
//    val al = KafkaAlert(enabled = true, "DEBUG", "topic")
//    val kafkaSender = getSender(al)
//    kafkaSender.sendAlert(logs).get
//    val messages = kafkaUnitServer.readMessages(al.topic, 1)
//    messages
//  }
//  //
//  //  test("no logs") {
//  //    val mailSender = getSender(Seq("-DSMTP_HOST=localhost", "-DSMTP_PORT=25"), MailAlert(true, Level.DEBUG.toString, "mailto", None, None, "subject"))
//  //    assert(mailSender.sendAlert(List()).isSuccess)
//  //    val list = Mailbox.get("mailto")
//  //    list
//  //  }
//  //
//  //  test("null logs") {
//  //    val mailSender = getSender(Seq("-DSMTP_HOST=localhost", "-DSMTP_PORT=25"), MailAlert(true, Level.DEBUG.toString, "mailto", None, None, "subject"))
//  //    assert(mailSender.sendAlert(null).isFailure)
//  //  }
//  //
//  //  test("invalid smtp options") {
//  //    val mailSender = getSender(Seq("-DSMTP_HOST=localhost", "-DSMTP_PORT=19"), MailAlert(true, Level.DEBUG.toString, "mailto", None, None, "subject"))
//  //    assert(mailSender.sendAlert(null).isFailure)
//  //  }
//}
