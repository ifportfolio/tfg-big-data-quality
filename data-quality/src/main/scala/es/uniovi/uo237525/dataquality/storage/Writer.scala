package es.uniovi.uo237525.dataquality.storage

import scala.util.Try

import org.apache.spark.sql.DataFrame

/**
  * Trait that specifies the schema to follow by all the different writers
  */
trait Writer {

  /**
    * Opens the connections and returns the connection instance
    *
    * @return An instance representing the connection
    */
  def open(): Try[Any]

  /**
    * Substitutes the content of the previous data with the content of the dataframe if the schema is compatible
    *
    * @param df         the dataframe that is going to be written.
    * @param connection an object that provides the connection to the source.
    */
  def overwrite(df: DataFrame, connection: Any): Try[Unit]

  /**
    * Appends the content of the  the resulting data-frame onto the specific format.
    *
    * @param df         the dataframe that is going to be written.
    * @param connection an object that provides the connection to the source.
    */
  def append(df: DataFrame, connection: Any): Try[Unit]

  /**
    * TODO
    *
    * @param df         the dataframe that is going to be written.
    * @param connection an object that provides the connection to the source.
    */
  def errorIfExists(df: DataFrame, connection: Any): Try[Unit]

  /**
    * TODO
    *
    * @param df         the dataframe that is going to be written.
    * @param connection an object that provides the connection to the source.
    */
  def ignore(df: DataFrame, connection: Any): Try[Unit]
}
