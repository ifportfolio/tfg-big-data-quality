package es.uniovi.uo237525.dataquality.utils

/**
  * Generic Utils class
  */
object Utils {

  def escapeSpecialRegexChars(str: String): String = {
    val specialRegexChars =
      """\^$.|?*+()[{""".toCharArray
    str.map(c => {
      if (specialRegexChars.contains(c)) {
        val char = """\""" + c
        char
      } else c
    }).mkString
  }

}
