package es.uniovi.uo237525.dataquality

import java.io.File

import es.uniovi.uo237525.dataquality.LogConstants.{Level, LogType}
import es.uniovi.uo237525.dataquality.alert.AlertEngine
import es.uniovi.uo237525.dataquality.configuration.Configuration
import es.uniovi.uo237525.dataquality.engine.RulesEngine
import es.uniovi.uo237525.dataquality.logmanager.LogManager
import es.uniovi.uo237525.dataquality.parser.JsonParser
import es.uniovi.uo237525.dataquality.reader.ReaderEngine
import es.uniovi.uo237525.dataquality.storage.WriterEngine
import org.apache.log4j.Logger

import scala.util.{Failure, Success, Try}

/**
  * Entry point of the program
  */
object App {

  val logger = Logger.getRootLogger

  /**
    * Main method
    *
    * @param args input program arguments
    */
  def main(args: Array[String]): Unit = {
    logger.warn("Starting data-quality-0.1.0")
    logger.warn(s"EXECUTION UNIQUE IDENTIFIER : $EXEC_UNIQUE_ID")
    LogManager.addGenericLog(Level.INFO, LogType.CONFIG, s"EXECUTION UNIQUE IDENTIFIER : $EXEC_UNIQUE_ID")
    logger.warn("Creating Configuration")
    val conf = Configuration(args)

    logger.warn("Loading rules from JSON")
    val wrapper = JsonParser.extractJsonSchema(conf.getRulesPath)
    val input = wrapper.input
    val output = wrapper.output
    val rules = wrapper.rules
    val alerts = wrapper.alerts

    // read quality write
    val execution = Try({
      logger.warn("Reading source data")
      val dataAq = new ReaderEngine(input)
      val df = dataAq.read()
      df.printSchema()

      logger.warn("Executing rules engine")
      val rEngine = new RulesEngine(df, rules)
      val resultingDf = rEngine.execute()

      logger.warn("Storing the resulting dataframe")
      val writerEngine = new WriterEngine()
      val dataWriter = writerEngine.createWriterFromOutParams(output)
      if (dataWriter.isDefined) {
        writerEngine.write(resultingDf, dataWriter.get, wrapper.output.get.mode.get, wrapper.output.get.repartition)
      }
      (conf, alerts)
    })

    logger.warn("Trying to send the alerts")
    val alertEngine = new AlertEngine(conf)
    alertEngine.sendAllAlerts(LogManager.logs, alerts)

    logger.warn("Writing the report file")
    val reportPath = conf.getOptionalStringProperty(ConfigFileConstants.REPORT_EXPORT_DIR).getOrElse(".")
    val reportFile = new File(s"$reportPath/$EXEC_UNIQUE_ID.json").getCanonicalFile
    logger.warn(s"Report File Path -> ${reportFile.getCanonicalPath}")
    LogManager.addGenericLog(Level.INFO, LogType.CONFIG, s"Report File Path -> ${reportFile.getCanonicalPath}")
    Try(LogManager.produceJsonReport(reportFile)) match {
      case Success(_) => logger.warn("Report successfully written.")
      case Failure(exception) => logger.error(s"Report could not be written -> ${exception.getMessage}")
    }

    execution match {
      case Success(_) =>
        logger.warn("Execution ended successfully")
        System.exit(0)
      case Failure(exception) =>
        logger.fatal(exception.toString)
        logger.fatal("Execution ended")
        System.exit(1)
    }
  }

}
