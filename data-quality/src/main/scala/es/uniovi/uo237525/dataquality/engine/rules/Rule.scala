package es.uniovi.uo237525.dataquality.engine.rules

import es.uniovi.uo237525.dataquality.engine.{EvaluationInfo, RemediationInfo, RuleInfo}
import scala.util.Try

import org.apache.spark.sql.DataFrame

/**
  * Trait the each of the options for a Rule must follow.
  */
trait RuleOptions

/**
  * Trait that represents the functionality of a quality rule.
  */
trait Rule {

  val id: String
  val field: String
  val options: RuleOptions

  /**
    * Checks all the needed conditions in order that the rule can be applied
    *
    * @return The error string in case a condition is not fulfilled. None otherwise
    */
  def checkPreconditions(df: DataFrame): Option[String]

  /**
    * Applies the rule to the data frame
    *
    * @param df the base dataframe where the rule is going to be applied
    * @return the rule log if the rule was successfully applied, an exception otherwise
    */
  def apply(df: DataFrame): Try[RuleInfo]
}

/**
  * Trait that each evaluation rule must implement.
  */
trait EvaluationRule extends Rule {
  override def apply(df: DataFrame): Try[EvaluationInfo]
}

/**
  * Trait that each remediation rule must implement.
  */
trait RemediationRule extends Rule {
  override def apply(df: DataFrame): Try[RemediationInfo]
}
