package es.uniovi.uo237525.dataqualityserver

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class Application

/**
  * Application, it can be run with the command
  * java -jar dataqualityserver-1.0.0.jar --server.port=8070
  */
object Application extends App {
  SpringApplication.run(classOf[Application], args:_*)
}