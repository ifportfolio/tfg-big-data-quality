#!/bin/sh

export KAFKA_HOME=/usr/local/kafka
export KAFKA_CONF_DIR=$KAFKA_HOME/config
export PATH=${KAFKA_HOME}/bin:${PATH}
