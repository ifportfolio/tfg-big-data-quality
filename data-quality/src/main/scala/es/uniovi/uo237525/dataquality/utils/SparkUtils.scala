package es.uniovi.uo237525.dataquality.utils

import scala.util.Try

import es.uniovi.uo237525.dataquality.SPARK_JOB_DEFAULT_NAME
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

/**
  * Utils class that provides useful functionality regarding the SparkSessions.
  */
object SparkUtils {

  /**
    * Returns an instance of a configured spark session, it makes use of the getOrCreate method so,
    * in case there is an active session it will reuse it.
    *
    * @param enableHiveSupport enables the hive support for the sessión
    * @param appName           name for the spark job
    * @param properties        map of value/key pairs used to provide the configuration params to the SparkSession
    * @return
    */
  def getSparkSession(enableHiveSupport: Boolean = false,
                      appName: String = SPARK_JOB_DEFAULT_NAME,
                      properties: Map[String, String] = Map()): SparkSession = {

    val conf = new SparkConf().setAll(properties)

    val aux = SparkSession.builder()
      .appName(appName)
      .config(conf)

    if (enableHiveSupport) aux.enableHiveSupport()
    val session = aux.getOrCreate()
    session.sparkContext.setLogLevel("WARN")
    session
  }

  /**
    * Returns the current database used by the provided SparkSession
    *
    * @param sparkSession SparkSession used to retrieve the current database.
    * @return the name of the current database
    */
  def getCurrentDatabase(sparkSession: SparkSession): String = {
    sparkSession.catalog.currentDatabase
  }

  /**
    * Modifies the current database used by the provided SparkSession
    *
    * @param sparkSession the session to modify the current database
    * @param dbName       the name of the database to put as current.
    * @return true if the change was successful, false otherwise
    */
  def changeSessionDatabase(sparkSession: SparkSession, dbName: String): Try[Unit] = {
    Try(sparkSession.sql(s"USE $dbName"))
  }

}
