#HOCON FILE See: https://github.com/lightbend/config/blob/master/HOCON.md

## REPORT FILE PATH
report.export.dir = "."

## MAIL SMTP CONNECTION
mail.smtp {
  //https://javaee.github.io/javamail/docs/api/com/sun/mail/smtp/package-summary.html
  user = ${?SMTP_USER}
  host = ${?SMTP_HOST}
  port = ${?SMTP_PORT}
  connectiontimeout = ${?SMTP_CONNECTIONTIMEOUT}
  timeout = ${?SMTP_TIMEOUT}
  writetimeout = ${?SMTP_WRITETIMEOUT}
  from = ${?SMTP_FROM}
  localhost = ${?SMTP_LOCALHOST}
  localaddress = ${?SMTP_LOCALADDRESS}
  localport = ${?SMTP_LOCALPORT}
  ehlo = ${?SMTP_EHLO}
  auth = ${?SMTP_AUTH}
  username = ${?SMTP_USERNAME}
  password = ${?SMTP_PASSWORD}
  auth.mechanisms = ${?SMTP_AUTH_MECHANISMS}
  auth.login.disable = ${?SMTP_AUTH_LOGIN_DISABLE}
  auth.plain.disable = ${?SMTP_AUTH_PLAIN_DISABLE}
  auth.digest-md5.disable = ${?SMTP_AUTH_DIGEST-MD5_DISABLE}
  auth.ntlm.disable = ${?SMTP_AUTH_NTLM_DISABLE}
  auth.ntlm.domain = ${?SMTP_AUTH_NTLM_DOMAIN}
  auth.ntlm.flags = ${?SMTP_AUTH_NTLM_FLAGS}
  auth.xoauth2.disable = ${?SMTP_AUTH_XOAUTH2_DISABLE}
  submitter = ${?SMTP_SUBMITTER}
  dsn.notify = ${?SMTP_DSN_NOTIFY}
  dsn.ret = ${?SMTP_DSN_RET}
  allow8bitmime = ${?SMTP_ALLOW8BITMIME}
  sendpartial = ${?SMTP_SENDPARTIAL}
  sasl.enable = ${?SMTP_SASL_ENABLE}
  sasl.mechanisms = ${?SMTP_SASL_MECHANISMS}
  sasl.authorizationid = ${?SMTP_SASL_AUTHORIZATIONID}
  sasl.realm = ${?SMTP_SASL_REALM}
  sasl.usecanonicalhostname = ${?SMTP_SASL_USECANONICALHOSTNAME}
  quitwait = ${?SMTP_QUITWAIT}
  reportsuccess = ${?SMTP_REPORTSUCCESS}
  socketFactory	SocketFactory = ${?SMTP_SOCKETFACTORY	SOCKETFACTORY}
  socketFactory.class = ${?SMTP_SOCKETFACTORY_CLASS}
  socketFactory.fallback = ${?SMTP_SOCKETFACTORY_FALLBACK}
  socketFactory.port = ${?SMTP_SOCKETFACTORY_PORT}
  ssl.enable = ${?SMTP_SSL_ENABLE}
  ssl.checkserveridentity = ${?SMTP_SSL_CHECKSERVERIDENTITY}
  ssl.trust = ${?SMTP_SSL_TRUST}
  ssl.socketFactory.SSLSocketFactory = ${?SMTP_SSL_SOCKETFACTORY_SSLSOCKETFACTORY}
  ssl.socketFactory.class = ${?SMTP_SSL_SOCKETFACTORY_CLASS}
  ssl.socketFactory.port = ${?SMTP_SSL_SOCKETFACTORY_PORT}
  ssl.protocols = ${?SMTP_SSL_PROTOCOLS}
  ssl.ciphersuites = ${?SMTP_SSL_CIPHERSUITES}
  starttls.enable = ${?SMTP_STARTTLS_ENABLE}
  starttls.required = ${?SMTP_STARTTLS_REQUIRED}
  proxy.host = ${?SMTP_PROXY_HOST}
  proxy.port = ${?SMTP_PROXY_PORT}
  proxy.user = ${?SMTP_PROXY_USER}
  proxy.password = ${?SMTP_PROXY_PASSWORD}
  socks.host = ${?SMTP_SOCKS_HOST}
  socks.port = ${?SMTP_SOCKS_PORT}
  mailextension = ${?SMTP_MAILEXTENSION}
  userset = ${?SMTP_USERSET}
  noop.strict = ${?SMTP_NOOP_STRICT}
}

## KAFKA CONNECTION
kafka {
  key.serializer = ${?KAFKA_KEY_SERIALIZER}
  value.serializer = ${?KAFKA_VALUE_SERIALIZER}
  bootstrap.servers = ${?KAFKA_BOOTSTRAP_SERVERS}
  topic = ${?KAFKA_TOPIC}
}

## REDIS CONNECTION
redis {
  host = ${?REDIS_HOST}
  port = ${?REDIS_PORT}
}