import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


// import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { ConfigEditorComponent, CreateConfigFileDialog } from './component/config-editor/config-editor.component';
import { RulesEditorComponent, CreateRulesFileDialog } from './component/rules-editor/rules-editor.component';
import { RuleLogsPaneComponent } from './component/dashboard/rule-logs-pane/rule-logs-pane.component';
import { GenericLogsPaneComponent } from './component/dashboard/generic-logs-pane/generic-logs-pane.component';


import { MatToolbarModule } from '@angular/material/toolbar';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatChipsModule } from '@angular/material/chips';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatDividerModule } from '@angular/material/divider';
import { MatTableModule } from '@angular/material/table';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCheckboxModule} from '@angular/material/checkbox';

import { AceModule } from 'ngx-ace-wrapper';
import { ACE_CONFIG } from 'ngx-ace-wrapper';
import { AceConfigInterface } from 'ngx-ace-wrapper';

//services
import { LaunchBdqService } from './services/launch-bdq.service';
import { AssetLoaderService } from './services/asset-loader.service';

/**
 * Empty default config for the AceEditor
 */
const DEFAULT_ACE_CONFIG: AceConfigInterface = {};

/*const routes: Routes = [
  { path: '**', component: AppComponent }
];*/

@NgModule({
  entryComponents: [RulesEditorComponent, CreateRulesFileDialog, ConfigEditorComponent, CreateConfigFileDialog],
  declarations: [
    AppComponent,
    DashboardComponent,
    ConfigEditorComponent,
    CreateConfigFileDialog,
    RulesEditorComponent,
    CreateRulesFileDialog,
    GenericLogsPaneComponent,
    RuleLogsPaneComponent
  ],
  imports: [
  // RouterModule.forRoot(routes),
    MatDividerModule,
    MatChipsModule,
    MatToolbarModule,
    MatStepperModule,
    MatButtonModule,
    MatTabsModule,
    MatIconModule,
    MatCardModule,
    MatGridListModule,
    MatSlideToggleModule,
    MatExpansionModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatInputModule,
    MatTableModule,
    MatListModule,
    MatDialogModule,
    MatCheckboxModule,
    AceModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    {
      provide: ACE_CONFIG,
      useValue: DEFAULT_ACE_CONFIG
    },
    LaunchBdqService,
    AssetLoaderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
