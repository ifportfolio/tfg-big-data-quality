import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

/**
 * Service that provides functionality for loadinf resources from the assets folder.
 */
@Injectable({
  providedIn: 'root'
})
export class AssetLoaderService {

  /**
   * Injects the needed services
   * @param http service for reading the files
   */
  constructor(private http: HttpClient) { }

  /**
   * Loads a text file content from the assets folder
   * @param filename Name of the file to load its content.
   */
  public loadTextFileAsset(filename:string) {
    return this.http.get('assets/'+filename, {responseType: 'text'})
  }
}
