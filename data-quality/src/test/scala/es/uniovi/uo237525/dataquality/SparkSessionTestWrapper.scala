package es.uniovi.uo237525.dataquality

import org.apache.spark.sql.SparkSession

trait SparkSessionTestWrapper {

  lazy val spark: SparkSession = {
    SparkSession.builder()
      .master("local")
      .appName("spark-data-quality-test-job")
      .enableHiveSupport()
      .getOrCreate()
  }

}
