#!/bin/bash

#Stop WEB APP script
PID_FILE=$(cd `dirname $0` && pwd)/../logs/pid-bdq-web
chmod 777 $PID_FILE
PID=$(<$PID_FILE)
echo $PID_FILE
echo $PID
kill $PID
rm -rf $PID_FILE
