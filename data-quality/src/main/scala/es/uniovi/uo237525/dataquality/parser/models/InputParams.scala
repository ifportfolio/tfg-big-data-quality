package es.uniovi.uo237525.dataquality.parser.models

/**
  *
  * All the possible input configuration parameters.
  *
  * @see https://spark.apache.org/docs/latest/api/java/org/apache/spark/sql/DataFrameReader.html
  * @param source                          Determines the type of the source from which the module is going to read the data.
  * @param csv_schema                      A ddl spark specification of the data schema.
  * @param csv_path                        Path of the csv file that contains the data.
  * @param csv_sep                         (default -> ,) Character used as a separator for each field and value.
  *                                        Delimiter can be set to any character.
  * @param csv_encoding                    (default UTF-8) Decodes the CSV files by the given encoding type.
  * @param csv_quote                       (default ") Character used for escaping quoted values where the separator can be part of the value.
  *                                        If you would like to turn off quotations, you need to set not null but an empty string.
  * @param csv_escape                      (default \) Sets a single character used for escaping quotes inside an already quoted value.
  * @param csv_charToEscapeQuoteEscaping   (default escape or \0) Sets a single character used for escaping the escape for the quote character.
  *                                        The default value is escape character when escape and quote characters are different, \0 otherwise.
  * @param csv_comment                     (default empty string) Character used for skipping lines beginning with this character.
  *                                        By default, it is disabled.
  * @param csv_header                      (default false) Uses the first line as names of columns.
  * @param csv_enforceSchema               (default true) If set to true, the specified or inferred schema will be forcibly applied to datasource files,
  *                                        and headers in CSV files will be ignored.
  *                                        If false, the schema will be validated against all headers in case the header option is true.
  *                                        Field names in the schema and column names in CSV headers are checked by their positions taking into account spark.sql.caseSensitive.
  *                                        Though the default value is true, it is recommended to disable the enforceSchema
  *                                        option to avoid incorrect results.
  * @param csv_inferSchema                 (default false) Infers the input schema automatically from data.
  *                                        It requires one extra pass over the data.
  * @param csv_samplingRatio               (default is 1.0) Defines fraction of rows used for schema inferring.
  * @param csv_ignoreLeadingWhiteSpace     (default false) Flag indicating if leading whitespaces from values being read should be skipped.
  * @param csv_ignoreTrailingWhiteSpace    (default false) Flag indicating if trailing whitespaces from values being read should be skipped.
  * @param csv_nullValue                   (default empty string) String representation of a null value.
  * @param csv_emptyValue                  (default empty string): String representation of an empty value.
  * @param csv_nanValue                    (default NaN): String representation of a non-number" value.
  * @param csv_positiveInf                 (default Inf): String representation of a positive infinity value.
  * @param csv_negativeInf                 (default -Inf): String representation of a negative infinity value.
  * @param csv_dateFormat                  (default yyyy-MM-dd) String that indicates a date format. This applies to date type.
  *                                        Custom date formats follow the formats at java.text.SimpleDateFormat.
  * @param csv_timestampFormat             (default yyyy-MM-dd'T'HH:mm:ss.SSSXXX) String that indicates a timestamp format.
  *                                        This applies to timestamp type. Custom date formats follow the formats at java.text.SimpleDateFormat.
  * @param csv_maxColumns                  (default 20480) Defines a hard limit of how many columns a record can have.
  * @param csv_maxCharsPerColumn           (default -1) Defines the maximum number of chars allowed for any given value being read.
  *                                        By default, it is -1 meaning unlimited length.
  * @param csv_mode                        (default PERMISSIVE) Allows a mode for dealing with corrupt records during parsing.
  *                                        It supports the following case-insensitive modes:
  *                                        - PERMISSIVE : when it meets a corrupted record, puts the malformed string
  *                                        into a field configured by columnNameOfCorruptRecord, and sets other fields
  *                                        to null. To keep corrupt records, a user can set a string type field named
  *                                        columnNameOfCorruptRecord in an user-defined schema.
  *                                        If a schema does not have the field, it drops corrupt records during parsing.
  *                                        A record with less/more tokens than schema is not a corrupted record to CSV.
  *                                        When it meets a record having fewer tokens than the length of the schema,
  *                                        sets null to extra fields. When the record has more tokens than the length
  *                                        of the schema, it drops extra tokens.
  *                                        - DROPMALFORMED : ignores the whole corrupted records.
  *                                        - FAILFAST : throws an exception when it meets corrupted records.
  * @param csv_columnNameOfCorruptRecord   (default is the value specified in spark.sql.columnNameOfCorruptRecord)
  *                                        Allows renaming the new field having malformed string created by PERMISSIVE mode.
  * @param csv_multiLine                   (default false) Parse one record, which may span multiple lines.
  * @param hive_metastore_uris             Location of the metastore database to connect
  * @param hive_warehouse_dir              Location of the hive warehouse.
  * @param hive_database                   Database name where the table is stored
  * @param hive_table                      Table where the hive needs to read from
  *
  */
case class InputParams(source: String,
                       csv_schema: Option[String] = None,
                       csv_path: Option[String] = None,
                       csv_sep: Option[String] = None,
                       csv_encoding: Option[String] = None,
                       csv_quote: Option[String] = None,
                       csv_escape: Option[String] = None,
                       csv_charToEscapeQuoteEscaping: Option[String] = None,
                       csv_comment: Option[String] = None,
                       csv_header: Option[Boolean] = None,
                       csv_enforceSchema: Option[Boolean] = None,
                       csv_inferSchema: Option[Boolean] = None,
                       csv_samplingRatio: Option[Double] = None,
                       csv_ignoreLeadingWhiteSpace: Option[Boolean] = None,
                       csv_ignoreTrailingWhiteSpace: Option[Boolean] = None,
                       csv_nullValue: Option[String] = None,
                       csv_emptyValue: Option[String] = None,
                       csv_nanValue: Option[String] = None,
                       csv_positiveInf: Option[String] = None,
                       csv_negativeInf: Option[String] = None,
                       csv_dateFormat: Option[String] = None,
                       csv_timestampFormat: Option[String] = None,
                       csv_maxColumns: Option[Int] = None,
                       csv_maxCharsPerColumn: Option[Int] = None,
                       csv_mode: Option[String] = None,
                       csv_columnNameOfCorruptRecord: Option[String] = None,
                       csv_multiLine: Option[Boolean] = None,
                       hive_metastore_uris: Option[String] = None,
                       hive_warehouse_dir: Option[String] = None,
                       hive_database: Option[String] = None,
                       hive_table: Option[String] = None) {

  def getOptionsByHeader(header: String): Map[String, String] = {
    classOf[InputParams].getDeclaredFields
      .filter(_.getName.contains(header))
      .map(field => {
        field.setAccessible(true)
        val value: String = field.get(this) match {
          case Some(n) => n.toString
          case None => ""
        }
        val name: String = field.getName.replace("_", ".").split('.').last
        (name, value)
      }).filter(_._2.length > 0).toMap
  }
}