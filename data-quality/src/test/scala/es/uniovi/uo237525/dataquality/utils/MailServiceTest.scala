package es.uniovi.uo237525.dataquality.utils

import java.io.ByteArrayInputStream
import java.nio.charset.StandardCharsets

import javax.mail.{MessagingException, NoSuchProviderException, Session}
import javax.mail.internet.{AddressException, MimeMessage}
import org.scalatest.{BeforeAndAfterEach, FunSuite}

class MailServiceTest extends FunSuite with BeforeAndAfterEach {

  var emptySession: Session = MailService.createSession(Map[String, String]())
  var basicMimeMsg = new MimeMessage(emptySession, new ByteArrayInputStream("message".getBytes(StandardCharsets.UTF_8)))

  override def beforeEach(): Unit = {
    emptySession = MailService.createSession(Map[String, String]())
    basicMimeMsg = new MimeMessage(emptySession, new ByteArrayInputStream("message".getBytes(StandardCharsets.UTF_8)))
  }

  /** CREATE SESSION TESTS */
  test("test createSession with empty map") {
    assert(emptySession.isInstanceOf[Session])
  }
  test("test createSession with null properties throws NullPointerException") {
    assertThrows[NullPointerException] {
      MailService.createSession(null)
    }
  }
  test("test createSession with null properties map throws NullPointerException") {
    val prop = Map[String, String](
      "smtp.host" -> null
    )
    assertThrows[NullPointerException] {
      MailService.createSession(prop)
    }
  }
  test("test createSession with valid properties format are correctly parsed") {
    val h1 = "smtp.host"
    val v1 = "up1"
    val h2 = "unrecognized.property2"
    val v2 = "up2"
    val prop = Map[String, String](
      h1 -> v1,
      h2 -> v2
    )
    val s = MailService.createSession(prop)
    assertResult(v1) {
      s.getProperty(h1)
    }
    assertResult(v2) {
      s.getProperty(h2)
    }
  }

  /** CREATE MIME MESSAGE TESTS */
  test("test createMimeMessage: empty session") {
    val msg = MailService.createMimeMessage(null, "content", "text/html")
    assert(msg.getSession == null)
  }
  test("test createMimeMessage: empty content") {
    val msg = MailService.createMimeMessage(emptySession, null, "text/html")
    assert(msg.getContent == null)
    val msg1 = MailService.createMimeMessage(emptySession, "", "text/html")
    assert(msg1.getContent == "")
  }
  test("test createMimeMessage: empty or invalid contentType is substituted by text/plain") {
    val DEFAULT_CONTENT_TYPE = "text/plain"
    val msgNull = MailService.createMimeMessage(MailService.createSession(Map()), "", "text/plain")
    assert(msgNull.getContentType == DEFAULT_CONTENT_TYPE)
    val msgEmpty = MailService.createMimeMessage(MailService.createSession(Map()), "", "")
    assert(msgEmpty.getContentType == DEFAULT_CONTENT_TYPE)
    val msgInvalid = MailService.createMimeMessage(MailService.createSession(Map()), "", "no-valid-content-type")
    assert(msgInvalid.getContentType == DEFAULT_CONTENT_TYPE)
  }

  /** SEND MIME MESSAGE TESTS */
  test("test sendMimeMessage: null session throws NullPointerException") {
    assertThrows[NullPointerException] {
      MailService.sendMimeMessage(null, "smtp", basicMimeMsg, "subject", "address@mail.com")
    }
    MailService.sendMimeMessage(emptySession, "smtp", basicMimeMsg, "subject", "address@mail.com")

  }

  test("test sendMimeMessage: empty, null or invalid protocol throws NoSuchProviderException") {
    assertThrows[NoSuchProviderException] {
      MailService.sendMimeMessage(emptySession, null, new MimeMessage(emptySession), "subject", "mail")
    }
    assertThrows[NoSuchProviderException] {
      MailService.sendMimeMessage(emptySession, "", new MimeMessage(emptySession), "subject", "mail")
    }
    assertThrows[NoSuchProviderException] {
      MailService.sendMimeMessage(emptySession, "invalid", new MimeMessage(emptySession), "subject", "mail")
    }
  }

  test("test sendMimeMessage: null MimeMessage throws NullPointerException") {
    assertThrows[NullPointerException] {
      MailService.sendMimeMessage(emptySession, "smtp", null, "subject", "mail")
    }
  }
  test("test sendMimeMessage: empty MimeMessage throws MessagingException") {
    assertThrows[MessagingException] {
      MailService.sendMimeMessage(emptySession, "smtp", new MimeMessage(emptySession), "subject", "mail")
    }
  }

  test("test sendMimeMessage: null address array throws NullPointerException") {
    assertThrows[NullPointerException] {
      MailService.sendMimeMessage(emptySession, "smtp", basicMimeMsg, "subject", null)
    }
  }

  test("test sendMimeMessage: empty address array throws NullPointerException") {
    assertThrows[NullPointerException] {
      MailService.sendMimeMessage(emptySession, "smtp", basicMimeMsg, "subject", "")
    }
  }

  test("test sendMimeMessage: invalid addresses array throws AddressException") {
    assertThrows[AddressException] {
      MailService.sendMimeMessage(emptySession, "smtp", basicMimeMsg, "subject", "/*^Ç¨Ç\\,")
    }
  }

  test("test sendMimeMessage: valid parameter send message") {
    MailService.sendMimeMessage(emptySession, "smtp", basicMimeMsg, "subject", "address@mail.com")
    succeed
  }

}
