package es.uniovi.uo237525.dataquality.engine.rules.evaluation

import java.text.SimpleDateFormat
import java.util.UUID

import es.uniovi.uo237525.dataquality.LogConstants.Level
import es.uniovi.uo237525.dataquality.engine.EvaluationInfo
import es.uniovi.uo237525.dataquality.engine.rules.{EvaluationRule, RuleOptions}
import scala.util.{Failure, Success, Try}

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StringType


case class EvDateFormatOptions(format: String) extends RuleOptions

/**
  * This evaluation rule checks the number of records in a string field that follow a provided date/timestamp format.
  * It can only be applied on string type fields and the provided format needs to comply with the java.text.SimpleDateFormat library
  *
  * @param id      Identifier for the rule. Automatically generated if not provided
  * @param field   Field in which the rule is going to be applied
  * @param options EvDateFormatOptions needed for the rule application.
  */
case class EvDateFormat(id: String = UUID.randomUUID().toString,
                        field: String,
                        options: EvDateFormatOptions) extends EvaluationRule {

  private val format = options.format

  /**
    * Checks the needed preconditions for the rule.
    *
    * The preconditions that need to be fulfilled are:
    *   - The field exists
    *   - The provided format is compliant with the SimpleDateFormat java library.
    *   - The provided field is of StringType
    *
    * @param df the dataframe to evaluate
    * @return The error string in case a condition is not fulfilled. None otherwise
    */
  override def checkPreconditions(df: DataFrame): Option[String] = {
    val exists = Try(df(field)).isSuccess
    if (!exists) {
      val errorMsg = s"Field '$field' does not exists."
      return Some(errorMsg)
    }

    try {
      new SimpleDateFormat(format)
    }
    catch {
      case th: Throwable =>
        val errorMsg = th.getMessage
        return Some(errorMsg)
    }

    val columnType = df.select(field).schema.head.dataType
    columnType match {
      case StringType => None
      case _ =>
        val errorMsg = s"Rule cannot be applied to fields of type: '$columnType'."
        Some(errorMsg)
    }
  }

  /**
    * Applies the rule over the dataframe.
    * Calculated the number of records in a field follow the provided date pattern.
    *
    * @param df the base dataframe where the rule is going to be applied
    * @return a Try object containing:
    *         - Success: The EvaluationInfo object compiled from the rule application
    *         - Failure: If the rule applications throws an exception.
    */
  override def apply(df: DataFrame): Try[EvaluationInfo] = Try {
    val totalRows = df.select(field).count()
    val isParsableUdf = udf((value: String) => {
      // we already know that the format is correct (checked in pre-conditions)
      val dateFormat = new SimpleDateFormat(format)
      dateFormat.setLenient(false)
      Try(dateFormat.parse(value)) match {
        case Success(parsedDate) => dateFormat.format(parsedDate).equals(value)
        case Failure(_) => false
      }
    })
    val countParsable = df.select(field).filter(isParsableUdf(col(field))).count()

    EvaluationInfo(
      Level.INFO,
      s"Evaluation of pattern: '$format' in '$field'",
      countParsable,
      totalRows)
  }

}
