package es.uniovi.uo237525.dataquality.engine.rules.evaluation

import java.util.UUID

import es.uniovi.uo237525.dataquality.LogConstants.Level
import es.uniovi.uo237525.dataquality.engine.EvaluationInfo
import es.uniovi.uo237525.dataquality.engine.rules.{EvaluationRule, RuleOptions}
import scala.util.Try

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{ArrayType, MapType}

case class EvFieldLengthOptions(max: Long) extends RuleOptions

/**
  * The purpose of this rule is the detection of whether a field record is out of the max allowed characters range.
  * When used on Array fields, it evaluates it based on the number of elements that the record contains.
  *
  * @param id      Identifier for the rule. Automatically generated if not provided
  * @param field   Field in which the rule is going to be applied
  * @param options EvDateFormatOptions needed for the rule application.
  */
case class EvFieldLength(id: String = UUID.randomUUID().toString,
                         field: String,
                         options: EvFieldLengthOptions) extends EvaluationRule {

  val max = options.max

  /**
    * Checks the needed preconditions for the rule.
    *
    * The preconditions that need to be fulfilled are:
    *   - The field exists
    *   - The max param is not lower than 0
    *
    * @param df the dataframe to evaluate
    * @return The error string in case a condition is not fulfilled. None otherwise
    */
  override def checkPreconditions(df: DataFrame): Option[String] = {
    val exists = Try(df(field)).isSuccess
    if (!exists) {
      val errorMsg = s"Field $field does not exists."
      return Some(errorMsg)
    }

    if (max < 0) Some("'max' must be equal or greater than 0.")
    else None
  }

  /**
    * Calculates the number of records in a field that are shorter than the maximum established.
    *
    * In case of Array & Map fields, it calculates the number of elements that contains.
    *
    * Null records are included as non-matched by the rule.
    *
    * @param df the base dataframe where the rule is going to be applied
    * @return A pair containing the resulting DataFrame and
    *         the results of the application of the rule
    */
  override def apply(df: DataFrame): Try[EvaluationInfo] = Try {
    val totalRows = df.select(field).count()

    val columnType = df.select(field).schema.head.dataType
    val recordsShorter = columnType match {
      case ArrayType(_, _) => df.select(field).filter(size(col(field)) <= max && size(col(field)) >= 0).count()
      case MapType(_, _, _) => df.select(field).filter(size(col(field)) <= max && size(col(field)) >= 0).count()
      case _ => df.select(field).filter(length(col(field)) <= max).count()
    }

    EvaluationInfo(Level.INFO,
      s"Max length: $max",
      recordsShorter,
      totalRows)
  }

}
