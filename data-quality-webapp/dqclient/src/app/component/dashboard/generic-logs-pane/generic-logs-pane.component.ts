import { Component, OnInit, Input, SimpleChanges, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

/**
 * Sub-component of the dashboard.
 * 
 * It shows all the alerts received with the exception of the Rule type ones in a table.
 */
@Component({
  selector: 'app-generic-logs-pane',
  templateUrl: './generic-logs-pane.component.html',
  styleUrls: ['./generic-logs-pane.component.css']
})
export class GenericLogsPaneComponent implements OnInit {
  /**
   * Raw list of all the logs received from the dashboard
   */
  @Input() logs: Array<any>;

  /**
   * The table displayed columns
   */
  displayedColumns: string[] = ['timestamp', 'level', 'type', 'info'];

  /**
   * The datasource for the table. Based on the received logs
   */
  private dataSource;

  /**
   * Sorting element of Angular material
   */
  @ViewChild(MatSort) sort: MatSort;

  /**
   * Initializes the sorting element
   */
  ngOnInit() {
    this.dataSource.sort = this.sort;
    this.dataSource = new MatTableDataSource(
      this.logs.filter(l => l.logType.name.toLocaleLowerCase() !== 'rule'))
  }

  /**
   * Refresh the table information.
   * 
   * @param changes the changes received from the dashboard
   */
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes)
    this.logs = changes.logs.currentValue
    let genericLogs = this.logs.filter(l => l.logType.name.toLocaleLowerCase() !== 'rule')
    this.dataSource = new MatTableDataSource(genericLogs)
  }

  /**
   * Filters the data registers with the input string.
   * @param filterValue The string to filter with
   */
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
