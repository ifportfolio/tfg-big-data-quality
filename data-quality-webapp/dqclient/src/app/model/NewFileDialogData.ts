/**
 * Model for storing the result of the user interactino when generating a new file.
 */
export class NewFileDialogData {
    /**
     * Name of the file that will be generated.
     */
    filename: string;
    /**
     * Represents whether the file content should be generated with a predefined template or not
     */
    useTemplate: boolean;
}