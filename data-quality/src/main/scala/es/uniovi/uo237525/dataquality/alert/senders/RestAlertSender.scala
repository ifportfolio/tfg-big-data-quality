//package es.uniovi.uo237525.dataquality.alert.senders
//
//import java.util
//
//import es.uniovi.uo237525.dataquality.alert.AlertSender
//import es.uniovi.uo237525.dataquality.configuration.Configuration
//import es.uniovi.uo237525.dataquality.logmanager.{Log, LogManager}
//import es.uniovi.uo237525.dataquality.parser.models.{Alert, RestAlert}
//import org.apache.http.NameValuePair
//import org.apache.http.client.entity.UrlEncodedFormEntity
//import org.apache.http.client.methods.HttpPost
//import org.apache.http.impl.client.HttpClientBuilder
//import org.apache.http.message.BasicNameValuePair
//import org.apache.log4j.Logger
//
//import scala.util.{Failure, Success, Try}
//
//case class RestAlertSender() extends AlertSender {
//
//  override val config: Configuration = null
//
//  val logger = Logger.getRootLogger
//
//  override def sendAlerts(logs: Iterable[Log], alerts: Array[Alert]): Iterable[(Alert, Iterable[Log])] = {
//    if (alerts.nonEmpty) {
//      alerts.filter(al => al.enabled && al.events.nonEmpty && al.isInstanceOf[RestAlert]).map(al => {
//        Try({
//          val cast = al.asInstanceOf[RestAlert]
//
//          val logsToSend = if (al.events.map(_.toLowerCase).toList.contains(Event.ALL.toString.toLowerCase())) logs
//          else logs.filter(l => al.events.map(_.toLowerCase).toList.contains(l.event.toString.toLowerCase))
//
//          val json = LogManager.produceJsonFromLogs(logsToSend.toList)
//
//          // add name value pairs to a post object
//          val post = new HttpPost(cast.url)
//
//          val nameValuePairs = new util.ArrayList[NameValuePair]()
//          nameValuePairs.add(new BasicNameValuePair(cast.paramName, json))
//          post.setEntity(new UrlEncodedFormEntity(nameValuePairs))
//
//          // send the post request
//          val client = HttpClientBuilder.create().build()
//          val response = client.execute(post)
//          response.getAllHeaders.foreach(arg => logger.info(arg.toString))
//          (al, logsToSend)
//        }) match {
//          case Success(i) => logger.warn("Successfully sent info"); i
//          case Failure(exception) => logger.warn(s"Unable to send request -> ${exception.getLocalizedMessage}"); null
//        }
//      }).filter(_ != null)
//    }
//    else Iterable()
//  }
//
//}