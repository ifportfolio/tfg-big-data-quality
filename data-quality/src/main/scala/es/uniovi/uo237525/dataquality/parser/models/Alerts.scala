package es.uniovi.uo237525.dataquality.parser.models

/**
  * Wrapper for all the alert types.
  *
  * @param mail  List of mail alerts
  * @param kafka List of kafka alerts
  * @param redis List of reddi alerts
  */
case class Alerts(mail: Array[MailAlert],
                  kafka: Array[KafkaAlert],
                  redis: Array[RedisAlert]) {}

/**
  * Trait that needs to be implemented by any alert type.
  */
trait Alert {
  val enabled: Boolean
  val level: String
}

/**
  * Case class for the mail alert.
  *
  * @param enabled Flag that indicates whether it is active or not. (true by default)
  * @param level   Granularity level associated to the alert
  * @param to      Destination addresses of the mail alert
  * @param cc      cc addresses of the mail alert
  * @param cco     cco addresses of the mail alert
  * @param subject subject of the mail alert
  */
case class MailAlert(enabled: Boolean = true, level: String,
                     to: String, cc: Option[String] = None, cco: Option[String] = None, subject: String) extends Alert

/**
  * Case class for the kafka alert
  *
  * @param enabled Flag that indicates whether it is active or not. (true by default)
  * @param level   Granularity level associated to the alert
  * @param topic   the Kafka topic in which the alert will be emitted.
  */
case class KafkaAlert(enabled: Boolean = true, level: String, topic: String) extends Alert

// case class RestAlert(enabled: Boolean = true, level: String, url: String, paramName: String) extends Alert

/**
  * Case class for the Redis alerts
  *
  * @param enabled Flag that indicates whether it is active or not. (true by default)
  * @param level   Granularity level associated to the alert
  * @param key     Key of the list that will store the ids
  */
case class RedisAlert(enabled: Boolean = true, level: String, key: String) extends Alert