package es.uniovi.uo237525.dataquality.engine.rules.remediation

import java.text.SimpleDateFormat
import java.util.UUID

import es.uniovi.uo237525.dataquality.LogConstants.Level
import es.uniovi.uo237525.dataquality.engine.RemediationInfo
import es.uniovi.uo237525.dataquality.engine.rules.{RemediationRule, RuleOptions}
import es.uniovi.uo237525.dataquality.utils.SparkUtils
import scala.util.{Failure, Success, Try}

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

case class RemDateFormatOptions(newFormat: String, candidatePatterns: Option[List[String]] = None) extends RuleOptions

/**
  * This remediation rule, detects the records in a field that do not match a specific pattern and tries to fix them.
  * It uses as list of candidate predefined patterns to detect the record value, but is highly encouraged to provided a
  * hint list with the provided patterns you expect in order to facilitate the pattern recognition.
  *
  * The candidate patterns are:
  *   -"yyyy-MM-dd HH:mm:ss.SSS",
  *   -"yyyy-MM-dd HH:mm:ss.SS",
  *   -"yyyy-MM-dd HH:mm:ss.S",
  *   -"yyyy-MM-dd HH:mm:ss",
  *   -"yyyy-MM-dd HH:mm ",
  *   -"yyyy-MM-dd",
  *   -"HH:mm",
  *   -"HH",
  *   -"yyyyMMddHHmm",
  *   -"yyyyMMdd HHmm",
  *   -"dd-MM-yyyy HH:mm",
  *   -"yyyy-MM-dd HH:mm",
  *   -"MM/dd/yyyy HH:mm",
  *   -"yyyy/MM/dd HH:mm",
  *   -"dd MMM yyyy HH:mm",
  *   -"dd MMMM yyyy HH:mm",
  *   -"yyyyMMddHHmmss",
  *   -"yyyyMMdd HHmmss",
  *   -"dd-MM-yyyy HH:mm:ss",
  *   -"yyyy-MM-dd HH:mm:ss",
  *   -"MM/dd/yyyy HH:mm:ss",
  *   -"yyyy/MM/dd HH:mm:ss",
  *   -"dd MMM yyyy HH:mm:ss",
  *   -"dd MMMM yyyy HH:mm:ss",
  *   -"yyyy-MM-dd HH:mm:ss.SSSZZZZ",
  *   -"yyyy-MM-dd'T'HH:mm:ssz",
  *   -"yyyy-MM-dd'T'HH:mm:ssZZZZ",
  *   -"yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ",
  *   -"yyyy-MM-dd'T'HH:mm:ssSSSSSS",
  *   -"EEE, dd MMM yyyy HH:mm:ss z",
  *   -"yyyy.MM.dd G 'at' HH:mm:ss z",
  *   -"EEE, MMM d, ''yy",
  *   -"h:mm a",
  *   -"hh 'o''clock' a, zzzz",
  *   -"K:mm a, z",
  *   -"yyyyy.MMMMM.dd GGG hh:mm aaa",
  *   -"EEE, d MMM yyyy HH:mm:ss Z",
  *   -"yyMMddHHmmssZ",
  *   -"yyyy-MM-dd'T'HH:mm:ss.SSSZ",
  *   -"yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
  *   -"YYYY-'W'ww-u",
  *   -// yyyy MM dd
  *   -"yyyyMMdd",
  *   -"yyyy MM dd",
  *   -"yyyy MMMM dd",
  *   -"yyyy/MM/dd",
  *   -"yyyy.MM.dd",
  *   -"yyyyMdd",
  *   -"yyyy M dd",
  *   -"yyyy-M-dd",
  *   -"yyyy/M/dd",
  *   -"yyyy.M.dd",
  *   -"yyyy MMM dd",
  *   -// dd MM yyyy
  *   -"ddMMyyyy",
  *   -"dd-MM-yyyy",
  *   -"dd/MM/yyyy",
  *   -"dd.MM.yyyy",
  *   -"dd MMM yyyy",
  *   -"dd MMMM yyyy",
  *   -"ddMyyyy",
  *   -"dd-M-yyyy",
  *   -"dd/M/yyyy",
  *   -"dd.M.yyyy",
  *   -"dd MMM yyyy",
  *   -// MM dd yyyy
  *   -"MMddyyyy",
  *   -"MM-dd-yyyy",
  *   -"MM/dd/yyyy",
  *   -"MM.dd.yyyy",
  *   -"MM dd yyyy",
  *   -"MMMM dd yyyy",
  *   -"Mddyyyy",
  *   -"M-dd-yyyy",
  *   -"M/dd/yyyy",
  *   -"M.dd.yyyy",
  *   -"M dd yyyy",
  *   -"MMM dd yyyy"
  *
  * @param id      Identifier for the rule. Automatically generated if not provided
  * @param field   Field in which the rule is going to be applied
  * @param options RemNumericFormatOptions needed for the rule application.
  */
case class RemDateFormat(id: String = UUID.randomUUID().toString,
                         field: String,
                         options: RemDateFormatOptions) extends RemediationRule {

  private val CANDIDATE_PATTERNS: List[String] = List(
    // TIMESTAMPS
    "yyyy-MM-dd HH:mm:ss.SSS",
    "yyyy-MM-dd HH:mm:ss.SS",
    "yyyy-MM-dd HH:mm:ss.S",
    "yyyy-MM-dd HH:mm:ss",
    "yyyy-MM-dd HH:mm ",
    "yyyy-MM-dd",
    "HH:mm",
    "HH",
    "yyyyMMddHHmm",
    "yyyyMMdd HHmm",
    "dd-MM-yyyy HH:mm",
    "yyyy-MM-dd HH:mm",
    "MM/dd/yyyy HH:mm",
    "yyyy/MM/dd HH:mm",
    "dd MMM yyyy HH:mm",
    "dd MMMM yyyy HH:mm",
    "yyyyMMddHHmmss",
    "yyyyMMdd HHmmss",
    "dd-MM-yyyy HH:mm:ss",
    "yyyy-MM-dd HH:mm:ss",
    "MM/dd/yyyy HH:mm:ss",
    "yyyy/MM/dd HH:mm:ss",
    "dd MMM yyyy HH:mm:ss",
    "dd MMMM yyyy HH:mm:ss",
    "yyyy-MM-dd HH:mm:ss.SSSZZZZ",
    "yyyy-MM-dd'T'HH:mm:ssz",
    "yyyy-MM-dd'T'HH:mm:ssZZZZ",
    "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ",
    "yyyy-MM-dd'T'HH:mm:ssSSSSSS",
    "EEE, dd MMM yyyy HH:mm:ss z",
    "yyyy.MM.dd G 'at' HH:mm:ss z",
    "EEE, MMM d, ''yy",
    "h:mm a",
    "hh 'o''clock' a, zzzz",
    "K:mm a, z",
    "yyyyy.MMMMM.dd GGG hh:mm aaa",
    "EEE, d MMM yyyy HH:mm:ss Z",
    "yyMMddHHmmssZ",
    "yyyy-MM-dd'T'HH:mm:ss.SSSZ",
    "yyyy-MM-dd'T'HH:mm:ss.SSSXXX",
    "YYYY-'W'ww-u",
    // yyyy MM dd
    "yyyyMMdd",
    "yyyy MM dd",
    "yyyy MMMM dd",
    "yyyy/MM/dd",
    "yyyy.MM.dd",
    "yyyyMdd",
    "yyyy M dd",
    "yyyy-M-dd",
    "yyyy/M/dd",
    "yyyy.M.dd",
    "yyyy MMM dd",
    // dd MM yyyy
    "ddMMyyyy",
    "dd-MM-yyyy",
    "dd/MM/yyyy",
    "dd.MM.yyyy",
    "dd MMM yyyy",
    "dd MMMM yyyy",
    "ddMyyyy",
    "dd-M-yyyy",
    "dd/M/yyyy",
    "dd.M.yyyy",
    "dd MMM yyyy",
    // MM dd yyyy
    "MMddyyyy",
    "MM-dd-yyyy",
    "MM/dd/yyyy",
    "MM.dd.yyyy",
    "MM dd yyyy",
    "MMMM dd yyyy",
    "Mddyyyy",
    "M-dd-yyyy",
    "M/dd/yyyy",
    "M.dd.yyyy",
    "M dd yyyy",
    "MMM dd yyyy"
  )


  /**
    * Determines whether a pattern is correct or not.
    *
    * @param pattern pattern to check
    * @return true if pattern correct, false otherwise.
    */
  private def isPatternValid(pattern: String): Boolean = {
    Try(new SimpleDateFormat(pattern)).isSuccess
  }

  /**
    * Checks the needed preconditions for the rule.
    *
    * The preconditions that need to be fulfilled are:
    *   - The field exists
    *   - The field is of type StringType
    *   - The provided format is compliant with the SimpleDateFormat java library.
    *   - The provided field is of StringType
    *
    * @param df the dataframe to evaluate
    * @return The error string in case a condition is not fulfilled. None otherwise
    */
  override def checkPreconditions(df: DataFrame): Option[String] = {
    // check field exists
    val res = Try(df(field)).isSuccess
    if (!res) {
      return Some(s"Field $field does not exists.")
    }

    // check column type
    val columnType = df.select(field).schema.head.dataType
    columnType match {
      case StringType => None
      case _ =>
        val errorMsg = s"Remediation phase cannot be applied to fields of type: '$columnType'."
        return Some(errorMsg)
    }

    // check new format pattern
    val newFormat = options.newFormat
    try {
      new SimpleDateFormat(newFormat)
    }
    catch {
      case th: Throwable =>
        val errorMsg = th.getMessage
        return Some(errorMsg)
    }
    None
  }

  /**
    * Applies the rule.
    * If a provided pattern is not valid,it will just discard him from the list
    *
    * @param df the base dataframe where the rule is going to be applied
    * @return A pair containing the resulting DataFrame and
    *         the results of the application of the rule
    */
  override def apply(df: DataFrame): Try[RemediationInfo] = Try {
    val spark = SparkUtils.getSparkSession()
    import spark.implicits._

    val candidatePatterns = options.candidatePatterns.getOrElse(List()).filter(p => isPatternValid(p))

    val newFormat = options.newFormat
    // udf
    val dateFormatterUdf = udf((value: String) => {
      if (value != null) {
        val mergedPatterns = candidatePatterns ++ CANDIDATE_PATTERNS
        val foundPattern = mergedPatterns.find(pat => {
          val dateFormat = new SimpleDateFormat(pat)
          dateFormat.setLenient(false)
          Try(dateFormat.parse(value)) match {
            case Success(parsedDate) => dateFormat.format(parsedDate).equals(value)
            case Failure(_) => false
          }
        })
        foundPattern match {
          case None => (value, false)
          case Some(pattern) =>
            if (!pattern.equals(newFormat)) {
              val parsedDate = new SimpleDateFormat(pattern).parse(value)
              val formatted = new SimpleDateFormat(newFormat).format(parsedDate)
              (formatted, true)

            }
            else (value, true)
        }
      }
      else (value, false)
    }
    )

    val nomRemField = s"${UUID.randomUUID()}"
    val dfWithCalc = df.withColumn(field, dateFormatterUdf(col(field)))
      .withColumn(nomRemField, $"$field._2").withColumn(field, $"$field._1")

    // calculate num replaced values
    val numRemValues = dfWithCalc.filter(dfWithCalc(nomRemField) === true).count()

    val modifiedDf = dfWithCalc.drop(nomRemField)
    val totalRows = df.select(field).count()

    RemediationInfo(Level.INFO,
      s"Adapting $field to date pattern -> $newFormat",
      numRemValues,
      totalRows,
      modifiedDf)
  }
}
