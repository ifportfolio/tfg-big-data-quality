import { Component, OnInit, SimpleChanges, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material';

/**
 * Sub component for the dashboard.
 * It shows the alerts associated to the rules in a table.
 */
@Component({
  selector: 'app-rule-logs-pane',
  templateUrl: './rule-logs-pane.component.html',
  styleUrls: ['./rule-logs-pane.component.css']
})
export class RuleLogsPaneComponent implements OnInit {

  /** 
   * The column headers of the table 
   */
  displayedColumns: string[];
  /**
   * Logs of rule type
   */
  ruleLogs:Array<any>;

  /**
   * DataSource ofr the table
   */
  dataSource

  /**
   * Raw list of all the logs received from the dashboard
   */
  @Input() logs: Array<any>;


  /**
   * Initializes the displayed columns and the ruleLogs list.
   */
  ngOnInit() {
    this.displayedColumns = ['timestamp', 'id', 'level', 'type', 'ruleType', 'matchedTotal', 'info'];
    this.ruleLogs = new Array<any>();
  }
  /**
   * Refresh the table contents
   * @param changes Changes in the @input elements.
   */
  ngOnChanges(changes: SimpleChanges) {
    console.log(changes)
    this.logs = changes.logs.currentValue
    console.log(this.logs)
    this.ruleLogs = this.logs.filter(l => l.logType.name.toLocaleLowerCase() === 'rule')
    this.dataSource = new MatTableDataSource(this.ruleLogs)
    console.log(this.ruleLogs)
    console.log(this.dataSource)
  }

}
