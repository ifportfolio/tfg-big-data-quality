package es.uniovi.uo237525.dataquality.reader.sources

import es.uniovi.uo237525.dataquality._
import es.uniovi.uo237525.dataquality.LogConstants.{Level, LogType}
import es.uniovi.uo237525.dataquality.logmanager.LogManager
import es.uniovi.uo237525.dataquality.parser.models.InputParams
import es.uniovi.uo237525.dataquality.utils.SparkUtils

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.types.StructType

/**
  * The reader implementation that accepts CSV files.
  *
  * @param inputParams the params that will be used by Spark
  */
case class CSVReader(inputParams: InputParams) extends Reader {

  /**
    * Read the data from the source and transforms it to the dataframe format.
    *
    * @return the resulting dataframe
    */
  override def createDF: DataFrame = {

    val definedParams = inputParams.getOptionsByHeader(InputTypes.CSV)

    definedParams.foreach(param => {
      LogManager.addGenericLog(Level.DEBUG, LogType.INPUT, s"Added param: ${param._1} -> ${param._2}")
    })

    val session = SparkUtils.getSparkSession(appName = s"$SPARK_JOB_DEFAULT_NAME-${OutputTypes.CSV}-reader")

    inputParams.csv_schema match {
      case Some(ddl: String) =>
        LogManager.addGenericLog(Level.DEBUG, LogType.INPUT, "Using DDL defined schema to create dataframe from CSV")
        session.read.schema(StructType.fromDDL(ddl)).options(definedParams).csv()
      case None =>
        session.read.options(definedParams).csv()
    }

  }
}
