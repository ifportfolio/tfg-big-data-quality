package es.uniovi.uo237525.dataquality.utils

import java.util.{Date, Properties}
import javax.mail._
import javax.mail.internet._

/**
  * Utils class for Mail related operations
  */
object MailService {

  /**
    * Get a new Session object.
    *
    * @param properties  Properties object that hold relevant properties.
    *                    It is expected that the client supplies values for the properties listed in Appendix A
    *                    of the JavaMail spec (particularly mail.store.protocol, mail.transport.protocol, mail.host,
    *                    mail.user, and mail.from) as the defaults are unlikely to work in all cases.
    * @return a new Session object with the set properties.
    */
  def createSession(properties: Map[String, String]): Session = {
    val msgProperties = new Properties()

    properties.foreach(entry => {
      msgProperties.put(entry._1, entry._2)
      //      msgProperties.put(s"mail.${entry._1}", entry._2)
    })

    Session.getInstance(msgProperties)
  }

  /**
    *
    * Default constructor. An empty message object is created
    * The headers field is set to an empty InternetHeaders object.
    * The flags field is set to an empty Flags object.
    * The modified flag is set to true.
    *
    * @param session     Session object for this message
    * @param content     the content object
    * @param contentType Mime type of the object
    * @throws MessagingException
    * @return
    */
  def createMimeMessage(session: Session, content: String, contentType: String): MimeMessage = {
    val msg = new MimeMessage(session)
    msg.setContent(content, contentType)
    msg
  }

  /**
    * Send the Message to the specified list of addresses.
    * Also, if any of the addresses is invalid, a SendFailedException is thrown.
    * Whether or not the message is still sent succesfully to any valid addresses depends on the Transport implementation.
    *
    * @param session
    * @param protocol Get a Transport object that implements the specified protocol.
    * @param message  the message to be sent
    * @param subject  subject of the message
    * @param to       list addresses to send this message toseparated by commas
    * @param cc       list of adresses to send this message as copy separated by commas
    * @param cco      list of adresses to send this message as hidden copy separated by commas
    * @throws NoSuchProviderException - If provider for the given protocol is not found.
    * @throws AddressException        - if the attempt to parse the addresses String fails
    * @throws IllegalWriteException   - if the underlying implementation does not support modification of existing values
    * @throws IllegalStateException   - if this message is obtained from a READ_ONLY folder.
    * @throws SendFailedException     - if the send failed because of invalid addresses.
    * @throws MessagingException      - if the connection is dead or not in the connected state
    *
    */
  def sendMimeMessage(session: Session, protocol: String, message: MimeMessage,
                      subject: String, to: String, cc: Option[String] = None, cco: Option[String] = None,
                      auth: Boolean = false, user: String = "", password: String = ""): Unit = {
    val transport = session.getTransport(protocol)
    if (auth) {
      transport.connect(user, password)
    }
    else transport.connect()
    message.setSubject(subject)
    if (cc.isDefined) message.setRecipients(Message.RecipientType.CC, cc.get)
    if (cco.isDefined) message.setRecipients(Message.RecipientType.BCC, cco.get)
    message.setRecipients(Message.RecipientType.TO, to)
    message.setSentDate(new Date())
    transport.sendMessage(message, message.getAllRecipients)
    transport.close()
  }

}
