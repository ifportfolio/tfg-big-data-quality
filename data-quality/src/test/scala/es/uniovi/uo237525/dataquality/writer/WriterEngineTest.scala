package es.uniovi.uo237525.dataquality.writer

import es.uniovi.uo237525.dataquality.storage.writers.{CSVWriter, HiveWriter}
import es.uniovi.uo237525.dataquality.OutputTypes
import es.uniovi.uo237525.dataquality.parser.models.OutputParams
import es.uniovi.uo237525.dataquality.storage.WriterEngine
import es.uniovi.uo237525.dataquality.storage.writers.{CSVWriter, HiveWriter}
import org.scalatest.FunSuite
import es.uniovi.uo237525.dataquality.storage.writers.{CSVWriter, HiveWriter}


class WriterEngineTest extends FunSuite {

  test("test creating hive") {
    val typeSource = OutputTypes.HIVE
    val we = new WriterEngine()
    assert(we.createWriterFromOutParams(Some(OutputParams(typeSource))).get.isInstanceOf[HiveWriter])

    val seUp = new WriterEngine()
    assert(seUp.createWriterFromOutParams(Some(OutputParams("hIvE"))).get.isInstanceOf[HiveWriter])
  }

  test("test creating csv") {
    val typeSource = OutputTypes.CSV
    val we = new WriterEngine()
    assert(we.createWriterFromOutParams(Some(OutputParams(typeSource))).get.isInstanceOf[CSVWriter])

    val seUp = new WriterEngine()
    assert(seUp.createWriterFromOutParams(Some(OutputParams("cSv"))).get.isInstanceOf[CSVWriter])
  }

  test("test creating un-existing source type") {
    val da = new WriterEngine()
    assertThrows[IllegalArgumentException] {
      da.createWriterFromOutParams(Some(OutputParams("NON_EXISTING_SOURCE_TYPE")))
    }
  }

  test("test creating writer with undefined output") {
    assert(new WriterEngine().createWriterFromOutParams(None).isEmpty)
  }
}
