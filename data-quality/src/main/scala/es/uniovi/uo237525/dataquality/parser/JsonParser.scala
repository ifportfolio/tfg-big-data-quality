package es.uniovi.uo237525.dataquality.parser

import java.io.{File, FileInputStream}

import es.uniovi.uo237525.dataquality.engine.rules.evaluation.{EvDateFormat, EvFieldLength, EvNumericFormat, EvRangeValues, EvRegex, EvRequiredFields}
import es.uniovi.uo237525.dataquality.engine.rules.remediation.{FillEmptyRecords, RemDateFormat, RemNumericFormat, ReplaceOutOfRange, SliceField}
import org.apache.log4j.Logger
import org.json4s._
import org.json4s.jackson.JsonMethods._

/**
  * Representation of a parser that recognises the schema defined for the quality component
  * and parses it into a JsonWrapper instance.
  */
object JsonParser {

  val logger: Logger = Logger.getRootLogger

  /**
    * Extracts the values from the json file specified in the path.
    * The path can be specified in absolute or relative form.
    *
    * All the new implemented rules must be de decared in this function,
    * otherwise they will be not recognized from the specificatin rules file.
    *
    * @param filePath   the path where the json is located
    * @param isAbsolute flag that indicates whether the provided path is absolute or not.
    * @return a JsonWrapper object containing all the valid rules and params declared in the JSON file.
    */
  def extractJsonSchema(filePath: String, isAbsolute: Boolean = false): JsonWrapper = {
    implicit val formats: DefaultFormats = new DefaultFormats {
      override val typeHintFieldName = "type"
      override val typeHints = ShortTypeHints(
        List(classOf[EvDateFormat],
          classOf[EvRequiredFields],
          classOf[EvNumericFormat],
          classOf[EvFieldLength],
          classOf[EvRegex],
          classOf[EvRangeValues],
          // Remediation
          classOf[FillEmptyRecords],
          classOf[RemNumericFormat],
          classOf[RemDateFormat],
          classOf[SliceField],
          classOf[ReplaceOutOfRange]
        )
      )
    }
    val path = if (isAbsolute) filePath
    else new File(filePath).getCanonicalPath

    logger.warn("Reading rules from: " + path)
    val inStream = new FileInputStream(path)
    val json = parse(inStream, useBigDecimalForDouble = false)
    json.extract[JsonWrapper]
  }
}
