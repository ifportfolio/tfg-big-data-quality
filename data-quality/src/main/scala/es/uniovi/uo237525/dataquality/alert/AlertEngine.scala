package es.uniovi.uo237525.dataquality.alert

import es.uniovi.uo237525.dataquality.EXEC_UNIQUE_ID
import es.uniovi.uo237525.dataquality.LogConstants.{Level, LogType}
import es.uniovi.uo237525.dataquality.alert.senders.{EmailAlertSender, KafkaAlertSender, RedisAlertSender}
import es.uniovi.uo237525.dataquality.configuration.Configuration
import es.uniovi.uo237525.dataquality.logmanager.{Log, LogManager}
import es.uniovi.uo237525.dataquality.parser.models.{Alerts, KafkaAlert, MailAlert, RedisAlert}
import org.apache.log4j.Logger

import scala.util.{Failure, Success}

/**
  * The responsible for managing the process of sending the configured alerts.
  * It will use the provided configuration to create each alert sender.
  *
  * @param conf The configuration that will be provided to each alert sender..
  * @author Ignacio Fernandez Fernandez
  */
class AlertEngine(conf: Configuration) {

  val logger: Logger = Logger.getRootLogger

  /**
    * This method is the responsible of sending all the alerts of mail type.
    * Depending of the granularity level specified in the mail alert, the logs will be filtered.
    * In case the sending process fail for an specific alert, the alert will be dismissed but the execution will not be stopped.
    *
    * @param logs   the list of logs that want to be sent.
    * @param alerts the list of different mail alerts configured.
    *
    */
  def sendMailAlerts(logs: Iterable[Log], alerts: Array[MailAlert]): Unit = {
    logger.warn(s"sending mail alerts -> ${alerts.length}")
    LogManager.addGenericLog(Level.INFO, LogType.ALERT, s"${alerts.length} mail alerts defined.")
    alerts.foreach(al => {
      logger.warn("creating mail sender")
      val sender = EmailAlertSender(conf, al)
      val logsToSend = LogManager.getLogsByLevel(al.level, logs.toList)
      sender.sendAlert(logsToSend) match {
        case Success(_) =>
          LogManager.addGenericLog(Level.DEBUG, LogType.ALERT, s"Successfully mail alert sent to ${al.to}")
          logger.warn("Successfully sent alert")
        case Failure(exception) =>
          LogManager.addGenericLog(Level.ERROR, LogType.ALERT, s"Error while sending mail alert -> ${exception.getLocalizedMessage}")
          logger.error(s"Unable to send mail alert -> ${exception.getLocalizedMessage}")
      }
    })
  }

  /**
    * This method is the responsible of sending all the alerts of redis type.
    * Depending of the granularity level specified in the mail alert, the logs will be filtered.
    * In case the sending process fail for an specific alert, the alert will be dismissed but the execution will not be stopped.
    *
    * @param logs   the list of logs that want to be sent.
    * @param alerts the list of different redis alerts configured.
    */
  def sendRedisAlerts(logs: Iterable[Log], alerts: Array[RedisAlert]): Unit = {
    logger.warn(s"sending redis alerts -> ${alerts.length}")
    LogManager.addGenericLog(Level.INFO, LogType.ALERT, s"${alerts.length} redis alerts defined.")
    alerts.foreach(al => {
      logger.warn("Creating redis alert")
      val sender = RedisAlertSender(conf, al)
      val logsToSend = LogManager.getLogsByLevel(al.level, logs.toList)
      sender.sendAlert(logsToSend) match {
        case Success(_) =>
          LogManager.addGenericLog(Level.DEBUG, LogType.ALERT, s"Redis LPUSH of Execution ID with key -> ${al.key}")
          LogManager.addGenericLog(Level.DEBUG, LogType.ALERT, s"Redis SET of alert into key -> $EXEC_UNIQUE_ID")
          logger.warn("Successfully saved redis alert")
        case Failure(exception) =>
          LogManager.addGenericLog(Level.ERROR, LogType.ALERT, s"Error while sending Redis alert -> ${exception.getLocalizedMessage}")
          logger.error(s"Error while sending Redis alert -> ${exception.getLocalizedMessage}")
      }
    })
  }

  /**
    * This method is the responsible of sending all the alerts of kafka type.
    * Depending of the granularity level specified in the kafka alert, the logs will be filtered.
    * In case the sending process fail for an specific alert, the alert will be dismissed but the execution will not be stopped.
    *
    * @param logs   the list of logs that want to be sent.
    * @param alerts the list of different mail alerts configured.
    */
  def sendKafkaAlerts(logs: Iterable[Log], alerts: Array[KafkaAlert]): Unit = {
    logger.warn(s"sending kafka alerts -> ${alerts.length}")
    LogManager.addGenericLog(Level.INFO, LogType.ALERT, s"${alerts.length} kafka alerts defined.")
    alerts.foreach(al => {
      logger.warn("Creating kafka alert")
      val sender = KafkaAlertSender(conf, al)
      val logsToSend = LogManager.getLogsByLevel(al.level, logs.toList)
      sender.sendAlert(logsToSend) match {
        case Success(_) =>
          LogManager.addGenericLog(Level.DEBUG, LogType.ALERT, s"Kafka alert successfully sent to topic ${al.topic}")
          logger.warn("Successfully sent kafka alert")
        case Failure(exception) =>
          LogManager.addGenericLog(Level.ERROR, LogType.ALERT, s"Error while sending Kafka alert -> ${exception.getLocalizedMessage}")
          logger.error(s"Error while sending Kafka alert -> ${exception.getLocalizedMessage}")
      }
    })
  }

  /**
    * Sends all the logs trough the different enabled senders.
    * The logs will be filtered depending on each sender associated granularity level.
    *
    * @param logs list containing all the logs that are going to be sent by each sender after it is filtered.
    * @return an iterable containing the alerts and the logs sent by each of them.
    */
  def sendAllAlerts(logs: Iterable[Log], alerts: Option[Alerts]): Unit = {
    if (alerts.isDefined) {
      sendMailAlerts(logs, alerts.get.mail)
      sendKafkaAlerts(logs, alerts.get.kafka)
      sendRedisAlerts(logs, alerts.get.redis)
      //      val restSent = RestAlertSender().sendAlerts(logs, alerts.get.kafka.map(_.asInstanceOf[Alert]))
    }
    else {
      logger.warn("No alerts defined.")
    }
  }
}
