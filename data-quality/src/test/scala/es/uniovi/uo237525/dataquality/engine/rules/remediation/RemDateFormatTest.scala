package es.uniovi.uo237525.dataquality.engine.rules.remediation

import java.text.SimpleDateFormat

import es.uniovi.uo237525.dataquality.SparkSessionTestWrapper

import org.scalatest.FunSuite
import scala.util.{Failure, Success, Try}

import org.apache.spark.sql.DataFrame

class RemDateFormatTest extends FunSuite with SparkSessionTestWrapper {

  import spark.implicits._

  val txtPath: String = getClass.getResource("/txt/date-format-dataset.txt").getPath
  private val someDf: DataFrame = spark.read.option("delimiter", "|").option("header", value = true).csv(txtPath).cache()

  /** REMEDIATION PRE-CONDITIONS TEST */
  test("test REMEDIATION PRE-CONDITIONS: apply on un-existing field") {
    val rule = RemDateFormat(field = "c1", options = RemDateFormatOptions("mm-dd-yyyy"))
    val someDF: DataFrame = Seq(1, 2, 3).toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }
  test("test REMEDIATION PRE-CONDITIONS: non-string column") {
    val rule = RemDateFormat(field = "nums", options = RemDateFormatOptions("mm-dd-yyyy"))
    val someDF: DataFrame = Seq(1, 2, 3).toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }
  test("test REMEDIATION PRE-CONDITIONS: invalid newFormat") {
    val rule = RemDateFormat(field = "nums", options = RemDateFormatOptions("non-valid"))
    val someDF: DataFrame = Seq("1", "2", "3").toDF("nums")
    assert(rule.checkPreconditions(someDF).isDefined)
  }

  /** REMEDIATION APPLICATION TEST */
  test("test REMEDIATION APPLICATION: generic") {
    val rule = RemDateFormat(field = "dd-MM-yyyy", options = RemDateFormatOptions("yyyy-MM-dd"))

    val results = rule.apply(someDf).get
    val numMatchingRows = results.modifiedDF.select(rule.field).collect().map(row => row.getString(0)).count(date => {
      val dateFormat = new SimpleDateFormat(rule.options.newFormat)
      dateFormat.setLenient(false)
      Try(dateFormat.parse(date)).isSuccess
    })
    assert(results.matched == numMatchingRows)
  }

  test("test REMEDIATION APPLICATION: testing dates") {
    val rule = RemDateFormat(field = "dates", options = RemDateFormatOptions("yyyy-MM-dd'T'HH:mm:ssZ"))

    val datesDf = Seq("2019-01-22T19:17:37719337",
      "2019-03-27 11:26:45.00",
      "2019-03-27 11:26",
      "2019-03-27",
      "11",
      "11:35"
    ).toDF("dates")
    val results = rule.apply(datesDf).get
    val resultsRemedied = results.matched
    val actualRemedied = results.modifiedDF.collect().map(row => row.getString(0)).count(date => {
      val dateFormat = new SimpleDateFormat(rule.options.newFormat)
      dateFormat.setLenient(false)
      Try(dateFormat.parse(date)) match {
        case Success(parsed) => dateFormat.format(parsed).equals(date)
        case Failure(_) => false
      }
    })
    assert(resultsRemedied == actualRemedied)
  }

  //  test("test Remediation APPLY: application dates") {
  //    val rule = RemDateFormat(field = "dates",
  //      options = RemDateFormatOptions("yyyy-MM-dd HH:mm:ss.SSS"),
  //      remediation = Some(RemDateFormatOptions("dd/MM/yyyy HH:mm")))
  //    val someDF = Seq("2019-03-28 09:21:03.007",
  //      "2019-03-28 09:21:03.007",
  //      "2019-03-28 09:21:03.007").toDF("dates")
  //    rule.apply(someDF).get
  //  }
}
