package es.uniovi.uo237525.dataquality.writer

import java.io.File
import java.nio.file.{Files, Paths}
import java.sql.{Date, Timestamp}
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.UUID

import es.uniovi.uo237525.dataquality.SparkSessionTestWrapper
import es.uniovi.uo237525.dataquality._
import es.uniovi.uo237525.dataquality.parser.models.OutputParams
import es.uniovi.uo237525.dataquality.storage.writers.CSVWriter
import org.apache.commons.io.FileUtils
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FunSuite}

import scala.io.Source
import scala.reflect.io.Path
import scala.util.Try
import scala.util.matching.Regex
import org.apache.spark.SparkException
import org.apache.spark.sql.{AnalysisException, DataFrame, Row}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import es.uniovi.uo237525.dataquality.SparkSessionTestWrapper


class CSVWriterTest extends FunSuite with SparkSessionTestWrapper with BeforeAndAfterEach with BeforeAndAfterAll {

  import spark.implicits._

  private val DEFAULT_DIR_LOCATION: String = "./"
  private val DEFAULT_DIR_NAME: UUID = UUID.randomUUID()
  private val DEFAULT_PATH: String = DEFAULT_DIR_LOCATION + DEFAULT_DIR_NAME

  private val ts = Timestamp.from(Instant.now())
  private val date = new Date(System.currentTimeMillis())
  private val map = Map("example1" -> "ex1", "example2" -> "ex2")
  private val mulArray = Array(Array("array of array1"), Array("array of array2"))
  private val seq = Seq(
    Row(1, Long.MaxValue, null, false, 0.5, ts, Array("val1", "val2"), mulArray, date, map),
    Row(2, Long.MaxValue, "val,ue", null, null, ts, Array("val1", "val2"), mulArray, null, map),
    Row(3, null, null, true, 0.004, ts, null, null, date, null),
    Row(null, null, null, null, null, null, null, null, null, null))

  val someSchema = StructType(
    StructField("id", IntegerType) ::
      StructField("long", LongType) ::
      StructField("word", StringType) ::
      StructField("bool", BooleanType) ::
      StructField("double", DoubleType) ::
      StructField("timestamp", TimestampType) ::
      StructField("array", ArrayType(StringType)) ::
      StructField("multiple", ArrayType(ArrayType(StringType))) ::
      StructField("date", DateType) ::
      StructField("map", MapType(StringType, StringType)) :: Nil)

  val someDf: DataFrame = spark.createDataFrame(
    spark.sparkContext.parallelize(seq),
    StructType(someSchema)
  ).cache()

  private def getFirstGenCSVLines: List[String] = {
    val csvGenFile = Source.fromFile(
      new File(Path(DEFAULT_PATH).path).listFiles.filter(_.getName.endsWith(".csv")).head
    )
    val lines = csvGenFile.getLines().toList
    csvGenFile.close()
    lines
  }

  private def deleteDefaultDir: Boolean = {
    if (Files.exists(Paths.get(DEFAULT_PATH))) {
      Try(FileUtils.forceDelete(new File(DEFAULT_PATH))).isSuccess
    }
    else true
  }

  override def beforeEach() {
    deleteDefaultDir
  }

  override def afterAll(): Unit = {
    deleteDefaultDir
  }

  /** TEST PARAMS */
  test("PATH: test valid path exports to desired location") {
    val w = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH)
    ))
    assert(!Files.exists(Paths.get(DEFAULT_PATH)))
    w.overwrite(someDf)
    assert(Files.exists(Paths.get(DEFAULT_PATH)))
  }
  test("PATH: test path not specified") {
    val w = CSVWriter(OutputParams(
      InputTypes.CSV
    ))
    assertThrows[IllegalArgumentException] {
      w.overwrite(someDf).get
    }
  }
  test("PATH: test on-use path export") {
    val w = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH)
    ))
    assert(w.overwrite(someDf).isSuccess)

    assertThrows[AnalysisException] {
      if (Files.exists(Paths.get(DEFAULT_PATH))) {
        w.errorIfExists(someDf).get
      }
      else {
        w.errorIfExists(someDf).get
      }
    }
  }

  test("SEP: test single char sep param") {
    val delim = "|"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_sep = Option(delim)
    ))
    writer.overwrite(someDf)
    val firstLine = getFirstGenCSVLines.head.split(delim.toCharArray.head)
    assert(firstLine.length === seq.head.toSeq.length)
  }
  test("SEP: test multiple char sep param not allowed") {
    val sep = "||"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_sep = Option(sep)
    ))
    assertThrows[IllegalArgumentException] {
      writer.overwrite(someDf).get
    }
  }
  test("SEP: test default sep is comma") {
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH)
    ))
    writer.overwrite(someDf).get
    // split by commas, ignoring commas between quotes
    val firstLine = getFirstGenCSVLines.head.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)")
    assert(firstLine.length === seq.head.toSeq.length)
  }

  test("QUOTE: test quote") {
    val quote = "\'"
    val sep = "|"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_sep = Option(sep),
      csv_quote = Option(quote),
      csv_quoteAll = Option(true)
    ))
    writer.overwrite(someDf).get

    val regex = (quote + "([^;]*)" + quote).r
    val arr = getFirstGenCSVLines.head.split(sep.toCharArray.head)
    assert(arr.forall(regex.findFirstIn(_).isDefined))
  }
  test("QUOTE: test default quote is \"") {
    val defQuote = "\""
    val sep = "|"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_sep = Option(sep),
      csv_quoteAll = Option(true)
    ))
    writer.overwrite(someDf).get

    val regex: Regex = (defQuote + "([^;]*)" + defQuote).r
    val arr = getFirstGenCSVLines.head.split(sep.toCharArray.head)
    assert(arr.forall(regex.findFirstIn(_).isDefined))
  }
  test("QUOTE: test multiple quote param not allowed") {
    val quote = "\'\'\'"
    val sep = "|"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_sep = Option(sep),
      csv_quote = Option(quote),
      csv_quoteAll = Option(true)
    ))

    assertThrows[RuntimeException] {
      writer.overwrite(someDf).get
    }
  }

  test("ESCAPE: test escape char") {
    val dfEscape = Seq(("textwith\"\"quotes", "textwith\"quotes2", "textWithoutQuotes")).toDF()
    val quote = "\""
    val escape = "*"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_escape = Option(escape),
      csv_quoteAll = Option(true)
    ))
    writer.overwrite(dfEscape)

    val arr = getFirstGenCSVLines.head.split(',')
    // every time the element contains the quotes char, it is escaped with the escape char
    arr.forall(el => {
      if (el.contains(quote)) {
        el.contains(escape + quote)
      }
      else true
    })
  }
  test("ESCAPE: test default escape char is \\") {
    val dfEscape = Seq(("textwith\"\"quotes", "textwith\"quotes2", "textWithoutQuotes")).toDF()
    val quote = "\""
    val defaultEscape = "\\"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_quoteAll = Option(true)
    ))
    writer.overwrite(dfEscape)

    val arr = getFirstGenCSVLines.head.split(',')
    // every time the element contains the quotes char, it is escaped with the escape char
    arr.forall(el => {
      if (el.contains(quote)) {
        el.contains(defaultEscape + quote)
      }
      else true
    })
  }
  test("ESCAPE: test multiple escape char param not allowed") {
    val escape = "***"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_escape = Option(escape),
      csv_quoteAll = Option(true)
    ))

    assertThrows[RuntimeException] {
      writer.overwrite(someDf).get
    }
  }

  test("charToEscapeQuoteEscaping: test param") {
    val dfEscape = Seq(("textwith\"\"quot\\esAnd\\\\", "textwith\"quotes2&\\", "textWithoutQuotes")).toDF()
    val escape = '\\'
    val defaultQuote = '\"'
    val charToEscapeQuoteEscaping = '^'
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_escape = Option(escape.toString),
      csv_quoteAll = Option(true),
      csv_charToEscapeQuoteEscaping = Option(charToEscapeQuoteEscaping.toString)
    ))
    writer.overwrite(dfEscape)
    val arr = getFirstGenCSVLines.head.split(',')
    assert(arr.forall(el => {
      if (el.contains(escape)) { // if it contains the escape char
        val chars = el.toCharArray
        chars.view.zipWithIndex.forall { d =>
          val ch = d._1
          val index = d._2
          if (ch == escape) { // if it is the escape char
            // if last char or next char is not a quote char
            if (index == el.length - 1 || chars(index + 1) != defaultQuote) {
              chars(index - 1).equals(charToEscapeQuoteEscaping)
            }
            else true
          }
          else true
        }
      }
      else true
    }))
  }
  test("charToEscapeQuoteEscaping: test defaul value is escape") {
    val dfEscape = Seq(("\\textwith\"\"quot\\esAnd\\\\", "textwith\"quotes2&\\", "textWithoutQuotes")).toDF()
    val escape = '\\'
    val defaultQuote = '\"'
    val defaultEscapeToEscape = '\\'
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_escape = Option(escape.toString),
      csv_quoteAll = Option(true)
    ))
    writer.overwrite(dfEscape)
    val arr = getFirstGenCSVLines.head.split(',')
    assert(arr.forall(el => {
      if (el.contains(escape)) { // if it contains the escape char
        val chars = el.toCharArray
        chars.view.zipWithIndex.forall { d =>
          val ch = d._1
          val index = d._2
          if (ch == escape) { // if it is the escape char
            // if last char
            if (index == el.length - 1) chars(index - 1).equals(defaultEscapeToEscape)
            else if (index == 0 && chars(index + 1) != defaultQuote) chars(index + 1).equals(defaultEscapeToEscape)
            else chars(index + 1) == defaultQuote ||
              chars(index + 1) == defaultEscapeToEscape ||
              chars(index - 1) == defaultEscapeToEscape
          }
          else true
        }
      }
      else true
    }))
  }

  test("escapeQuotes: test correct behaviour when deactivated") {
    val dfEscape = Seq(("textwith\"\"quotes", "textwith\"quotes", "textWithoutQuotes")).toDF()
    val escape = '\\'
    val defaultQuote = '\"'
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_escapeQuotes = Option(false)
    ))
    writer.overwrite(dfEscape)
    val arr = getFirstGenCSVLines.head.split(',')
    assert(
      arr.forall(el => {
        if (el.contains(defaultQuote)) !el.contains(escape)
        else true
      })
    )
  }

  test("escapeQuotes: test default behaviour (activated") {
    val dfEscape = Seq(("textwith\"\"quotes", "textwith\"quotes", "textWithoutQuotes")).toDF()
    val escape = '\\'
    val defaultQuote = '\"'
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH)
    ))
    writer.overwrite(dfEscape).get
    val arr = getFirstGenCSVLines.head.split(',')
    assert(
      arr.forall(el => {
        if (el.contains(defaultQuote)) el.contains(escape)
        else true
      })
    )
  }

  test("quoteAll: test behaviour when deactivated (default)") {
    val df = Seq(("textw\"\"quotes", "text", "text")).toDF()
    val defQuot = '\"'
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH)
    ))
    writer.overwrite(df).get

    val arr = getFirstGenCSVLines.head.split(',')
    assert(arr.forall(el => {
      if (el.contains("\"")) el.charAt(0) == defQuot && el.charAt(el.length - 1) == defQuot
      else true
    }))
  }

  test("quoteAll: test behaviour when activated") {
    val df = Seq(("textw\"\"quotes", "text", "text")).toDF()
    val defQuot = '\"'
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_quoteAll = Option(true)
    ))
    writer.overwrite(df).get

    val arr = getFirstGenCSVLines.head.split(',')
    assert(arr.forall(el => el.charAt(0) == defQuot && el.charAt(el.length - 1) == defQuot))
  }

  test("HEADER: test header param true") {
    val writeWithHeader = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_header = Option(true)
    ))
    writeWithHeader.overwrite(someDf).get

    val firstLineWithHeader = getFirstGenCSVLines.head.split(',')
    assert(firstLineWithHeader.deep == someSchema.fieldNames.deep)
  }

  test("HEADER: test header param false") {
    val writeWithoutHeader = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_header = Option(false)
    ))
    writeWithoutHeader.overwrite(someDf).get

    val firstLineWithoutHeader = getFirstGenCSVLines.head.split(',')
    assert(firstLineWithoutHeader.deep != someSchema.fieldNames.deep)
  }

  test("nullValue: test usual str as null value") {
    val nullVal = "defNullVal"
    val sep = '|'
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_sep = Option(sep.toString),
      csv_nullValue = Option(nullVal)
    ))

    writer.overwrite(someDf).get

    val arr = getFirstGenCSVLines

    someDf.collect().view.zipWithIndex.foreach(el => {
      assert(el._1.toSeq.count(_ == null) == arr(el._2).split(sep).count(_.contains(nullVal)))
    })
  }

  test("nullValue: test empty null value (default)") {
    val sep = '|'
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_sep = Option(sep.toString)
    ))
    writer.overwrite(someDf).get

    val arr = getFirstGenCSVLines.map(_.split(sep))

    someDf.collect().view.zipWithIndex.foreach(el => {
      assert(el._1.toSeq.count(_ == null) == arr(el._2).count(_.equals("\"\"")))
    })
  }

  test("dateFormat: valid format") {
    val format = "dd/MM/yyyy"
    val dateColName = "date"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_dateFormat = Option(format)
    ))

    writer.overwrite(someDf.select(dateColName).filter(col(dateColName).isNotNull)).get

    val content = getFirstGenCSVLines
    assert(
      content.forall(el => {
        Try(new SimpleDateFormat(format).parse(el)).isSuccess
      })
    )
  }

  test("dateFormat: invalid format") {
    val format = "l/RR/xxxx"
    val dateColName = "date"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_dateFormat = Option(format)
    ))

    assertThrows[IllegalArgumentException] {
      writer.overwrite(someDf.select(dateColName).filter(col(dateColName).isNotNull)).get
    }
  }

  test("dateFormat: default format is yyyy-MM-dd") {
    val defaultSparkFormat = "yyyy-MM-dd"
    val dateColName = "date"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH)
    ))

    writer.overwrite(someDf.select(dateColName).filter(col(dateColName).isNotNull)).get
    val content = getFirstGenCSVLines
    assert(
      content.forall(el => {
        Try(new SimpleDateFormat(defaultSparkFormat).parse(el)).isSuccess
      })
    )
  }

  test("dateFormat: default spark format applied when null is passed") {
    val format = null
    val defaultSparkFormat = "yyyy-MM-dd"
    val dateColName = "date"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_dateFormat = Option(format)
    ))

    writer.overwrite(someDf.select(dateColName).filter(col(dateColName).isNotNull)).get
    val content = getFirstGenCSVLines
    assert(
      content.forall(el => {
        Try(new SimpleDateFormat(defaultSparkFormat).parse(el)).isSuccess
      })
    )
  }

  test("timestampFormat: test valid format") {
    val format = "yyyy/MM/dd'T'H-s-mm.SSSXXX"
    val colName = "timestamp"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_timestampFormat = Option(format)
    ))

    writer.overwrite(someDf.select(colName).filter(col(colName).isNotNull)).get
    val content = getFirstGenCSVLines
    assert(
      content.forall(el => {
        Try(new SimpleDateFormat(format).parse(el)).isSuccess
      })
    )
  }

  test("timestampFormat: invalid format") {
    val format = "l/RR/xxxx'T'H-s-mm.SSSXXX"
    val colName = "timestamp"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_timestampFormat = Option(format)
    ))

    assertThrows[IllegalArgumentException] {
      writer.overwrite(someDf.select(colName).filter(col(colName).isNotNull)).get
    }
  }

  test("timestampFormat: default format is yyyy-MM-dd'T'HH:mm:ss.SSSXXX") {
    val defaultSparkFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX"
    val colName = "timestamp"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH)
    ))

    writer.overwrite(someDf.select(colName).filter(col(colName).isNotNull)).get
    val content = getFirstGenCSVLines
    assert(
      content.forall(el => {
        Try(new SimpleDateFormat(defaultSparkFormat).parse(el)).isSuccess
      })
    )
  }

  test("timestampFormat: default spark format applied when null is passed") {
    val format = null
    val defaultSparkFormat = "yyyy-MM-dd"
    val colName = "timestamp"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_timestampFormat = Option(format)
    ))

    writer.overwrite(someDf.select(colName).filter(col(colName).isNotNull)).get
    val content = getFirstGenCSVLines
    assert(
      content.forall(el => {
        Try(new SimpleDateFormat(defaultSparkFormat).parse(el)).isSuccess
      })
    )
  }

  test("ignoreLeadingWhiteSpace: test true (default) value") {
    val df = Seq(("      textWitSpace", "   text  ", " text")).toDF()
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH)
    ))
    writer.overwrite(df).get
    getFirstGenCSVLines.map(_.split(',')).foreach(_.foreach(term => assert(term.charAt(0) != ' ')))
  }

  test("ignoreLeadingWhiteSpace: test false value") {
    val df = Seq(("      textWitSpace", "   text  ", " text")).toDF()
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_ignoreLeadingWhiteSpace = Option(false)
    ))
    writer.overwrite(df).get
    getFirstGenCSVLines.map(_.split(',')).foreach(_.foreach(term => assert(term.charAt(0) == ' ')))
  }

  test("ignoreTrailingWhiteSpace: test true (default) value") {
    val df = Seq(("textWitSpace  ", "   text  ", "text ", "t ")).toDF()
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH)
    ))
    writer.overwrite(df).get

    getFirstGenCSVLines.map(_.split(',')).foreach(_.foreach(term => assert(term.last != ' ')))
  }

  test("ignoreTrailingWhiteSpace: test false value") {
    val df = Seq(("textWitSpace  ", "   text  ", "text ", "t ")).toDF()
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_ignoreTrailingWhiteSpace = Option(false)
    ))
    writer.overwrite(df).get

    getFirstGenCSVLines.map(_.split(',')).foreach(_.foreach(term => assert(term.last == ' ')))
  }

  test("compression: \"none\" value") {
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_compression = Option("none")
    ))

    writer.overwrite(someDf).get
    assert(new File(Path(DEFAULT_PATH).path).listFiles.count(_.getName.endsWith(".csv")) > 0)
  }

  test("compression: default value") {
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH)
    ))

    writer.overwrite(someDf).get
    assert(new File(Path(DEFAULT_PATH).path).listFiles.count(_.getName.endsWith(".csv")) > 0)
  }

  ignore("compression: \"bzip2\" value") {
    val c = "bzip2"
    val extension = ".bz2"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_compression = Option(c)
    ))
    try {
      writer.overwrite(someDf).get
      assert(new File(Path(DEFAULT_PATH).path).listFiles.count(_.getName.endsWith(extension)) > 0)
    }
    catch {
      case se: SparkException => // native library not available
      case th: Throwable => throw th
    }
  }

  test("compression: un-existing library value") {
    val c = "non-existing"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_compression = Option(c)
    ))
    assertThrows[IllegalArgumentException] {
      writer.overwrite(someDf).get
    }
  }

  ignore("compression: \"gzip\" value") {
    val c = "gzip"
    val extension = ".gz"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_compression = Option(c)
    ))
    try {
      writer.overwrite(someDf).get
      assert(new File(Path(DEFAULT_PATH).path).listFiles.count(_.getName.endsWith(extension)) > 0)
    }
    catch {
      case se: SparkException => // native library not available
      case th: Throwable => throw th
    }
  }

  ignore("compression: \"lz4\" value") {
    // (none, bzip2, gzip, lz4, snappy and deflate).
    val c = "lz4"
    val extension = ".lz4"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_compression = Option(c)
    ))
    try {
      writer.overwrite(someDf).get
      assert(new File(Path(DEFAULT_PATH).path).listFiles.count(_.getName.endsWith(extension)) > 0)

    }
    catch {
      case se: SparkException => // native library not available
      case th: Throwable => throw th
    }
  }

  ignore("compression: \"snappy\" value") {
    val c = "snappy"
    val extension = ".sz"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_compression = Option(c)
    ))
    try {
      writer.overwrite(someDf).get
      assert(new File(Path(DEFAULT_PATH).path).listFiles.count(_.getName.endsWith(extension)) > 0)
    }
    catch {
      case se: SparkException => // native library not available
      case th: Throwable => throw th
    }
  }

  ignore("compression: \"deflate\" value") {
    val c = "deflate"
    val extension = ".deflate"
    val writer = CSVWriter(OutputParams(
      InputTypes.CSV,
      csv_path = Option(DEFAULT_PATH),
      csv_compression = Option(c)
    ))
    try {
      writer.overwrite(someDf).get
      assert(new File(Path(DEFAULT_PATH).path).listFiles.count(_.getName.endsWith(extension)) > 0)
    }
    catch {
      case se: SparkException => // native library not available
      case th: Throwable => throw th
    }
  }

  /** TEST OVERWRITE BEHAVIOUR */
  // TODO

  /** TEST APPEND BEHAVIOUR */
  // TODO

  /** TEST ERRORIFEXISTS BEHAVIOUR */
  // TODO

  /** TEST IGNORE BEHAVIOUR */
  // TODO
}
