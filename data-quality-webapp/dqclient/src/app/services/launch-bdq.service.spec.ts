import { TestBed } from '@angular/core/testing';

import { LaunchBdqService } from './launch-bdq.service';

describe('LaunchBdqService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LaunchBdqService = TestBed.get(LaunchBdqService);
    expect(service).toBeTruthy();
  });
});
