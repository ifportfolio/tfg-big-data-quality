package es.uniovi.uo237525.dataquality.alert.senders

import es.uniovi.uo237525.dataquality.LogConstants._
import es.uniovi.uo237525.dataquality.configuration.Configuration
import es.uniovi.uo237525.dataquality.logmanager.Log
import es.uniovi.uo237525.dataquality.logmanager.logs.GenericLog
import es.uniovi.uo237525.dataquality.parser.models.MailAlert
import org.scalatest.{BeforeAndAfterAll, FunSuite}

import scala.util.{Failure, Success}

class EmailAlertSenderTest extends FunSuite with BeforeAndAfterAll {

  private val arrWithRules = Array("-r", "")
  val logs: List[Log] = LogType.values.map(t => GenericLog(Level.INFO, "info", t)).toList


  override def beforeAll(): Unit = {
    // testing email connection
    val mailSender = getSender(Seq("-DSMTP_HOST=localhost", "-DSMTP_PORT=25"), MailAlert(true, Level.DEBUG.toString, "mailto", None, None, "subject"))
    withClue("The connection with the email server could not be made, so no mail related test can be performed.") {
      mailSender.sendAlert(logs) match {
        case Success(_) => succeed
        case Failure(ex) => fail(ex)
      }
    }

  }

  /** CREATE HTML CONTENT */

  private def getSender(properties: Seq[String], al: MailAlert): EmailAlertSender = {
    EmailAlertSender(Configuration(arrWithRules ++ properties), al)
  }

  /** TEST SEND ALERTS WITH CORRECT HOST PARAMS */
  test("correct logs") {
    val mailSender = getSender(Seq("-DSMTP_HOST=localhost", "-DSMTP_PORT=25"), MailAlert(true, Level.DEBUG.toString, "mailto", None, None, "subject"))
    assert(mailSender.sendAlert(logs).isSuccess)
  }

  test("no logs") {
    val mailSender = getSender(Seq("-DSMTP_HOST=localhost", "-DSMTP_PORT=25"), MailAlert(true, Level.DEBUG.toString, "mailto", None, None, "subject"))
    assert(mailSender.sendAlert(List()).isSuccess)
  }

  test("null logs") {
    val mailSender = getSender(Seq("-DSMTP_HOST=localhost", "-DSMTP_PORT=25"), MailAlert(true, Level.DEBUG.toString, "mailto", None, None, "subject"))
    assert(mailSender.sendAlert(null).isFailure)
  }

  test("invalid smtp options") {
    val mailSender = getSender(Seq("-DSMTP_HOST=localhost", "-DSMTP_PORT=19"), MailAlert(true, Level.DEBUG.toString, "mailto", None, None, "subject"))
    assert(mailSender.sendAlert(null).isFailure)
  }
}
