package es.uniovi.uo237525.dataquality.storage.writers

import es.uniovi.uo237525.dataquality.HiveConstants.{HIVE_METASTORE_URIS_HEADER, SPARK_WAREHOUSE_DIR_HEADER}
import es.uniovi.uo237525.dataquality.LogConstants.{Level, LogType}
import es.uniovi.uo237525.dataquality.logmanager.LogManager
import es.uniovi.uo237525.dataquality.parser.models.OutputParams
import es.uniovi.uo237525.dataquality.storage.Writer
import es.uniovi.uo237525.dataquality.utils.SparkUtils
import es.uniovi.uo237525.dataquality.{HiveConstants, OutputTypes, _}
import org.apache.log4j.Logger
import org.apache.spark.sql.catalyst.TableIdentifier
import org.apache.spark.sql.catalyst.catalog.CatalogTableType
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}

import scala.util.{Failure, Success, Try}


/**
  * Implementation of a writer adapted to Hive tables.
  *
  * @param metastoreUris parameters needed to connect and write the information into the desired hive database and table.
  */
class HiveWriter(metastoreUris: Option[String],
                 warehouseDir: Option[String],
                 databaseName: Option[String],
                 tableName: Option[String]) extends Writer {

  val logger: Logger = Logger.getRootLogger

  /**
    * Alternative constructor using an outParams element
    *
    * @param outParams parameters needed to connect and write the information into the desired hive database and table.
    */
  def this(outParams: OutputParams) = this(
    outParams.hive_metastore_uris,
    outParams.hive_warehouse_dir,
    outParams.hive_database,
    outParams.hive_table
  )

  /**
    * Alternative constructor where all the parameters result to None except for the db name and the table name.
    *
    * @param databaseName name of the database
    * @param tableName    name of the table inside the database
    * @return an instance of the HiveWriter
    */
  def this(databaseName: String, tableName: String) = this(None, None, Some(databaseName), Some(tableName))


  /**
    * Creates an auxiliary table from the original one and loads the data inside it. After that it drops the auxiliary table
    *
    * @param df         dataframe to save.
    * @param session    session used to query the database
    * @param auxName    name of the auxiliary table.
    * @param originName name of the original table.
    * @return a Try instance of whether it is a success or not.
    */
  private def isDataframeCompatible(df: DataFrame,
                                    session: SparkSession,
                                    auxName: String,
                                    originName: String): Try[DataFrame] = Try {
    logger.warn(s"CREATING AUX TABLE")
    session.sql(s"CREATE TABLE IF NOT EXISTS $auxName LIKE $originName")
    logger.warn(s"SAVING ELEMENTS")
    LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"Creating temporary auxiliary table $auxName to compare data and target table schemas.")
    df.write.mode(SaveMode.Append).insertInto(auxName)
    val newDf = session.sql(s"SELECT * FROM $auxName")
    newDf
  }

  /**
    * Creates the SparkSession needed to write into a hive database.
    *
    * Configures the metastore.uris and then the warehouse dir if any of them is defined.
    * It also adds the catalogImplementation property to to hive and then enables hive support for the session.
    *
    * @return A SparkSession instance
    */
  override def open(): Try[SparkSession] = Try {
    val properties = metastoreUris match {
      case Some(x) =>
        logger.warn(s"""Setting "${HiveConstants.HIVE_METASTORE_URIS_HEADER}" -> $x.""")
        Map(HiveConstants.HIVE_METASTORE_URIS_HEADER -> x)
      case None =>
        LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"Inheriting $HIVE_METASTORE_URIS_HEADER from default Spark Hive configuration.")
        Map[String, String]()
    }

    val propertiesWithWarehouse = warehouseDir match {
      case Some(loc) =>
        logger.warn(s"""Setting "${HiveConstants.SPARK_WAREHOUSE_DIR_HEADER}" -> $loc.""")
        properties + (HiveConstants.SPARK_WAREHOUSE_DIR_HEADER -> loc)
      case None =>
        LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"Inheriting $SPARK_WAREHOUSE_DIR_HEADER from default Spark Hive configuration.")
        properties
    }

    logger.warn(s"""Setting "${HiveConstants.SPARK_CATALOG_IMPLEMENTATION_HEADER}" -> hive.""")
    LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"""Setting "${HiveConstants.SPARK_CATALOG_IMPLEMENTATION_HEADER}" -> hive.""")

    val propertiesWithHiveCatalog = propertiesWithWarehouse + (HiveConstants.SPARK_CATALOG_IMPLEMENTATION_HEADER -> "hive")

    if (propertiesWithWarehouse.nonEmpty) {
      LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT,
        s"Hive Configuration: \n ${propertiesWithWarehouse.toList.map(el => s"${el._1} -> ${el._2}").mkString("\n")}")
    }

    SparkUtils.getSparkSession(enableHiveSupport = true,
      appName = s"$SPARK_JOB_DEFAULT_NAME-${OutputTypes.HIVE}-writer",
      propertiesWithHiveCatalog)
  }

  /**
    * Checks for the database param: In case it is absent uses the current database of the session,
    * else uses the database specified.
    * Checks for the mandatory tableName parameter.
    * Loads the data in a temporary auxiliary table and, if the process has no errors, it loads it in the final table,
    * else the data is not loaded.
    *
    * This method DOES NOT override the entire table, only the data.
    * If the schema of the data is not compatible with the existing table, the method will fail
    *
    * @param df         the dataframe that is going to be written.
    * @param connection an object that provides the connection to the source.
    * @return
    */
  override def overwrite(df: DataFrame, connection: Any): Try[Unit] = Try {
    val session = connection.asInstanceOf[SparkSession]

    val dbase = databaseName match {
      case Some(db) =>
        SparkUtils.changeSessionDatabase(session, db) match {
          case Success(_) =>
            logger.warn(s"""Using DB: "$db".""")
            LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"Using DB: $db for reading")
            db
          case Failure(exception) => throw exception
        }
      case None =>
        val current = SparkUtils.getCurrentDatabase(session)
        logger.warn(s"""Using DB: "$current".""")
        LogManager.addGenericLog(Level.WARN, LogType.OUTPUT, s"Unspecified DB, using current Hive one: $current for reading")
        current
    }

    val tName = this.tableName.getOrElse(
      throw new IllegalArgumentException(s"""Missing mandatory output HIVE param "hive.tableName"."""))

    LogManager.addGenericLog(Level.INFO, LogType.OUTPUT, s"Setting target table: $dbase.$tableName")

    if (session.catalog.tableExists(tName)) {
      logger.warn(s"""Table $dbase.$tableName already exists.""")
      LogManager.addGenericLog(Level.INFO, LogType.OUTPUT, s"""Table $dbase.$tableName already exists.""")
      val auxName = "bdq_temp_" + tName
      isDataframeCompatible(df, session, auxName, tName) match {
        case Success(newDf) =>
          LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"Both schemas are compatible.")
          val isExternal = session.catalog.getTable(tName).tableType.equals(CatalogTableType.EXTERNAL.name)
          if (isExternal) {
            LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"Target table is EXTERNAL, needs to be temporarily altered.")
            val oldTable = session.sessionState.catalog.getTableMetadata(
              TableIdentifier(tName, Some(session.catalog.currentDatabase)))
            val alteredTable = oldTable.copy(tableType = CatalogTableType.MANAGED)
            session.sessionState.catalog.alterTable(alteredTable)
            LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"Successfully altered to MANAGED type.")
          }
          logger.warn(s"""TRUNCATING TABLE $tName.""")
          session.sql(s"TRUNCATE TABLE $tName")

          // insert the data in the table
          logger.warn(s"""INSERTING DATA TABLE $tName.""")
          newDf.write.mode(SaveMode.Append).insertInto(tName)
          LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"Successfully inserted data to target table.")
          if (isExternal) {
            val oldTable = session.sessionState.catalog.getTableMetadata(
              TableIdentifier(tName, Some(session.catalog.currentDatabase)))
            val alteredTable = oldTable.copy(tableType = CatalogTableType.EXTERNAL)
            session.sessionState.catalog.alterTable(alteredTable)
            LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"Target table returned back to EXTERNAL type.")
          }
          logger.warn(s"""DROPPING AUXILIARY TABLE $auxName.""")
          session.sql(s"DROP TABLE IF EXISTS $auxName")
          LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"Dropped auxiliary temp table.")
        case Failure(exception) => throw exception
      }
    }
    else {
      df.write.mode(SaveMode.Overwrite).saveAsTable(tName)
      LogManager.addGenericLog(Level.INFO, LogType.OUTPUT, s"Created new table: $dbase.$tableName with the data.")

    }

  }

  /**
    * Checks for the database param: In case it is absent uses the current database of the session,
    * else uses the database specified.
    * Checks for the mandatory tableName parameter.
    * Appends the data to the table
    *
    * @param df         the dataframe that is going to be written.
    * @param connection an object that provides the connection to the source.
    * @return
    */
  override def append(df: DataFrame, connection: Any): Try[Unit] = Try {
    val session = connection.asInstanceOf[SparkSession]

    val dbase = databaseName match {
      case Some(db) =>
        SparkUtils.changeSessionDatabase(session, db) match {
          case Success(_) =>
            logger.warn(s"""Using DB: "$db".""")
            LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"Using DB: $db for reading")
            db
          case Failure(exception) => throw exception
        }
      case None =>
        val current = SparkUtils.getCurrentDatabase(session)
        logger.warn(s"""Using DB: "$current".""")
        LogManager.addGenericLog(Level.WARN, LogType.OUTPUT, s"Unspecified DB, using current Hive one: $current for reading")
        current
    }

    val tName = this.tableName.getOrElse(
      throw new IllegalArgumentException(s"""Missing mandatory output HIVE param "hive.tableName"."""))

    LogManager.addGenericLog(Level.INFO, LogType.OUTPUT, s"Setting target table: $dbase.$tableName")

    if (session.catalog.tableExists(tName)) {
      logger.warn(s"""Table $dbase.$tableName already exists.""")
      LogManager.addGenericLog(Level.INFO, LogType.OUTPUT, s"""Table $dbase.$tableName already exists.""")
      df.write.mode(SaveMode.Append).insertInto(tName)
    }
    else {
      df.write.mode(SaveMode.Overwrite).saveAsTable(tName)
      LogManager.addGenericLog(Level.INFO, LogType.OUTPUT, s"Created new table: $dbase.$tableName with the data.")
    }
  }

  override def errorIfExists(df: DataFrame, connection: Any): Try[Unit] = Try {
    val session = connection.asInstanceOf[SparkSession]

    val dbase = databaseName match {
      case Some(db) =>
        SparkUtils.changeSessionDatabase(session, db) match {
          case Success(_) =>
            logger.warn(s"""Using DB: "$db".""")
            LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"Using DB: $db for reading")
            db
          case Failure(exception) => throw exception
        }
      case None =>
        val current = SparkUtils.getCurrentDatabase(session)
        logger.warn(s"""Using DB: "$current".""")
        LogManager.addGenericLog(Level.WARN, LogType.OUTPUT, s"Unspecified DB, using current Hive one: $current for reading")
        current
    }
    val tName = this.tableName.getOrElse(
      throw new IllegalArgumentException(s"""Missing mandatory output HIVE param "hive.tableName"."""))

    LogManager.addGenericLog(Level.INFO, LogType.OUTPUT, s"Setting target table: $dbase.$tableName")

    df.write.mode(SaveMode.ErrorIfExists).insertInto(tName)
  }

  override def ignore(df: DataFrame, connection: Any): Try[Unit] = Try {
    val session = connection.asInstanceOf[SparkSession]

    val dbase = databaseName match {
      case Some(db) =>
        SparkUtils.changeSessionDatabase(session, db) match {
          case Success(_) =>
            logger.warn(s"""Using DB: "$db".""")
            LogManager.addGenericLog(Level.DEBUG, LogType.OUTPUT, s"Using DB: $db for reading")
            db
          case Failure(exception) => throw exception
        }
      case None =>
        val current = SparkUtils.getCurrentDatabase(session)
        logger.warn(s"""Using DB: "$current".""")
        LogManager.addGenericLog(Level.WARN, LogType.OUTPUT, s"Unspecified DB, using current Hive one: $current for reading")
        current
    }

    val tName = this.tableName.getOrElse(
      throw new IllegalArgumentException(s"""Missing mandatory output HIVE param "hive.tableName"."""))

    LogManager.addGenericLog(Level.INFO, LogType.OUTPUT, s"Setting target table: $dbase.$tableName")

    df.write.mode(SaveMode.Ignore).insertInto(tName)
  }

}