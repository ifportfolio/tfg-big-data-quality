package es.uniovi.uo237525.dataquality.engine.rules.evaluation

import java.sql.{Date, Timestamp}
import java.time.Instant

import es.uniovi.uo237525.dataquality.SparkSessionTestWrapper
import org.scalatest.{BeforeAndAfterAll, FunSuite}

import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.sql.types._

class EvRangeValuesTest extends FunSuite with SparkSessionTestWrapper with BeforeAndAfterAll {

  import spark.implicits._

  private val ts = Timestamp.from(Instant.now())
  private val date = new Date(System.currentTimeMillis())
  private val map = Map("example1" -> "ex1", "example2" -> "ex2")
  private val mulArray = Array(Array("array of array1"), Array("array of array2"))
  private val seq = Seq(Row(1, Long.MaxValue, null, false, 0.5, ts, Array("val1", "val2"), mulArray, date, map),
    Row(2, Long.MaxValue, "value", null, null, ts, Array("val1", "val2"), mulArray, null, map),
    Row(3, null, "", true, 0.004, ts, null, null, date, null),
    Row(4, 3434343.toLong, "value2345", true, 0.00344, ts, Array(), null, date, null),
    Row(null, null, "6757656", null, null, null, null, null, null, null))

  val someSchema = StructType(
    StructField("int", IntegerType) ::
      StructField("long", LongType) ::
      StructField("string", StringType) ::
      StructField("boolean", BooleanType) ::
      StructField("double", DoubleType) ::
      StructField("timestamp", TimestampType) ::
      StructField("array", ArrayType(StringType)) ::
      StructField("multiple", ArrayType(ArrayType(StringType))) ::
      StructField("date", DateType) ::
      StructField("map", MapType(StringType, StringType)) :: Nil)

  val someDf: DataFrame = spark.createDataFrame(
    spark.sparkContext.parallelize(seq),
    StructType(someSchema))

  val colorsDf = Seq("red", "blue", "yellow", "Blue", "rEd", "YELLOW", "car", "25", "scala").toDF("colors")

  override def afterAll(): Unit = someDf.unpersist()

  /** TEST CHECK PRECONDITIONS */
  test("test CHECK PRE-CONDITIONS: application on un-existing field") {
    val opts = EvRangeValuesOptions(Array("value"))
    val rule = EvRangeValues(field = "non-existing", options = opts)
    assert(rule.checkPreconditions(colorsDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: application on existing field") {
    val opts = EvRangeValuesOptions(Array("value"))
    val rule = EvRangeValues(field = colorsDf.schema.fields.head.name, options = opts)
    assert(rule.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: empty list non accepted") {
    val opts = EvRangeValuesOptions(Array())
    val rule = EvRangeValues(field = colorsDf.schema.fields.head.name, options = opts)
    assert(rule.checkPreconditions(colorsDf).isDefined)
  }


  test("test QUALITY PRE-CONDITIONS: check preconditions in allowed field type (StringType)") {
    val opts = EvRangeValuesOptions(Array("value"))
    val rule = EvRangeValues(field = colorsDf.schema.fields.head.name, options = opts)
    assert(rule.checkPreconditions(colorsDf).isEmpty)
  }

  test("test QUALITY PRE-CONDITIONS: numeric fields") {
    // int
    val intF = "int"
    val ruleInt = EvRangeValues(field = intF, options = EvRangeValuesOptions(Array("value")))
    assert(ruleInt.checkPreconditions(someDf).isDefined)
    // long
    val longF = "long"
    val ruleLong = EvRangeValues(field = longF, options = EvRangeValuesOptions(Array("value")))
    assert(ruleLong.checkPreconditions(someDf).isDefined)
    // decimal / double
    val doubleF = "double"
    val ruleDouble = EvRangeValues(field = doubleF, options = EvRangeValuesOptions(Array("value")))
    assert(ruleDouble.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: BOOLEAN && TIMESTAMP fields") {

    // bool
    val boolF = "boolean"
    val ruleBool = EvRangeValues(field = boolF, options = EvRangeValuesOptions(Array("value")))
    assert(ruleBool.checkPreconditions(someDf).isDefined)
    // timestamp
    val tsF = "timestamp"
    val ruleTs = EvRangeValues(field = tsF, options = EvRangeValuesOptions(Array("value")))
    assert(ruleTs.checkPreconditions(someDf).isDefined)
  }

  test("test QUALITY PRE-CONDITIONS: ARRAY && MAP field types") {
    val rule = EvRangeValues(field = "array", options = EvRangeValuesOptions(Array("value")))
    assert(rule.checkPreconditions(someDf).isDefined)

    val rule2 = EvRangeValues(field = "multiple", options = EvRangeValuesOptions(Array("value")))
    assert(rule2.checkPreconditions(someDf).isDefined)

    val rule3 = EvRangeValues(field = "map", options = EvRangeValuesOptions(Array("value")))
    assert(rule3.checkPreconditions(someDf).isDefined)
  }


  /** TEST APPLICATION OF THE RULE */
  test("test QUALITY APPLY: TEST CASE SENSITIVE") {
    val rule = EvRangeValues(field = colorsDf.schema.fields.head.name,
      options = EvRangeValuesOptions(Array("blue", "red", "yellow"), caseSensitive = true))
    val res = rule.apply(colorsDf).get
    //    val expectedValue = colorsDf.collect().map(_.getString(0)).count().collect()
    //      .map(_.getString(0)).count(el => el.nonEmpty && el.exists(_.isDigit))
    val expectedValue = colorsDf.collect().map(_.getString(0)).count(el => el == "blue" || el == "red" || el == "yellow")
    assert(res.matched == expectedValue)
  }

  test("test QUALITY APPLY: TEST CASE INSENSITIVE") {
    val rule = EvRangeValues(field = colorsDf.schema.fields.head.name,
      options = EvRangeValuesOptions(Array("blue", "red", "yellow")))
    val res = rule.apply(colorsDf).get
    val expectedValue = colorsDf.collect().map(_.getString(0)).count(el =>
      el.toLowerCase() == "blue" || el.toLowerCase() == "red" || el.toLowerCase() == "yellow")

    assert(res.matched == expectedValue)
  }

}
