package es.uniovi.uo237525.dataquality.logmanager

import java.sql.Timestamp

import es.uniovi.uo237525.dataquality.LogConstants.Level.Level
import es.uniovi.uo237525.dataquality.LogConstants.LogType.LogType

/**
  * Record of the information associated with an specific activity occurred in the execution of the application.
  * It records the timestamp where the log is recorded, the log type associated with it,
  * the granularity level and the the additional info that serves as description of the event.
  */
trait Log {

  val timestamp: Timestamp
  val level: Level
  val logType: LogType
  val info: String

  override def toString: String = s"$timestamp [$level] - $logType: $info"
}
