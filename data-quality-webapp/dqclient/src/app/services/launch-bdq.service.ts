import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

/**
 * Service that communicates to the backend the request of launching a new execution.
 */
@Injectable({
  providedIn: 'root'
})
export class LaunchBdqService {

  /** The backend url for making the requests */
  private backendURL: string;

  /**
   * Injects the services and initializes the backend url variable.
   * @param http THe http service used to communicate with the backend
   */
  constructor(private http: HttpClient) {
    //this.backendURL = document.baseURI+'launch';
    this.backendURL = "http://10.211.55.101:8081/launch"
  }

  /**
   * Emits a new POST request to the backend api with the rules and config filenames and contents in order to launch a new
   * execution.
   * 
   * @param rulesFile name for the generated rules file.
   * @param rulesContent  content of the generated rules file.
   * @param confFile  name for the generated config file. 
   * @param confContent Content for the generated config file.
   */
  public launchBDQComponent(rulesFile: string, rulesContent, confFile: string, confContent): Observable<any> {
    console.log(this.backendURL);
    let formData = new FormData();
    formData.append('rules-file', rulesFile)
    formData.append('conf-file', confFile)
    formData.append('rules-content', rulesContent)
    formData.append('conf-content', confContent);

    return this.http.post(this.backendURL, formData)
  }
}
