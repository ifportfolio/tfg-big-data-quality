package es.uniovi.uo237525.dataquality.reader

import java.nio.charset.UnsupportedCharsetException

import com.univocity.parsers.common.TextParsingException
import es.uniovi.uo237525.dataquality._
import es.uniovi.uo237525.dataquality.parser.models.InputParams
import es.uniovi.uo237525.dataquality.reader.sources.CSVReader
import org.scalatest.FunSuite

import scala.io.Source
import scala.reflect.io.File
import org.apache.spark.SparkException
import org.apache.spark.sql.AnalysisException
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{DateType, StringType, TimestampType}

import scala.util.Try

class CSVReaderTest extends FunSuite {

  private val multiLineCSV = getClass.getResource("/csv/multi-line.csv")
  private val samplingRatioTestFile = getClass.getResource("/csv/sampling-ratio-test-file.csv")
  private val dataDifTypesCSV = getClass.getResource("/csv/data-dif-types.csv")
  private val dataCSV = getClass.getResource("/csv/data.csv")
  private val dataNoHeaderCSV = getClass.getResource("/csv/data-no-header.csv")
  private val dataWithQuotesAndComments = getClass.getResource("/csv/data-q&c.csv")
  private val datesCSV = getClass.getResource("/csv/dates.csv")

  /** TEST PATH PARAMETER */
  test("test PATH: valid path reads from file") {
    val numLines = Source.fromInputStream(dataCSV.openStream()).getLines().size
    val path = dataCSV.getPath
    assume(path != null,
      "An existing CSV file path must be provided in order that the test is performed.")

    val src = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(path)
    ))
    // if no exception is thrown then it is read
    src.createDF.count()
    succeed
  }
  test("test PATH: if not specified throws IllegalArgumentException") {
    val s = CSVReader(InputParams(InputTypes.CSV))
    assert(Try(s.createDF).isFailure)
  }
  test("test PATH: invalid path throws AnalysisException") {
    val wrong = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option("wrong-path")
    ))
    assertThrows[AnalysisException] {
      wrong.createDF
    }
  }
  test("test PATH: empty path throws IllegalArgumentException") {
    val empty = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option("")
    ))
    assert(Try(empty.createDF).isFailure)
  }
  test("test PATH: null path throws IllegalArgumentException") {
    val nullP = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(null)
    ))
    assert(Try(nullP.createDF).isFailure)
  }

  /** TEST SEP PARAMETER */
  test("test SEP: valid sep provokes correct parsing of columns") {
    val separator = "*"
    val firstLine = Source.fromInputStream(dataCSV.openStream()).getLines().next()
    val srcNoEx = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_sep = Option(separator)
    ))
    val df = srcNoEx.createDF
    assertResult(firstLine.split("\\" + separator).length) {
      df.columns.length
    }
  }
  test("test SEP: multiple char sep throws IllegalArgumentException") {
    val srcLongDel = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_sep = Option("long-sep")
    ))
    assertThrows[IllegalArgumentException] {
      srcLongDel.createDF
    }
  }
  test("test SEP: incorrect char sep provokes incorrect columns parsing") {
    val correctSep = "*"
    val incorrectSep = "+"
    val numColsCorrectSep = Source.fromInputStream(dataCSV.openStream()).getLines().next().split("\\" + correctSep).length
    val srcWrongDel = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_sep = Option(incorrectSep)
    ))
    val dfWithWrongDel = srcWrongDel.createDF
    assert(dfWithWrongDel.columns.length != numColsCorrectSep) // this df should have 3 columns
  }

  /** TEST ENCODING PARAMETER */
  test("test ENCODING: empty value defaults to UTF-8") {
    val sep = "*"
    val r = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_sep = Option(sep),
      csv_encoding = Option("")
    ))
    val df = r.createDF
    val firstVal = df.first().mkString(sep)
    val utf8String = new String(firstVal.getBytes, "UTF-8")

    assert(firstVal === utf8String)
  }
  test("test ENCODING: val encoding behaves as expected") {
    val firstLine = Source.fromInputStream(dataCSV.openStream()).getLines().next()
    val sep = "*"
    val r = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_sep = Option(sep),
      csv_encoding = Option("UTF-16")
    ))
    val df = r.createDF
    val firstVal = df.first().mkString("*")
    val utf16String = new String(firstLine.getBytes(), "UTF-16")

    assert(firstVal === utf16String)
  }
  test("test ENCODING: unrecognized value throws UnsupportedCharsetException") {
    val r = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_encoding = Option("not-recognized")
    ))
    assertThrows[UnsupportedCharsetException] {
      r.createDF
    }
  }
  test("test ENCODING: null value defaults to UTF-8n") {
    val sep = "*"
    val r = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_sep = Option(sep),
      csv_encoding = Option(null)
    ))
    val df = r.createDF
    val firstVal = df.first().mkString(sep)
    val utf8String = new String(firstVal.getBytes, "UTF-8")

    assert(firstVal === utf8String)
  }

  /** TEST QUOTE PARAMETER */
  test("test QUOTE: multiple char quote throws RunTimeException") {
    val src = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataWithQuotesAndComments.getPath),
      csv_quote = Option("long quote")
    ))
    assertThrows[RuntimeException] {
      src.createDF
    }
  }
  test("test QUOTE: char behaves as expected") {
    val fileContent = Source.fromInputStream(dataWithQuotesAndComments.openStream()).getLines().mkString("\n")
    val quote = "'"
    assume(fileContent.contains(quote))
    val src = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataWithQuotesAndComments.getPath),
      csv_quote = Option(quote),
      csv_sep = Option("*"),
      csv_header = Option(true)
    ))
    assert(src.createDF.collect().forall(row => !row.mkString(" ").contains("'")))
  }
  test("test QUOTE: null char converts to default") {
    val src = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataWithQuotesAndComments.getPath),
      csv_quote = Option(null)
    ))
    src.createDF
  }

  /** TEST ESCAPE PARAMETER */
  test("test ESCAPE: empty value throws RunTimeException") {
    val srcTooLongEscape = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataWithQuotesAndComments.getPath),
      csv_escape = Option("long escape char")
    ))
    assertThrows[RuntimeException] {
      srcTooLongEscape.createDF
    }
  }
  test("test ESCAPE: char value behaves as expected") {
    val srcEscape = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataWithQuotesAndComments.getPath),
      csv_escape = Option("\\"),
      csv_sep = Option("*")
    ))
    val df = srcEscape.createDF
    assert(df.select("_c3").filter(col("_c3").contains("\"")).count() > 0)
  }

  /** TEST CHAR TO ESCAPE QUOTE ESCAPING PARAMETER */
  test("test charToEscapeQuoteEscaping: escape char preceded by charToEscapeQuoteEscaping are recognized as a normal char") {
    val srcEscape = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataWithQuotesAndComments.getPath),
      csv_escape = Option("\\"),
      csv_charToEscapeQuoteEscaping = Option("^"),
      csv_sep = Option("*")
    ))
    val df = srcEscape.createDF
    assert(df.select("_c3").filter(col("_c3").contains("\\")).count() > 0)
    assert(df.select("_c3").filter(col("_c3").contains("^")).count() == 0)
  }

  /** TEST COMMENT PARAMETER */
  test("test COMMENT: commented lines are not introduced in the parsed dataframe") {
    val src = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataWithQuotesAndComments.getPath),
      csv_sep = Option("*"),
      csv_comment = Option("#")
    ))

    // Check that there are no rows starting by '#'
    assertResult(0) {
      src.createDF.withColumn("firstChar", substring(col("_c0"), 0, 1))
        .filter("firstChar == '#'").count()
    }
    // Check that the other special char is recognized
    assert(
      src.createDF.withColumn("firstChar", substring(col("_c0"), 0, 1))
        .filter("firstChar == '$'").count() > 0
    )

    // Check that the # lines are recognized after specifying different comment char
    val srcWith$ = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataWithQuotesAndComments.getPath),
      csv_sep = Option("*"),
      csv_comment = Option("$")
    ))
    // Check that the lines starting with the comment char are not recognized as rows in the df
    assert(srcWith$.createDF.withColumn("firstChar", substring(col("_c0"), 0, 1))
      .filter("firstChar == '#'").count() > 0)

    assertResult(0) {
      srcWith$.createDF.withColumn("firstChar", substring(col("_c0"), 0, 1))
        .filter("firstChar == '$'").count()
    }
  }
  test("test COMMENT: multiple char value throws RunTimeException") {
    val src = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataWithQuotesAndComments.getPath),
      csv_sep = Option("*"),
      csv_comment = Option("###")
    ))

    assertThrows[RuntimeException] {
      src.createDF
    }
  }

  /** TEST HEADER PARAMETER */
  test("test HEADER: header true reads first line as a header") {
    val firstLine = Source.fromInputStream(dataCSV.openStream()).getLines().next()
    val s = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_header = Option(true),
      csv_sep = Option("*")
    ))
    val df = s.createDF
    assertResult(firstLine) {
      df.schema.fieldNames.mkString("*")
    }
  }
  test("test HEADER: header false reads first line as a value") {
    val firstLine = Source.fromInputStream(dataCSV.openStream()).getLines().next()
    val s = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataNoHeaderCSV.getPath),
      csv_header = Option(false),
      csv_sep = Option("*")
    ))
    val df = s.createDF
    assert(firstLine !== df.schema.fieldNames.mkString("*"))
  }

  /** TEST ENFORCE SCHEMA PARAMETER */
  ignore("test ENFORCE SCHEMA: enabled ->") {
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_header = Option(true),
      csv_sep = Option("*"),
      csv_enforceSchema = Option(true)
    ))
    val df = reader.createDF
    // TODO check supposed behaviour when enabled

  }
  ignore("test ENFORCE SCHEMA: disabled ->") {
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_header = Option(true),
      csv_sep = Option("*"),
      csv_enforceSchema = Option(false)
    ))
    val df = reader.createDF
    df.printSchema()
    df.show
    // TODO check supposed behaviour when disabled
  }

  /** TEST INFER SCHEMA PARAMETER */
  test("test INFER SCHEMA: enabled means field types are inferred (NOT ALL DATATYPES ARE INFERRED)") {
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_header = Option(true),
      csv_sep = Option("|"),
      csv_inferSchema = Option(true)
    ))
    val df = reader.createDF
    assert(df.dtypes.map(_._2).exists(_ !== StringType.toString))
  }
  test("test INFER SCHEMA: disabled means all fields are traveled once and parsed as string type") {
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_header = Option(true),
      csv_sep = Option("|"),
      csv_inferSchema = Option(false)
    ))
    val df = reader.createDF
    assert(!df.dtypes.map(_._2).exists(_ !== StringType.toString))
  }

  /** TEST SAMPLING RATIO PARAMETER */
  test("test SAMPLING RATIO: value <= 0 throws IllegalArgumentException") {
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_header = Option(true),
      csv_sep = Option("|"),
      csv_samplingRatio = Option(0)
    ))
    assertThrows[IllegalArgumentException] {
      reader.createDF
    }
  }
  test("test SAMPLING RATIO: the lower less rows are used to infer the schema type") {
    // 0,001 means 0,1% of rows are used to infer the type so as there is only one string row,
    // if the row is not taken as sample, it will infer the int/double type instead of string
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(samplingRatioTestFile.getPath),
      csv_header = Option(true),
      csv_sep = Option(","),
      csv_inferSchema = Option(true),
      csv_samplingRatio = Option(0.001)
    ))
    var df = reader.createDF
    do {
      df = reader.createDF
    }
    while (!df.dtypes.map(_._2).exists(_ !== StringType.toString))
    assert(df.dtypes.map(_._2).exists(_ !== StringType.toString))

    // as all the rows are evaluated to infer the type,
    // it will infer always string type even if there is only one string row in the field

    val readerWithMaxSR = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(samplingRatioTestFile.getPath),
      csv_header = Option(true),
      csv_sep = Option(","),
      csv_inferSchema = Option(true),
      csv_samplingRatio = Option(1.0)
    ))
    val df2 = readerWithMaxSR.createDF
    assert(!df2.dtypes.map(_._2).exists(_ !== StringType.toString))
  }

  /** TEST IGNORE LEADING WHITESPACE PARAMETER */
  test("test IGNORE LEADING WHITESPACE: enabled means leading whitespaces are ignored") {
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_sep = Option("|"),
      csv_header = Option(true),
      csv_ignoreLeadingWhiteSpace = Option(true)
    ))
    val df = reader.createDF
    val firstCol = "string"
    assert(!df.select(firstCol).collect().map(_.getString(0)).exists(_.charAt(0) == ' '))
  }
  test("test IGNORE LEADING WHITESPACE: disabled means leading whitespaces are introduced as part of the value") {
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_sep = Option("|"),
      csv_header = Option(true),
      csv_ignoreLeadingWhiteSpace = Option(false)
    ))
    val df = reader.createDF
    val firstCol = "string"
    assert(df.select(firstCol).collect().map(_.getString(0)).exists(_.charAt(0) == ' '))
  }

  /** TEST IGNORE TRAILING WHITESPACE PARAMETER */
  test("test IGNORE TRAILING WHITESPACE: enabled means trailing whitespaces are ignored") {
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_sep = Option("|"),
      csv_header = Option(true),
      csv_ignoreTrailingWhiteSpace = Option(true)
    ))
    val df = reader.createDF
    val firstCol = "string"
    assert(!df.select(firstCol).collect().map(_.getString(0)).exists(_.toCharArray.last == ' '))
  }
  test("test IGNORE TRAILING WHITESPACE: disabled means trailing whitespaces are introduced as part of the value") {
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_sep = Option("|"),
      csv_header = Option(true),
      csv_ignoreTrailingWhiteSpace = Option(false)
    ))
    val df = reader.createDF
    val firstCol = "string"
    assert(df.select(firstCol).collect().map(_.getString(0)).exists(_.toCharArray.last == ' '))
  }

  /** TEST NULL VALUE PARAMETER */
  test("test NULL VALUE: values with null representing string are parsed as null") {
    val srcNormal = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_header = Option(true),
      csv_sep = Option("*")
    ))
    assert(srcNormal.createDF.where(col("id").isNull).count() == 1)
    val srcWithNull = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_header = Option(true),
      csv_sep = Option("*"),
      csv_nullValue = Option("null")
    ))

    assert(3 == srcWithNull.createDF
      .where(
        col("id").isNull || col("total_value").isNull)
      .count())
  }

  /** TEST EMPTY VALUE PARAMETER */
  test("test EMPTY VALUE: all empty values are replaced by the string specified") {
    val fileContent = Source.fromInputStream(dataDifTypesCSV.openStream()).getLines().toString
    val colName = "stringWithEmpties"
    val subEmptyStr = "empty-sub"
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_sep = Option("|"),
      csv_header = Option(true),
      csv_emptyValue = Option(subEmptyStr)
    ))
    val df = reader.createDF
    assume(!fileContent.contains(subEmptyStr))
    assert(df.select(colName).collect().map(_.getString(0)).exists(_ === subEmptyStr))
  }

  /** TEST NAN VALUE PARAMETER */
  test("test NAN VALUE: matches are converted to NaN") {
    val fileContent = Source.fromInputStream(dataDifTypesCSV.openStream()).getLines().toString
    val colName = "NaN"
    val subNanStr = "nan-string"
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_sep = Option("|"),
      csv_header = Option(true),
      csv_inferSchema = Option(true),
      csv_nanValue = Option(subNanStr)
    ))
    val df = reader.createDF
    assume(!fileContent.contains(subNanStr))
    assert(df.select(colName).collect().map(_.getDouble(0)).forall(_.isNaN))
  }

  /** TEST POSITIVE INF VALUE PARAMETER */
  test("test POSITIVE INF VALUE: matches are recognized as positive infinite") {
    val fileContent = Source.fromInputStream(dataDifTypesCSV.openStream()).getLines().toString
    val colName = "posInf"
    val posInfStr = "posInf"
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_sep = Option("|"),
      csv_header = Option(true),
      csv_inferSchema = Option(true),
      csv_positiveInf = Option(posInfStr)
    ))
    val df = reader.createDF
    assume(!fileContent.contains(posInfStr))
    assert(df.select(colName).collect().map(_.getDouble(0)).forall(_.isPosInfinity))
  }

  /** TEST NEGATIVE INF PARAMETER */
  test("test NEGATIVE INF VALUE: matches are recognized as negative infinite") {
    val fileContent = Source.fromInputStream(dataDifTypesCSV.openStream()).getLines().toString
    val colName = "negInf"
    val negInfStr = "negInf"
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_sep = Option("|"),
      csv_header = Option(true),
      csv_inferSchema = Option(true),
      csv_negativeInf = Option(negInfStr)
    ))
    val df = reader.createDF
    assume(!fileContent.contains(negInfStr))
    assert(df.select(colName).collect().map(_.getDouble(0)).forall(_.isNegInfinity))
  }

  /** TEST DATE FORMAT PARAMETER */
  test("test DATE FORMAT: `valid pattern is recognized and data values are parsed") {
    // inferSchema does not support inferring DateType fields so it must be done by manually specifying the schema
    val schema = "date_field_1 Date, date_field_2 Date"
    val srcWithDateFormat = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(datesCSV.getPath),
      csv_header = Option(true),
      csv_sep = Option(","),
      csv_schema = Option(schema),
      csv_dateFormat = Option("yyyy/MM/dd")
    ))
    val dfDateFormat = srcWithDateFormat.createDF
    dfDateFormat.schema.fields.map(_.dataType).foreach(typ =>
      assert(DateType.typeName.equals(typ.typeName))
    )
    dfDateFormat.schema.names.foreach(fieldName => {
      assert(dfDateFormat.select(fieldName).count == dfDateFormat.filter(col(fieldName).isNotNull).count)
    })
  }
  test("test DATE FORMAT: wrong format provokes value replaced by null (PERMISSIVE MODE)") {
    val schema = "date_field_1 Date, date_field_2 Date"
    val srcWithDateFormatWrong = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(datesCSV.getPath),
      csv_header = Option(true),
      csv_sep = Option(","),
      csv_schema = Option(schema)
      ,
      csv_dateFormat = Option("dd|MM|yyyy")
    ))
    // if not date pattern is matched, value is replaced by null
    val df = srcWithDateFormatWrong.createDF
    df.schema.names.foreach(fieldName => {
      assert(0 == df.filter(col(fieldName).isNotNull).collect.length)
    })
  }

  /** TEST TIMESTAMP FORMAT PARAMETER */
  test("test TIMESTAMP: values matching the pattern are recognized as timestamps") {
    val srcWithTimestampFormat = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(datesCSV.getPath),
      csv_header = Option(true),
      csv_sep = Option(","),
      csv_inferSchema = Option(true),
      csv_timestampFormat = Option("dd/MM/yyyy")
    ))
    srcWithTimestampFormat.createDF.schema.fields.map(_.dataType).foreach(typ =>
      assert(TimestampType.typeName.equals(typ.typeName))
    )
  }
  test("test TIMESTAMP: fields recognized as strings if the pattern is not matched") {
    val srcWithTimestampFormatIncorrect = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(datesCSV.getPath),
      csv_header = Option(true),
      csv_sep = Option(","),
      csv_inferSchema = Option(true),
      csv_timestampFormat = Option("yyyy-/dd/MM")
    ))
    srcWithTimestampFormatIncorrect.createDF.schema.fields.map(_.dataType).foreach(typ =>
      assert(StringType.typeName.equals(typ.typeName))
    )
  }

  /** TEST MAX COLUMNS PARAMETER */
  test("test MAX COLUMNS: negative number throws NegativeArraySizeException") {
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_sep = Option("|"),
      csv_header = Option(true),
      csv_maxColumns = Option(-1)
    ))
    assertThrows[NegativeArraySizeException] {
      reader.createDF
    }
  }
  test("test MAX COLUMNS: limit value lower than number columns throws TextParsingException") {
    val numColumns = Source.fromInputStream(dataDifTypesCSV.openStream()).getLines().next().split('|').length
    val maxColumns = 2
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_sep = Option("|"),
      csv_header = Option(true),
      csv_maxColumns = Option(maxColumns)
    ))
    assume(numColumns > maxColumns)
    assertThrows[TextParsingException] {
      reader.createDF
    }
  }
  test("test MAX COLUMNS: limit value equal or higher than the actual number of column -> correctly parsed df") {
    val numColumns = Source.fromInputStream(dataDifTypesCSV.openStream()).getLines().next().split('|').length
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_sep = Option("|"),
      csv_header = Option(true),
      csv_maxColumns = Option(numColumns)
    ))
    reader.createDF
    succeed
  }

  /** TEST MAX CHARS PER COLUMNS PARAMETER */
  test("test MAX CHARS PER COLUMNS: negative value throws NegativeArraySizeException") {
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_sep = Option("|"),
      csv_header = Option(true),
      csv_maxCharsPerColumn = Option(-10)
    ))
    assertThrows[NegativeArraySizeException] {
      reader.createDF
    }
  }
  test("test MAX CHARS PER COLUMNS: value lower than chars length throws TextParsingException") {
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_sep = Option("|"),
      csv_header = Option(true),
      csv_maxCharsPerColumn = Option(1)
    ))
    assertThrows[TextParsingException] {
      reader.createDF
    }
  }
  test("test MAX CHARS PER COLUMNS:: limit value equal or higher than the actual number of column -> correctly parsed df") {
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataDifTypesCSV.getPath),
      csv_sep = Option("|"),
      csv_header = Option(true),
      csv_maxCharsPerColumn = Option(150)
    ))
    reader.createDF
    succeed
  }


  /** TEST MODE PARAMETER */
  test("test MODE: PERMISSIVE mode, lines with errors are filled") {
    val srcPermissiveMode = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_header = Option(true),
      csv_sep = Option("*"),
      csv_mode = Option("PERMISSIVE")
    ))
    val linesOfFile = File(dataCSV.getPath).lines().count(!_.isEmpty)
    assertResult(linesOfFile - 1) {
      srcPermissiveMode.createDF.count()
    }
  }
  test("test MODE: empty value defaults to PERMISSIVE mode, lines with errors are filled") {
    val srcDefaultMode = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_header = Option(true),
      csv_sep = Option("*")
    ))
    val linesOfFile = File(dataCSV.getPath).lines().count(!_.isEmpty)
    assertResult(linesOfFile - 1) {
      srcDefaultMode.createDF.count()
    }
  }
  test("test MODE: DROPMALFORMED mode, lines with errors are omitted") {
    val srcDropMalfMode = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_header = Option(true),
      csv_sep = Option("*"),
      csv_mode = Option("DROPMALFORMED")
    ))
    assertResult(0) {
      srcDropMalfMode.createDF.where("id == 'malf-line'").count()
    }
  }
  test("test MODE: FAILFAST mode, throws SparkException on fail") {
    val srcFailFastMode = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_header = Option(true),
      csv_sep = Option("*"),
      csv_mode = Option("FAILFAST")
    ))
    assertThrows[SparkException] {
      srcFailFastMode.createDF.show()
    }
  }

  /** TEST COLUMN NAME OF CORRUPTED RECORD PARAMETER */
  test("test COLUMN NAME OF CORRUPTED RECORD: existing column stores all the corrupted info inside") {
    val lines = Source.fromInputStream(dataCSV.openStream()).getLines().toArray.map(_.toString)
    val corrRecCol = "id"
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(dataCSV.getPath),
      csv_sep = Option("*"),
      csv_header = Option(true),
      csv_columnNameOfCorruptRecord = Option(corrRecCol)
    ))
    val df = reader.createDF.cache()
    assert(df.select(corrRecCol).collect().forall(row => lines.contains(row.getString(0))))
  }

  /** TEST MULTI LINE PARAMETER */
  test("test MULTI LINE: enabled") {
    val numRecords = 9
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(multiLineCSV.getPath),
      csv_sep = Option("|"),
      csv_header = Option(true),
      csv_multiLine = Option(true)
    ))
    assertResult(numRecords) {
      reader.createDF.count
    }
  }

  test("test MULTI LINE: disabled") {
    val numLines = Source.fromInputStream(multiLineCSV.openStream()).getLines().size
    val reader = CSVReader(InputParams(
      InputTypes.CSV,
      csv_path = Option(multiLineCSV.getPath),
      csv_sep = Option("|"),
      csv_header = Option(true),
      csv_multiLine = Option(false)
    ))
    assertResult(numLines - 1) {
      reader.createDF.count
    }
  }

}
